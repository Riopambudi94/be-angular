<?php
namespace Services;

class Email
{
    private $MAILGUN_API_KEY = MAILGUN_API_KEY;

    //using MailGun
    public function sendMailBy($to, $body, $from, $subject, $cc = null, $tag = 'Transaction', $attachment = [])
    {
        $data = array(
            'from' => $from,
            'to' => $to,
            'subject' => $subject,
            'html' => $body,
            'o:tag' => $tag,
        );

        if (!empty($attachment['file_name']) && !empty($attachment['mime_type'])) {
            $postFilename = $attachment['post_name']?:'file';
            $data['attachment'] = new \CURLFile($attachment['file_name'], $attachment['mime_type'], $postFilename);
        }

        if ($cc) {
            $cc = implode(",", $cc);
            $data['bcc'] = $cc;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->MAILGUN_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt(
            $ch,
            CURLOPT_URL,
            'https://api.mailgun.net/v2/mg.cls-indo.co.id/messages'
        );
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            $data
        );
        if (isset($data['attachment'])) curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        
        $result = curl_exec($ch);
        curl_close($ch);
    
        return $result;
    }
    
    //using mTarget, but spam detected
    public function sendMailBy_old($to, $body, $from, $subject, $cc)
    {
        if ($subject=='' && $body='') {
            $subject = $from = $body = 'Error Null';
        }

        if ($from=='') {
            $from = 'Locky dari LOCARD+<cs@locard.co.id>';
        }

        if (!empty($cc)) {
            $targetEmail = $cc;
            array_push($targetEmail, $to);
        } else {
            $targetEmail = array($to);
        }

        foreach ($targetEmail as $recepient) {
            $ch = curl_init();
            // $MT_APIKEY             = 'LCx7xkaxyihMeplWN2lYs0Y3ZkEhAt57O5gOz8R45SWzszLKsdKSw962bB8wmjTl';
            $MT_APIKEY             = 'CHmz49n0S1mfiHyeTKjLbDO811bS9TK8vTOij9SraLDuPK8TvX8myieir0PzfeWf';
            $SENDER_EMAIL          = $from;
            $RECIPIENT_EMAIL       = array($recepient);
            $LABEL_EMAIL           = array($subject);
            $FIELD                 = array( 'accessToken' => $MT_APIKEY,
                                        'from' => $from,
                                        'to' => $RECIPIENT_EMAIL,
                                        'labels' => $LABEL_EMAIL,
                                        'subject' => $subject,
                                        'content' => $body);

            $data_string = json_encode($FIELD);
            $headers =    array('Content-Type: text/html');
    
            // set url
            curl_setopt($ch, CURLOPT_URL, 'https://trx.mailtarget.co/outbox');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
            $output[] = curl_exec($ch);
            curl_close($ch);
        }
        // var_dump($output);
        // die();
        return $output;
    }

    public function sendReceipt($to, $msgbody, $htmlBody, $from='info1@locard.co.id', $subject, $header='')
    {
        $boundary = md5(uniqid() . microtime());

        if ($header=="") {
            $headers = 'From: '.$from . "\r\n" .
                        'Reply-To: info1@locard.co.id' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
            $headers .= "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:multipart/alternative; boundary=\"$boundary\"" . "\r\n";
        }

        // Plain text version of message
        $body = "--$boundary\r\n" .
           "Content-Type: text/plain; charset=ISO-8859-1\r\n" .
           "Content-Transfer-Encoding: base64\r\n\r\n";
        $body .= chunk_split(base64_encode($msgbody));

        // HTML version of message
        $body .= "--$boundary\r\n" .
           "Content-Type: text/html; charset=ISO-8859-1\r\n" .
           "Content-Transfer-Encoding: base64\r\n\r\n";
        $body .= chunk_split(base64_encode($htmlBody));
        $body .= "--$boundary--";


        $sendmail = @mail($to, $subject, $body, $headers);

        return $sendmail;
    }
}

?>


<?php
namespace Services;

require_once(__DIR__.'/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv as CsvWriter;
use PhpOffice\PhpSpreadsheet\Reader\Csv as CsvReader;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use PhpOffice\PhpSpreadsheet\IOFactory;

class SpreadsheetService{

	function __construct(){
		$this->spreadsheet = new Spreadsheet();
		$this->writer = new CsvWriter($this->spreadsheet);
		$this->writer->setSheetIndex(0);
		$reader = new CsvReader();
		
	}

	function convertToXlsx($data, $file){
		$this->writer = new XlsxWriter($this->spreadsheet);


		$this->setHeader($data['header']);
		$this->setRows($data['data']);

		$this->writer->save($file);
		return($file);
	}

	function setSecurity($password = "PhpSpreadsheet") {
		$security = $this->spreadsheet->getSecurity();
		$security->setLockWindows(true);
		$security->setLockStructure(true);
		$security->setWorkbookPassword($password);
	}

	function convertToCsv($data,$file){

		$this->setHeader($data['header']);
		$this->setRows($data['data']);
		// $spreadsheet = $this->spreadsheet;
		
		// var_dump($data['header'], $data['data']);

		// $sheet = $spreadsheet->getActiveSheet();

		// $sheet->setCellValue('A1', 'Test !');
		// $sheet->setCellValue('B1', 'Testing Master');
		// $sheet->setCellValue('C1', 'Are You Kidding Me ?');

		// $writer = new Xlsx($this->spreadsheet);
		$this->writer->save($file);
		return($file);
	}

	function setHeader($header){
		$sheet = $this->spreadsheet->getActiveSheet();

		$range = $this->getcolumnrange('A', 'ZZ');

		// $sheet->setCellValue('B1', 'Testing Master');
		// $sheet->setCellValue('C1', 'Are You Kidding Me ?');
		$arrayColumn=[];
		$muchNumber =count($header);
		$this->muchHeaderColumns = $muchNumber;

		$number 	= 0;

		foreach ($range as $column){
			$sheet->setCellValue($column.'1', $header[$number]);
			if($number==$muchNumber-1) break;
			$number++;
		}

		return $arrayColumn;
	}

	function setRows($rows){
		$sheet = $this->spreadsheet->getActiveSheet();

		$range = $this->getcolumnrange('A', 'ZZ');

		$howMuchRow = count($rows);

		$startRow	= 2;
		$howMuchColumns = $this->muchHeaderColumns;
		
		foreach($rows as $rowData){
			$number = 0;
			
			foreach($rowData as $columnName=>$value){
				$column = $range[$number];
				
				$sheet->setCellValue($column.$startRow, $value);
				$number++;
			}
			$startRow++;
		}
	}

	function convertToArray($destination, $type='csv', $setting = [])
	{
		//var_dump($type);
		if(in_array($type, ['xls','xlsx']))
			return $this->doConvertXLStoArray($destination);
		else{
			return false;
		}
		// var_dump(file_exists($destination));
		// $finfo = new finfo();
		// $fileinfo = $finfo->file($destination, FILEINFO_MIME);
		
		// var_dump($fileinfo);
	}

	function doConvertXLStoArray($destination, $setting = [], $useHeader = true, $strToLower = true){
		
		$spreadsheet = IOFactory::load($destination);
		// $reader->setReadDataOnly(true);
		$worksheet = $spreadsheet->getActiveSheet();

		$highestColumn = $spreadsheet->getActiveSheet()->getHighestColumn();
		$highestRow 	= (int)$spreadsheet->getActiveSheet()->getHighestRow();

		$initRow = isset($setting['init_row']) ? $setting['init_row'] : 1;
		$initCol = isset($setting['init_col']) ? $setting['init_col'] : 'A';

		if(!is_numeric($initRow)) return false;
		if(preg_match("/([^A-Z])/", $initCol)) return false;

		$range 		= $this->getcolumnrange($initCol, $highestColumn);

		if($useHeader)
		{
			foreach ($range as $column){
				if(!is_null($val = $worksheet->getCell($column.$initRow)->getValue()))
				{
					if($strToLower == true){
						$val = strtolower($val);
					}
					$header[$column] = $val;
				} else {
					$lastColumn = $column;
					break;
				}
			}
		}

		$rows = [];
		$inc = 0;
		$col = count($header);
		for($row = $initRow+1; $row<=$highestRow; $row++){
			$c = 0;
			foreach ($header as $key => $value){
				if(!is_null($val = $worksheet->getCell($key.$row)->getValue()))
				{
					$rows[$inc][$key] = $val;
				} else {
					$rows[$inc][$key] = "";
					$c++;
				}
			}

			if($c >= $col){
				unset($rows[$inc]);
				break;
			}
			$inc++;
		}

		$dataSet = [];
		if(!empty($header)){
			$inc = $initRow + 1;
			foreach($rows as $data){
				foreach($header as $key=>$tHeader){
					$dataSet[$inc][$tHeader] = $data[$key];
				}
				$inc++;
			}
		} else {
			$dataSet = $data;
		}
		
		return $dataSet;
	}

	function getcolumnrange($min,$max){
		$pointer=strtoupper($min);
		$output=array();
		$pos = 0;
		while($pos <= 0){
			array_push($output,$pointer);
			$pointer++;
			$pos = $this->positionalcomparison($pointer,strtoupper($max));
		}
		return $output;
  	}
  
	function positionalcomparison($a,$b){
		if(strlen($a)<strlen($b)){
			return -1;
		} else {
			return strcmp($a,$b);
		}
	}

	/*
		while(true){
			$header = [];
			foreach ($range as $column){
				if(!is_null($val = $worksheet->getCell($column.$numb)->getValue()))
				{
					if($strToLower == true){
						$val = strtolower($val);
					}
					$header[] = $val;
				} else {}
				// var_dump($headerName->getValue());
				// if($number==$muchNumber-1) break;
				// $number ++;
			}
			// var_dump(count($header)>=$);
			if(count($header)>=count($range)){
				break;
			}
				
			$numb++;
		}
	*/
}


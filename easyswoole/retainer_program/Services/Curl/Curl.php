<?php
namespace Services;

class Curl
{
    public function sendRequest($param)
    {
        if ($param['method'] == 'GET') {
            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => $param['url'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_SSL_VERIFYPEER, false,
                CURLOPT_HTTPHEADER => $param['headers']
            ));

            $curl_result = curl_exec($ch);
            $responseCode = curl_getinfo( $ch , CURLINFO_RESPONSE_CODE);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close($ch);

            $header = substr($curl_result, 0, $header_size);
            $body = substr($curl_result, $header_size);
            
            $result = json_decode($body, 1);

            $returnResult = [
                "header"=>$header,
                'response_code' => $responseCode,
                'raw' => $body,
                'result' => $result
            ];

            return $returnResult;

        } elseif ($param['method'] == 'POST') {
            $ch = curl_init();
            
            curl_setopt_array($ch, array(
                CURLOPT_URL => $param['url'],
                CURLOPT_HTTPHEADER => $param['headers'],
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $param['body']
            ));

            $curl_result = curl_exec($ch );
            $responseCode = curl_getinfo( $ch , CURLINFO_RESPONSE_CODE);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close( $ch );

            $header = substr($curl_result, 0, $header_size);
            $body = substr($curl_result, $header_size);
            
            $result = json_decode($body, 1);

            $returnResult = [
                "header"=>$header,
                'response_code' => $responseCode,
                'raw' => $body,
                'result' => $result
            ];

            return $returnResult;
        }
    }
}

<?php
use App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

class OrderInvoiceController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowedRequest = 'order_id,status,products,address_detail,delivery_detail,return_detail,redelivery_detail,username,member_detail,order_expire_date,request_date,approve_date,cancel_date,last_shipping_info,based_on,invoice_no,po_no';
            foreach ($request['search'] as $key => $value) {
                if (strpos($key, '.') !== false){
                    $allowedRequest .= ",{$key}";
                }
            }

            $data = $this->validateSearchRequest($request, $allowedRequest);

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();

            $result = $OrderInvoiceViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    public function findReportSalesOrder($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowedRequest = 'order_id,status,products,address_detail,delivery_detail,return_detail,redelivery_detail,member_detail,id_toko,nama_toko,nama_pemilik,no_wa_pemilik,alamat_toko,group,order_expire_date,request_date,approve_date,cancel_date,last_shipping_info,based_on,invoice_no,po_no';
            foreach ($request['search'] as $key => $value) {
                if (strpos($key, '.') !== false){
                    $allowedRequest .= ",{$key}";
                }
            }

            $data = $this->validateSearchRequest($request, $allowedRequest);

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();

            $result = $OrderInvoiceViewModel->findReportSalesOrder($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();

            $result = $OrderInvoiceViewModel->find($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function createNewOrder($srv, $params) {
        try {
            $this->authorize("member");
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'address_detail' => expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->createNewOrder($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function processOrderInvoice($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'order_id' => expectedResultString()->required()
            ]);

            $getBody = $this->getBody();

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->processOrderInvoice($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function cancelOrderInvoice($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'order_id' => expectedResultString()->required()
            ]);

            $getBody = $this->getBody();

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->cancelOrderInvoice($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    /* function processOrderToDelivery($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'order_id' => expectedResultString()->required(),
                        'courier'=>expectedResultString()->required(),
                        'courier_name'=>expectedResultString(),
                        'delivery_service'=>expectedResultString()->required(),
                        'delivery_method'=>expectedResultString()->required(),
                        'awb_number'=>expectedResultString(),
                        'remarks'=>expectedResultString()->required()
                    ]);
                    $getBody = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->processOrderToDelivery($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    } */

    function processOrderToComplete($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();
            
            $this->checkAcceptedRequiredBodyParams([
                'order_id' => expectedResultArray()->required()
            ]);
            $getBody = $this->getBody();

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->processOrderToComplete($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    /* function processReturnOrder($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'return_type' => expectedResultString()->required(),
                        'order_id' => expectedResultString()->required(),
                        'courier' => expectedResultString()->required(),
                        'return_date' => expectedResultString()->required(),
                        'remarks' => expectedResultString()->required(),
                        'awb_number' => expectedResultString()->required(),
                    ]);
                    $getBody = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->returnOrderProcess($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    } */

    /* function prosessOrderRedelivery($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'order_id' => expectedResultString()->required(),
                        'courier' => expectedResultString()->required(),
                        'delivery_method' => expectedResultString()->required(),
                        'delivery_service' => expectedResultString()->required(),
                        'delivery_date' => expectedResultString()->required(),
                        'awb_number' => expectedResultString()->required(),
                        'remarks' => expectedResultString()->required(),
                    ]);
                    $getBody = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->returnOrderRedelivery($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    } */

    function getOrderDetail($srv, $param) {
        try {
            $this->authorize('admin');

            $orderId = $srv->get('GET.order_id');

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->getOrderDetail($orderId);

            return $this->sendResult($result);

        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function notifyPendingOrder($srv, $param) {
        try {

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->notifyPendingOrder();

            return $this->sendResult($result);

        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function processBulkOrder($srv, $param) {
        try {

            $this->authorize("admin");
            $auth = $this->getPayload();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $getBody = $file['document'];

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->processBulkOrder($auth, $getBody);

            return $this->sendResult($result);

        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateInvoiceNumber($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $option = $srv->get('GET.option') ?: 'invoice_no';

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $getBody = $file['document'];

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->updateInvoiceNumber($auth, $option, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateAdditionalInfo($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $orderId = $srv->get('GET.order_id');
            
            $postBody = array_merge($srv->POST?:[], $srv->get("FILES")?:[]);

            $OrderInvoiceViewModel = new OrderInvoiceViewModel();
            $result = $OrderInvoiceViewModel->updateAdditionalInfo($auth, $orderId, $postBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}
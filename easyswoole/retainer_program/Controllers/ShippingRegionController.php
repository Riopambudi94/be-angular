<?php
use App\GeneralData\ViewModels\ShippingRegionViewModel;
use App\GeneralData\ViewModels\CourierViewModel;

class ShippingRegionController extends MasterController{

    function firstLoad(){

    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");

            $request = json_decode($srv->get('GET.request'), 1);

            $search = $request['search'] ?: [];

            $ShippingRegionViewModel = new ShippingRegionViewModel();

            $result = $ShippingRegionViewModel->findAdminGroup($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getShipmentCost($srv, $params) {
        try {
            $origin = $srv->get('GET.origin');
            $destination = $srv->get('GET.destination');
            $weight = $srv->get('GET.weight');

            $CourierViewModel = new CourierViewModel();

            $bodyData = [
                'origin'=>$origin,
                'destination'=>$destination,
                'weight'=>$weight
            ];
            $result = $CourierViewModel->getSAPShipmentCost($bodyData);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateShippingRegion($srv, $params) {
        try {
            $this->authorize("admin");

            $courier = $srv->get('GET.courier');

            $auth = $this->getPayload();
            $ShippingRegionViewModel = new ShippingRegionViewModel();

            $result = $ShippingRegionViewModel->updateShippingRegion($courier);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}
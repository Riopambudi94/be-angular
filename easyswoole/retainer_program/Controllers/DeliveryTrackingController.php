<?php

use App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

class DeliveryTrackingController extends MasterController
{
    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'order_id,reference_no,status,courier,courier_name,delivery_type,delivery_method,delivery_service,awb_number,packing_no,last_shipping_info,created_date,updated_date';
            foreach ($request['search'] as $key => $value) {
                if (strpos($key, '.') !== false) {
                    $allowed_request .= ",{$key}";
                }
            }

            $data = $this->validateSearchRequest($request, $allowed_request);
            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'] ? $data['limit_per_page'] : 50, null);

            return $this->sendResult($result);
        } catch (Exception $e) {
            $this->sendError($e);
        }
    }

    public function processPackaging($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'ref_no' => expectedResultString()->required(),
                        'weight' => expectedResultNumber()->required(),
                        'dimensions' => expectedResultArray()->required(),
                    ]);
                    $getBody = $this->getBody();
                    break;

                case 'bulk':
                    $file = $srv->get('FILES');
                    if (empty($file['document'])) {
                        throw new Exception('upload document is required', 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->processPackaging($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }

    public function processOnDelivery($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'ref_no' => expectedResultString()->required(),
                        'courier' => expectedResultString()->required(),
                        'courier_name' => expectedResultString(),
                        'delivery_service' => expectedResultString()->required(),
                        'delivery_method' => expectedResultString()->required(),
                        'delivery_options' => expectedResultArray()->required(),
                        'remarks' => expectedResultString()->required(),
                        'awb_number' => expectedResultString(),
                    ]);
                    $getBody = $this->getBody();
                    break;

                case 'bulk':
                    $file = $srv->get('FILES');
                    if (empty($file['document'])) {
                        throw new Exception('upload document is required', 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->processOnDelivery($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }

    public function processDelivered($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'ref_no' => expectedResultString()->required(),
                        'delivered_date' => expectedResultString()->required(),
                        'receiver_name' => expectedResultString()->required(),
                        'delivery_status' => expectedResultString()->required(),
                        'remarks' => expectedResultString(),
                    ]);
                    $getBody = $this->getBody();
                    break;

                case 'bulk':
                    $file = $srv->get('FILES');
                    if (empty($file['document'])) {
                        throw new Exception('upload document is required', 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->processDelivered($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }

    public function cancelPickupRequest($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'ref_no' => expectedResultString()->required(),
                        'description' => expectedResultString(),
                    ]);
                    $getBody = $this->getBody();
                    break;

                case 'bulk':
                    $file = $srv->get('FILES');
                    if (empty($file['document'])) {
                        throw new Exception('upload document is required', 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->cancelPickupRequest($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }

    public function markLabelPrinted($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'reference_no' => expectedResultArray()->required(),
                'status' => expectedResultString()->required(),
            ]);

            $getBody = $this->getBody();

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->markLabelPrinted($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }

    /*public function createBulkDeliveryTrack($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->createBulkDeliveryOrder($auth);

            return $this->sendResult($result);
        } catch (Throwable $e) {
            return $this->sendError($e);
        }
    }*/

    function processReturnDelivery($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'return_type' => expectedResultString()->required(),
                        'ref_no' => expectedResultString()->required(),
                        'courier' => expectedResultString()->required(),
                        'return_date' => expectedResultString()->required(),
                        'remarks' => expectedResultString()->required(),
                        'awb_number' => expectedResultString()->required(),
                    ]);
                    $getBody = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingviewModel();
            $result = $DeliveryTrackingViewModel->returnDeliveryProcess($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

     function processRedelivery($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $method = $srv->get('GET.method');

            switch ($method) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'ref_no' => expectedResultString()->required(),
                        'courier' => expectedResultString()->required(),
                        'delivery_method' => expectedResultString()->required(),
                        'delivery_service' => expectedResultString()->required(),
                        'delivery_date' => expectedResultString()->required(),
                        'awb_number' => expectedResultString()->required(),
                        'remarks' => expectedResultString()->required(),
                    ]);
                    $getBody = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $getBody = $file['document'];
                    break;
            }

            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
            $result = $DeliveryTrackingViewModel->redeliveryProcess($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}

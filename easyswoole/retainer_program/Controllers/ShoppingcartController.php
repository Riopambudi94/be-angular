<?php

use App\RetailerProgram\ViewModels\ShoppingCartViewModel;

class ShoppingcartController extends MasterController
{
    public function allComplete($srv)
    {
        
        $request = $srv->get("GET.request");
        $orderBy = $search = [];
        
        if ($request) {
            $request = json_decode($request, 1);
            $search = $request['search'];
            $orderBy = $request['order_by'];
        } else {
            $search = array();
        }
        
        try {
            $this->authorize('admin');
            
            $ShoppingCartViewModel = new ShoppingCartViewModel();

            $result = $ShoppingCartViewModel->findAllComplete($search, $orderBy);

            return $this->sendResult($result);
            
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }
    
    public function findShoppingcartByUser($srv)
    {
        try {
            $this->authorize('member');
        
            $auth = $this->getPayload();

            $ShoppingCartViewModel = new ShoppingCartViewModel();

            $result = $ShoppingCartViewModel->findActiveCart($auth['subject_id']);
            
            return $this->sendResult($result);

        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    public function updateAllProducts($srv, $params)
    {
        
        try {
            $this->authorize("member");

            $this->checkAcceptedRequiredBodyParams([
                'products'=>expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();
            $auth = $this->getPayload();

            $ShoppingCartViewModel = new ShoppingCartViewModel();
            $result = $ShoppingCartViewModel->updateAllProducts($auth, $getBody);
            
            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    public function deleteProduct($srv, $params)
    {
        $this->authorize('member');

        $getBody = $this->getBody();

        $auth  = $this->getPayload();
        
        try {

            $ShoppingCartViewModel = new ShoppingCartViewModel();
            
            $result = $ShoppingCartViewModel->deleteProduct($auth, $getBody);
           
            return $this->sendResult($result);

        } catch (Exception $e) {
            return $this->sendError($e);
        }
        
    }

    public function addProduct($srv, $params)
    {
        try {
            $this->authorize("member");

            $this->checkAcceptedRequiredBodyParams([
                'products'=>expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();
            $auth = $this->getPayload();

            $ShoppingCartViewModel = new ShoppingCartViewModel();
            $result = $ShoppingCartViewModel->addProductToShoppingCart($auth, $getBody);
            
            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }
}
<?php
use App\RetailerProgram\ViewModels\MigrateViewModel;

class MigrateController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'username,full_name,cell_phone,email,status,gender,created_date,point_balance';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $MigrateViewModel = new MigrateViewModel();

            $result = $MigrateViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function migrateFromQuickwin($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            if ($auth['app_name'] != "retail_poin_sahabat") {
                $this->sendError(new \Exception("invalid access", 400));
            }

            $MigrateViewModel = new MigrateViewModel();

            $result = $MigrateViewModel->migrateFromQuickwin($auth);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}
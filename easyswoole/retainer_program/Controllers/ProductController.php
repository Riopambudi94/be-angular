<?php
use App\RetailerProgram\ViewModels\ProductViewModel;

class ProductController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'product_name,product_code,product_price,base_value,description,tnc,type,category,status';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $ProductViewModel = new ProductViewModel();

            $result = $ProductViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("member", "admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $ProductViewModel = new ProductViewModel();

            $result = $ProductViewModel->find($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getProductDetail($srv, $params) {
        try {
            $this->authorize("member", "admin");
            $auth = $this->getPayload();

            $productCode = $params['product_code'];

            $ProductViewModel = new ProductViewModel();

            $result = $ProductViewModel->getProductDetail($productCode);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getProductType($srv, $params) {
        try {
            $this->authorize("member","admin");
            $auth = $this->getPayload();

            $ProductViewModel = new ProductViewModel();
            
            $result = $ProductViewModel->getProductType();

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function addNewProduct($srv, $params) {
        try {
            $this->authorize("admin");
            $this->checkAcceptedRequiredBodyParams(
                [
                    'product_name'=>expectedResultString()->required(),
                    'product_code'=>expectedResultString()->required(),
                    'variation'=>expectedResultArray()->required(),
                    'type'=>expectedResultString()->required(),
                    'category'=>expectedResultString()->required(),
                    'status'=>expectedResultString()->required(),
                    // 'weight'=>expectedResultNumber()->required(),
                    // 'dimensions'=>expectedResultArray()->required(),
                    'images_gallery'=>expectedResultArray()->required(),
                    'product_sku'=>expectedResultArray()->required()
                ]
            );

            $auth = $this->getPayload();

            $getBody = $this->getBody();

            $ProductViewModel = new ProductViewModel();
            
            $result = $ProductViewModel->addNewProduct($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function uploadProductBulk($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $ProductViewModel = new ProductViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $result = $ProductViewModel->uploadProductBulk($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateProduct($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $getBody = $this->getBody();
            $ProductViewModel = new ProductViewModel();

            $result = $ProductViewModel->updateProduct($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateProductBulk($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $ProductViewModel = new ProductViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $result = $ProductViewModel->updateProductBulk($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteProduct($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();
            
            $productCode = $params['product_code'];

            $ProductViewModel = new ProductViewModel();

            $result = $ProductViewModel->deleteProduct($auth, $productCode);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}
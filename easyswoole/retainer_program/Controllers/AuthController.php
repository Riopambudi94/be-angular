<?php
use App\UserManagement\ViewModels\AuthViewModel;

class AuthController extends MasterController{

    function firstLoad(){

    }

    function requestTokenAuth($srv, $params) {
        try {
            $this->checkAcceptedRequiredBodyParams([
                'username'=>expectedResultString()->required(),
                'password'=>expectedResultString()->required()
            ]);

            $getBody = $this->getBody();

            $appLabel = $this->getHeaders('app_label')?:$this->getHeaders('app-label');
            $AuthViewModel = new AuthViewModel();
            $result = $AuthViewModel->requestTokenMember($getBody, $appLabel);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function requestTokenAdmin($srv, $params) {
        try {
            $this->checkAcceptedRequiredBodyParams([
                'username'=>expectedResultString()->required(),
                'password'=>expectedResultString()->required()
            ]);
            
            $getBody = $this->getBody();

            $AuthViewModel = new AuthViewModel();

            $result = $AuthViewModel->requestTokenAdmin($getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function requestTokenMember($srv, $params) {
        try {
            $this->authorize('admin');

            $this->checkAcceptedRequiredBodyParams([
                'username'=>expectedResultString()->required()
            ]);
            
            $getBody = $this->getBody();
            $auth = $this->getPayload();

            $AuthViewModel = new AuthViewModel();

            $result = $AuthViewModel->requestTokenMemberByAdmin($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}
<?php
namespace App\UserManagement\ViewModels\AdminViewModel;

use App\UserManagement\Models\AdminModel;

function findAdmin($arguments, $thisViewModel) {
    $search = $arguments[0];
    try {

        $AdminModel = new AdminModel();

        $result = $AdminModel->find($search);
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
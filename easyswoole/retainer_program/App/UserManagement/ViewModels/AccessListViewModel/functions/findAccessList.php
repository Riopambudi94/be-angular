<?php
namespace App\UserManagement\ViewModels\AccessListViewModel;

use App\UserManagement\Models\AccessListModel;

function findAccessList($arguments, $thisViewModel) {
    $search = $arguments[0];
    try {

        $AccessListModel = new AccessListModel();
        $result = $AccessListModel->find($search);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
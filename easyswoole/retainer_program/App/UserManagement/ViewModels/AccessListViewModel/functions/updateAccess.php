<?php
namespace App\UserManagement\ViewModels\AccessListViewModel;

use App\UserManagement\Models\AccessListModel;
use App\UserManagement\Models\AppListModel;

function updateAccess($arguments, $thisViewModel) {
    $getParams = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $acceptedBodyData = 'description,status,endpoint,app_label';


        $AccessListModel = new AccessListModel();
        $find = $AccessListModel->findByNameOrEndpoint(['name'=>$getParams['name'], 'app_label'=>$getParams['app_label']]);
        if (empty($find['result'])) {
            $thisViewModel->sendError("access menu does not exists", 404);
        }

        $acceptedField = explode(",", $acceptedBodyData);
        foreach ($bodyData as $key => $value) {
            if(!in_array($key, $acceptedField)) {
                unset($bodyData[$key]);
            } else {
                $bodyData[$key] = trim($value);
            }
        }


        if (empty($bodyData['app_label'])) {
            $thisViewModel->sendError("App label field is required", 400);
        }
        $bodyData['app_label'] = trim($bodyData['app_label']);

        $AppListModel = new AppListModel();
        $findApp = $AppListModel->findByAppLabel($bodyData['app_label'], 'ACTIVE');
        if (empty($findApp['result'])) {
            $thisViewModel->sendError("App label is invalid", 400);
        }
        $bodyData['app_id'] = $findApp['result'][0]->_id;

        
        $result = $AccessListModel->update(
            ['_id'=>$find['result'][0]->_id],
            $bodyData
        );

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
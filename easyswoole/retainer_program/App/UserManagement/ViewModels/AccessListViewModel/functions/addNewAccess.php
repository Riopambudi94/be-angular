<?php
namespace App\UserManagement\ViewModels\AccessListViewModel;

use App\UserManagement\Models\AccessListModel;
use App\UserManagement\Models\AppListModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function addNewAccess($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $options = $arguments[1];
    $bodyData = $arguments[2];
    try {

        switch ($options) {
            case 'single':
                $bodyData['name'] = trim($bodyData['name']);
                $bodyData['endpoint'] = trim($bodyData['endpoint']);
                unset($bodyData['status']);

                if (empty($bodyData['app_label'])) {
                    $thisViewModel->sendError("App label field is required", 400);
                }
                $bodyData['app_label'] = trim($bodyData['app_label']);

                $AppListModel = new AppListModel();
                $findApp = $AppListModel->findByAppLabel($bodyData['app_label'], 'ACTIVE');
                if (empty($findApp['result'])) {
                    $thisViewModel->sendError("App label is invalid", 400);
                }

                $AccessListModel = new AccessListModel();
                $find = $AccessListModel->findByNameOrEndpoint($bodyData);
                if (!empty($find['result'])) {
                    $thisViewModel->sendError("access name or endpoint already exists", 409);
                }

                $bodyData['app_id'] = $findApp['result'][0]->_id;
                $bodyData['status'] = 'ACTIVE';

                $result = $AccessListModel->insert($bodyData);
                break;
            
            case 'bulk':
                $MediaViewModel = new MediaViewModel();
                $document = $MediaViewModel->uploadDocument($auth, $bodyData, "ADD_ACCESS_BULK");

                $SpreadsheetService = new SpreadsheetService();
                $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

                $accessList = [];
                $AppListModel = new AppListModel();
                $AccessListModel = new AccessListModel();
                foreach ($files as $key => $value) {
                    $name = trim($value['name']);
                    $endpoint = trim($value['endpoint']);
                    $appLabel = trim($value['app_label']);

                    $k = $key+1;
                    empty($name) ?
                        $thisViewModel->sendError("Name field on row {$k} is required", 400): null;
                    empty($endpoint) ?
                        $thisViewModel->sendError("Endpoint field on row {$k} is required", 400): null;
                    empty($appLabel) ?
                        $thisViewModel->sendError("App label field on row {$k} is required", 400): null;

                    $findApp = $AppListModel->findByAppLabel($appLabel, 'ACTIVE');
                    if (empty($findApp['result'])) {
                        $thisViewModel->sendError("App label {$appLabel} is invalid", 400);
                    }

                    $find = $AccessListModel->findByNameOrEndpoint($value);
                    if (!empty($find['result'])) {
                        $thisViewModel->sendError("access name or endpoint on row {$k} already exists", 409);
                    }

                    $accessList[] = [
                        'name'=>$name,
                        'description'=>'',
                        'status'=>'ACTIVE',
                        'endpoint'=>$endpoint,
                        'app_id'=>$AccessListModel->convertToObjectId($findApp['result'][0]->_id),
                        'app_label'=>$appLabel
                    ];
                }

                $result = $AccessListModel->insertBatch($accessList);
                break;
        }

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\UserManagement\ViewModels\AppListViewModel;

use App\UserManagement\Models\AppListModel;

function findApp($arguments, $thisViewModel) {
    $search = $arguments[0];
    try {

        $AppListModel = new AppListModel();
        $result = $AppListModel->find($search);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
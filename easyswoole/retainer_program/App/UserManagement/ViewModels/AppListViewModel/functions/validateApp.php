<?php
namespace App\UserManagement\ViewModels\AppListViewModel;

function validateApp($arguments, $thisViewModel) {
    global $selectedDB;

    $appLabel = $arguments[0];
    try {

        $filter = [
            'app_label'=>['eq'=>$appLabel],
            'status'=>['eq'=>'ACTIVE']
        ];
        $findApp = $thisViewModel->findApp($filter);
        if (empty($findApp['result'])) {
            $thisViewModel->sendError("Unauthorized, invalid access", 403);
        }
        $selectedDB = $findApp['result'][0]->app_name;

        return $thisViewModel->objectToArray($findApp['result'][0]);
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
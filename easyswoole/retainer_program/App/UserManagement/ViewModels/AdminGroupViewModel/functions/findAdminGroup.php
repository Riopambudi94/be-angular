<?php
namespace App\UserManagement\ViewModels\AdminGroupViewModel;

use App\UserManagement\Models\AdminGroupModel;

function findAdminGroup($arguments, $thisViewModel) {
    $search = $arguments[0];
    try {

        $AdminGroupModel = new AdminGroupModel();
        $result = $AdminGroupModel->find($search);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\RetailerProgram\Models\MemberAuthorizationModel;
use App\UserManagement\ViewModels\AppListViewModel;
use App\RetailerProgram\ViewModels\MemberViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function requestTokenMember ($arguments, $thisViewModel){

    $bodyData = $arguments[0];
    $appLabel = $arguments[1];
    try {

        // validate application
        $AppListViewModel = new AppListViewModel();
        $AppListViewModel->validateApp($appLabel);

        $MemberViewModel = new MemberViewModel();
        $memberData = $MemberViewModel->validateMember($bodyData['username'], $bodyData['password']);

        $iat = time();
        $exp = 36000;

        $auth = [
            'type' => 'member',
            'user_id'=>$memberData['_id'],
            'subject_id'=>$memberData['_id'],
            'member_username'=>$memberData['username'],
            'full_name'=>$memberData['full_name'],
            'issued_time'=>$iat,
            'expiry_time'=>$exp,
            'active'=>1
        ];

        $CryptoViewModel = new CryptoViewModel();
        $token = $CryptoViewModel->generateJWT($auth);

        $auth = array_merge(
            ['token'=>$token],
            $auth
        );
 
        $MemberAuthorizationModel = new MemberAuthorizationModel();
        $saveToken = $MemberAuthorizationModel->insert($auth);

        $result = [
            'result'=>[
                'token'=>$token,
                'type' => 'member',
                'member_username'=>$memberData['username'],
                'full_name'=>$memberData['full_name'],
                'issued_time'=>$iat,
                'expiry_time'=>$exp,
                'active'=>1
            ]
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
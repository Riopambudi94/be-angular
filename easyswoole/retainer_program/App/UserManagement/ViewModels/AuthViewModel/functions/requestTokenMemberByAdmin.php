<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\RetailerProgram\Models\MemberAuthorizationModel;
use App\RetailerProgram\ViewModels\MemberViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function requestTokenMemberByAdmin($arguments, $thisViewModel){
    $auth = $arguments[0];
    $bodyData = $arguments[1];
    try {

        $MemberViewModel = new MemberViewModel();
        $memberData = $MemberViewModel->findMember(['username'=>$bodyData['username'], 'status'=>'ACTIVE']);
        if (empty($memberData['result'])) {
            $thisViewModel->sendError("username is not valid", 400);
        }
        $memberData = $thisViewModel->objectToArray($memberData['result'][0]);

        $iat = time();
        $exp = 36000;

        $auth = [
            'type' => 'member',
            'user_id'=>$auth['user_id'],
            'subject_id'=>$memberData['_id'],
            'member_username'=>$memberData['username'],
            'full_name'=>$memberData['full_name'],
            'issued_time'=>$iat,
            'expiry_time'=>$exp,
            'active'=>1
        ];

        $CryptoViewModel = new CryptoViewModel();
        $token = $CryptoViewModel->generateJWT($auth);

        $auth = array_merge(
            ['token'=>$token],
            $auth
        );
 
        $MemberAuthorizationModel = new MemberAuthorizationModel();
        $saveToken = $MemberAuthorizationModel->insert($auth);

        $result = [
            'result'=>[
                'token'=>$token,
                'type' => 'member',
                'member_username'=>$memberData['username'],
                'full_name'=>$memberData['full_name'],
                'issued_time'=>$iat,
                'expiry_time'=>$exp,
                'active'=>1
            ]
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\UserManagement\Models;

use Infrastructure\MongoModel;

class AppListModel extends MongoModel {
    public $table;
    public $dbName = "user_management";
    public $collectionName = "dtm_app_list";

    public $requestColumns = "_id,app_name,app_label,app_prefix,app_description,status,count";
    public $requestMapping = [
        '_id' => '$_id',
        'app_name'=>'$app_name',
        'app_label'=>'$app_label',
        'app_prefix'=>'$app_prefix',
        'app_description'=>'$app_description',
        'status'=>'$status'
    ];

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByAppLabel($appLabel, $status = null) {
        try {
            $match = ['app_label'=>$appLabel];

            !empty($status)?
                $match['status'] = $status:
                null;

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
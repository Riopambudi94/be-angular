<?php
namespace App\UserManagement\Models;

use Infrastructure\MongoModel;

class AccessListModel extends MongoModel {
    public $table;
    public $dbName = "user_management";
    public $collectionName = "dtm_access_list";

    public $requestColumns = "_id,name,description,status,endpoint,app_id,app_label,count";
    public $requestMapping = [
        '_id' => '$_id',
        'name'=>'$name',
        'description'=>'$description',
        'status'=>'$status',
        'endpoint'=>'$endpoint',
        'app_id'=>'$app_id',
        'app_label'=>'$app_label'
    ];

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id' || $key == 'app_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByNameOrEndpoint($search) {
        try {
            $match = [
                '$or'=>[
                    ['name'=>$search['name']],
                    ['endpoint'=>$search['endpoint']],
                ]
            ];

            !empty($search['status'])?
                $match['status'] = $search['status']:
                null;
            
            !empty($search['app_label'])?
                $match['app_label'] = $search['app_label']:
                null;

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function getAccessNameList($search = []) {
        try {
            $match = [];
            if (!empty($search['name'])) {
                !is_array($search['name']) ?
                    $search['name'] = array($search['name']):
                    null;

                $match['name'] = ['$in'=>$search['name']];
            }

            if (!empty($search['status'])) {
                $match['status'] = $search['status'];
            }

            if (!empty($search['_id'])) {
                !is_array($search['_id']) ?
                    $search['_id'] = array($search['_id']):
                    null;

                foreach ($search['_id'] as $key => $value) {
                    $search['_id'][$key] = $this->convertToObjectId($value);
                }
                $match['_id'] = ['$in'=>$search['_id']];
            }

            if (!empty($search['app_label'])) {
                !is_array($search['app_label']) ?
                    $search['app_label'] = array($search['app_label']):
                    null;

                $match['app_label'] = ['$in'=>$search['app_label']];
            }

            if (!empty($search['endpoint'])) {
                !is_array($search['endpoint']) ?
                    $search['endpoint'] = array($search['endpoint']):
                    null;

                $match['endpoint'] = ['$in'=>$search['endpoint']];
            }

            $pipeline = [];
            if (!empty($match)) {
                $pipeline[] = ['$match'=>$match];
            }

            $pipeline[] = [
                '$group' => [
                    '_id'=>null,
                    'name'=>['$push'=>'$name'],
                    'endpoint'=>['$push'=>'$endpoint'],
                    'app_label'=>['$push'=>'$app_label'],
                ]
            ];
            
            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function getAccessListByApp() {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $this->requestColumns .= ',access_list';
            $this->requestMapping['access_list'] = '$access_list';

            $pipeline = [
                [
                    '$match' => [
                        'status' => 'ACTIVE'
                    ]
                ],
                [
                    '$group'=>[
                        '_id'=>'$app_label',
                        'access_list'=>[
                            '$push' => [
                                'name' => '$name',
                                'description' => '$description'
                            ]
                        ]
                    ]
                ]
            ];
            
            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $bodyData['app_id'] = $this->convertToObjectId($bodyData['app_id']);
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            if (!empty($bodyData['app_id'])) {
                $bodyData['app_id'] = $this->convertToObjectId($bodyData['app_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
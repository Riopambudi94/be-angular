<?php
namespace App\GeneralData\ViewModels\ReportViewModel;

use Services\SpreadsheetService;

function convertResultToCsv($arguments, $thisViewModel) {
    $result = $arguments[0];
    $directory = $arguments[1];
    $fileName = $arguments[2];

    empty($directory) ? $directory = 'assets/csv/' : null;
    empty($fileName) ? $fileName = 'file.csv' : null;
    try {

        if (empty($result)) {
            return false;
        }
        $fileName = (uniqid(rand(0, 999))).$fileName;

        $filePath = PROJECT_PATH.$directory;
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, true);
        }
        $outputDir = $filePath.$fileName;

        $resJson = json_decode(json_encode($result), 1);

        $data = ['header' => []];

        foreach ($resJson[0] as $key => $value) {
            $data['header'][] = $key;
        }

        foreach ($resJson as $index => $dtRes) {
            foreach ($dtRes as $key => $value) {
                if (!in_array($key, $data['header'])) {
                    $data['header'][] = $key;
                }
                if (is_array($value)) {
                    $resJson[$index][$key] = json_encode($value);
                }
            }
        }

        $data['data'] = $resJson;
        $SpreadsheetService = new SpreadsheetService();

        $SpreadsheetService->convertToCsv($data, $outputDir);

        $result = [
            'output_dir' => $outputDir,
            'filename' => $fileName
        ];
        return $result;
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
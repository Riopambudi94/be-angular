<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use App\GeneralData\Models\CourierListModel;

function getCourier($arguments, $thisViewModel)
{
    $courierCode = $arguments[0];
    try {
        $CourierListModel = new CourierListModel();

        $courier = $CourierListModel->findByCourierCode($courierCode);

        if (!empty($courier['result'])) {
            $courier = $thisViewModel->objectToArray($courier['result'][0]);

            if(ENVIRONTMENT == 'PRODUCTION') {
                $apiKey = !empty($courier['api_key']['production']) ? $courier['api_key']['production'] : [];
                $apiUrl = !empty($courier['api_urls']['production']) ? $courier['api_urls']['production'] : [];
            } elseif(ENVIRONTMENT == 'DEVELOPMENT') {
                $apiKey = !empty($courier['api_key']['stagging']) ? $courier['api_key']['stagging'] : [];
                $apiUrl = !empty($courier['api_urls']['stagging']) ? $courier['api_urls']['stagging'] : [];
            }

            $courier['api_key'] = $apiKey;
            $courier['api_url'] = $apiUrl;
        } else {
            $courier = [];
            // $thisViewModel->sendError("No Courier Found", 404);
        }

        return $courier;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;
function cancelPickupSAP($arguments, $thisViewModel){
    $getBody = $arguments[0];
    try{
        $Curl = new Curl();
        $param = $thisViewModel->getCourier("REALS-SAP");

        // if(ENVIRONTMENT == 'DEVELOPMENT') $param['api_key'] = "DEV_m4rK3tPlac3#_2019";

        $requestBody = [
            'awb_no'=>$getBody['awb_number'],
            'desc'=>$getBody['description']?:"CANCEL PICKUP",
            'cancel_date'=>$getBody['cancel_date']?:date('Y-m-d H:i:s')
        ];

        $setParam['url'] = $param['api_url']['cancel_pickup'];
        $setParam['method'] = 'POST';
        $setParam['headers'] = [
                            "api-key: ".$param['api_key']['tracking'],
                            'Content-Type: application/json'
                            ];
        $setParam['body'] = json_encode($requestBody);

        $result = $Curl->sendRequest($setParam);

        $dataLog = ['request_type'=>'SAP_CANCEL_PICKUP',
                    'request_header'=>$setParam['headers'],
                    'request_body'=>$setParam['body'],
                    'request_url'=>$setParam['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');


        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
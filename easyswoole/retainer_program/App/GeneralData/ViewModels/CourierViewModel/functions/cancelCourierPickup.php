<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

function cancelCourierPickup($arguments, $thisViewModel){
    $courier = $arguments[0];
    $getBody = $arguments[1];
    try{

        switch ($courier) {
            case 'REALS-SAP':
                $result = $thisViewModel->cancelPickupSAP($getBody);
                break;
            
            default:
                # code...
                break;
        }

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
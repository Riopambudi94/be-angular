<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use App\GeneralData\Models\ShippingRegionModel;
use App\GeneralData\Models\AdministrativeRegionModel;

function getAvailableCourier($arguments, $thisViewModel)
{
    $origin = $arguments[0];
    $destination = $arguments[1];
    $weight = $arguments[2];
    $typeSpecial = $arguments[3] ?: false;
    $packageValue = $arguments[4];

    try {

        (empty($origin) || empty($destination)) ? 
            $thisViewModel->sendError("origin and destination is required", 400) : null;
            
        $ShippingRegionModel = new ShippingRegionModel();
        /* $AdministrativeRegionModel = new AdministrativeRegionModel();

        $getOrigin = $AdministrativeRegionModel->findRegionByCode($origin);
        empty($getOrigin['result']) ? 
            $thisViewModel->sendError("origin {$origin} not found", 400) : null;

        $getDestination = $AdministrativeRegionModel->findRegionByCode($destination);
        empty($getDestination['result']) ? 
            $thisViewModel->sendError("destination {$destination} not found", 400) : null; */

        $courierList = $thisViewModel->getCourierList();

        $returnResult = [];
        foreach ($courierList as $key => $value) {
            if (!empty($value['default_origin'])) {

                $filter = [
                    'courier_code'=>$value['courier_code'],
                    'region_code'=>$value['default_origin']['origin_code'],
                    'usage'=>'origin'
                ];
                $regionOrigin = $ShippingRegionModel->find($filter);

                $originAddress = $value['default_origin']['origin_address'];
                unset($value['default_origin']);
            } else {

                $filter = [
                    'courier_code'=>$value['courier_code'],
                    'usage'=>'origin',
                    'regional_code'=>['$in'=>$origin['code']]
                ];
                $regionOrigin = $ShippingRegionModel->find($filter);
                $originAddress = $origin['address']['address'];
            }

            if (empty($regionOrigin['result'])) continue;
            $originCode = $regionOrigin['result'][0]->region_code;

            $value['origin'] = [
                'regional_code' => $regionOrigin['result'][0]->regional_code,
                'origin_branch' => $regionOrigin['result'][0]->region_data->tlc_branch_code,
                'region_code' => $regionOrigin['result'][0]->region_code,
                'address' => $originAddress
            ];

            $filter = [
                'courier_code'=>$value['courier_code'],
                'usage'=>'destination',
                'regional_code'=>['$in'=>$destination['code']]
            ];
            $regionDest = $ShippingRegionModel->find($filter);

            if (empty($regionDest['result'])) continue;

            $destCode = $regionDest['result'][0]->region_code;

            $value['destination'] = [
                'regional_code' => $regionDest['result'][0]->regional_code,
                'dest_branch' => $regionDest['result'][0]->region_data->tlc_branch_code,
                'region_code' => $regionDest['result'][0]->region_code,
                'address' => $destination['address']['address']
            ];
            
            $deliveryService = $thisViewModel->getAvailableDeliveryService($value['courier_code'], $originCode, $destCode, $weight, $typeSpecial, $packageValue);
            
            $services = [];
            foreach ($value['delivery_service'] as $sk => $sval) {
                if (!empty($deliveryService[$sval])) {
                    $services[$sk] = $deliveryService[$sval];
                }
            }
            $value['delivery_service'] = $services;
            $returnResult[] = $value;
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;
function courierPickUpSAP($arguments, $thisViewModel){
    $getBody = $arguments[0];
    $special = $arguments[1]?:false;
    try{
        $Curl = new Curl();
        $param = $thisViewModel->getCourier("REALS-SAP");

        $logType = 'SAP_PICKUP_REQUEST';

        $apiKey = $param['api_key']['pickup'];
        if ($special)  {

            $logType = 'SAP_PICKUP_REQUEST_SPECIAL';
            
            $apiKey = $param['api_key']['pickup_special'];
        }

        $setParam['url'] = $param['api_url']['pickup_data_transfer'];
        $setParam['method'] = 'POST';
        $setParam['headers'] = [
                            "api-key: ".$apiKey,
                            'Content-Type: multipart/form-data'
                            ];
        /* $setParam['body'] = [
            'document[]' => '@'.$filePath.'; type='.$mime.'; filename='.$info['basename']
        ]; */
        // $setParam['body'] = ['file' => new CURLFile($filePath, $mime, $info['filename'])];
        $setParam['body'] = $getBody['file'];

        $result = $Curl->sendRequest($setParam);

        $dataLog = ['request_type'=>$logType,
                    'request_header'=>$setParam['headers'],
                    'request_body'=>$setParam['body'],
                    'request_url'=>$setParam['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');


        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
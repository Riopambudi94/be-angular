<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;

function getSAPShipmentCost($arguments, $thisViewModel) 
{
    $getBody = $arguments[0];
    $special = $arguments[1]?:false;
    try {

        $Curl = new Curl();
        $courier = $thisViewModel->getCourier("REALS-SAP");

        $requestBody = [
            'origin'=>$getBody['origin'],
            'destination'=>$getBody['destination'],
            'weight'=>$getBody['weight']
        ];

        $logType = 'SAP_CALCULATE_DELIVERY_COST';

        $apiKey = $courier['api_key']['tracking'];
        $apiUrl = $courier['api_url']['shipment_cost'];

        if (ENVIRONTMENT == 'PRODUCTION') {
            $requestBody['customer_code'] = $courier['customer_code']['regular'];
            if ($special) {

                $logType = 'SAP_CALCULATE_DELIVERY_COST_SPECIAL';
    
                $apiUrl = $courier['api_url']['shipment_cost_special'];
                $apiKey = $courier['api_key']['special'];
                
                $requestBody['customer_code'] = $courier['customer_code']['special'];
                $requestBody['insurance'] = $courier['insurance_rate']['special'];
                $requestBody['item_value'] = $getBody['item_value'];
            }
        } else {
            $requestBody['cost_type'] = 'NEW';
             if ($special) {

                $logType = 'SAP_CALCULATE_DELIVERY_COST_SPECIAL';
    
                $apiUrl = $courier['api_url']['shipment_cost_special'];
                $apiKey = $courier['api_key']['special'];
                $requestBody['customer_code'] = $courier['customer_code']['special'];
                $requestBody['insurance'] = $courier['insurance_rate']['special'];
                $requestBody['item_value'] = $getBody['item_value'];
            }
        }

        $requests['url'] = $apiUrl;
        $requests['method'] = 'POST';
        $requests['headers'] = [
                            "api-key: ".$apiKey,
                            'Content-Type: application/json'
                            ];
        $requests['body'] = json_encode($requestBody);
        
        $result = $Curl->sendRequest($requests);

        $dataLog = ['request_type'=>$logType,
                    'request_header'=>$requests['headers'],
                    'request_body'=>$requests['body'],
                    'request_url'=>$requests['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');

        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
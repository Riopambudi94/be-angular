<?php
namespace App\GeneralData\ViewModels\EmailViewModel;

use App\GeneralData\Models\EmailTemplateModel;
use Services\Email;
function getEmailTemplate($arguments, $thisViewModel) {
    $filter = $arguments[0];
    try {

        $EmailTemplateModel = new EmailTemplateModel();

        $template = $EmailTemplateModel->find($filter);

        return $thisViewModel->objectToArray($template['result'][0]);
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\GeneralData\ViewModels\MediaViewModel;

use App\GeneralData\Models\ConfigurationModel;
use App\GeneralData\Models\MediaUploadModel;

function uploadImage($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $image = $arguments[1];
    try {

        if ($image['size'] > 3*1024 *1024) {
            $thisViewModel->sendError("Maximum image size is 3 MB", 400);
        }
        
        $file = [
            'filename' => $image['name'].uniqid(),
            'file_path' => $image['tmp_name'],
            'size' => $image['size']
        ];

        $ConfigurationModel = new ConfigurationModel();
        $uploadConfig = $ConfigurationModel->findByKeyName('image-upload-configuration');
        $uploadConfig = $thisViewModel->objectToArray($uploadConfig['result'][0]->values);
        $whitelistType = $uploadConfig['image_type'];

        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $fileMimeType = finfo_file($fileinfo, $image['tmp_name']);
        if (!in_array($fileMimeType, $whitelistType)) {
            $error = true;
        }
        if($error){
            $fileMimeType = mime_content_type($image['tmp_name']);
            if (!in_array($fileMimeType, $whitelistType)) {
                $thisViewModel->sendError("Uploaded file is not a valid image", 400);
            }
        }
        $file['type'] = $fileMimeType;
        $file['folder'] = $auth['type'].'/image/';

        $upload_file[] = $file;

        $cloudinaryConfig = $ConfigurationModel->findByKeyName("cloudinary-configuration");
        $cloudinaryConfig = $cloudinaryConfig['result'][0]->values;
        $result = $thisViewModel->uploadToCloudinary('image', $upload_file, $cloudinaryConfig, $uploadConfig, null);

        $insertData = [];
        $returnResult = [];
        $MediaUploadModel = new MediaUploadModel();
        $MediaUploadModel->setCurrentUser($auth['user_id']);
        
        foreach ($result as $key => $value) {
            $insertData[] = $value;

            $imageUrl = $value['image_url'];
            $imagePath = [
                'base_url'=>$imageUrl['base_url'],
                'pic_file_name'=>$imageUrl['pic_file_name'],
                'pic_small_path'=>$imageUrl['pic_small_path'],
                'pic_medium_path'=>$imageUrl['pic_medium_path'],
                'pic_big_path'=>$imageUrl['pic_big_path'],
            ];
            if(isset($imageUrl['request_pic_path'])) $imagePath['request_pic_path'] = $imageUrl['request_pic_path'];

            $returnResult[] = $imagePath;
        }
        if(count($returnResult) == 1){
            $returnResult = $returnResult[0];
        }
        $insertBatch = $MediaUploadModel->insertBatch($insertData);
        
        $returnResult = ['result'=>$returnResult];
        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
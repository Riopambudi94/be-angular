<?php
namespace App\GeneralData\ViewModels\ShippingRegionViewModel;

use App\GeneralData\ViewModels\CourierViewModel;
use App\GeneralData\Models\AdministrativeRegionModel;
use App\GeneralData\Models\ShippingRegionModel;

function updateShippingRegion($arguments, $thisViewModel) {
    
    $courier = $arguments[0];
    try {

        $CourierViewModel = new CourierViewModel();
        $AdministrativeRegionModel = new AdministrativeRegionModel();
        $ShippingRegionModel = new ShippingRegionModel();

        $notFound = [];
        $newShippingRegion = [];
        switch ($courier) {
            case 'SAP':
                $regions = $CourierViewModel->getSAPRegion();

                foreach ($regions as $key => $value) {
                    $search = [
                        'province'=>trim($value['provinsi_name']),
                        'city'=>trim($value['city_name']),
                        'subdistrict'=>trim($value['district_name'])
                    ];
                    $regionData = $AdministrativeRegionModel->findRegion($search);

                    if (!empty($regionData['result'])) {
                        $regionalCode = [
                            $regionData['result'][0]->subdistrict_code,
                            $regionData['result'][0]->city_code
                        ];
                        $newShippingRegion[] = [
                            'courier_code'=>'REALS-SAP',
                            'region_name'=>$value['district_name'],
                            'region_code'=>$value['district_code'],
                            'usage'=>['origin', 'destination'],
                            'route'=>'domestic',
                            'region_data'=>$value,
                            'regional_code'=>$regionalCode,
                            'regional_name'=>$regionData['result'][0]->subdistrict
                        ];
                    } else {
                        $notFound[] = $value;
                    }
                }

                if (!empty($notFound)) {
                    $newShippingRegion[] = [
                        'courier_code'=>'REALS-SAP',
                        'region_data'=>$notFound,
                        'not_found'=>1,
                        'deleted'=>1
                    ];
                }
                break;
            
            default:
                # code...
                break;
        }

        if (!empty($newShippingRegion)){
            $insert = $ShippingRegionModel->insertBatch($newShippingRegion);
        }

        return ['result'=>'success'];
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
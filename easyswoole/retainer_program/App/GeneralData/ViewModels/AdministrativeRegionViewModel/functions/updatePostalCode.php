<?php
namespace App\GeneralData\ViewModels\AdministrativeRegionViewModel;

use App\GeneralData\Models\AdministrativeRegionModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function updatePostalCode($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];
    try {

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "PRODUCT_UPLOAD");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $AdministrativeRegionModel = new AdministrativeRegionModel();
        $AdministrativeRegionModel->setCurrentUser($auth['user_id']);

        $regExr = "/((?<=\()([a-zA-Z0-9\s\-\'\.]+))|((?<=\))([a-zA-Z0-9\s\-\'\.]+))|((?<=\/)([a-zA-Z0-9\s\-\'\.]+))|(([a-zA-Z0-9\s\-\'\.]+)(?=\/))|([a-zA-Z0-9\s\-\'\.]+)/";

        $notFound = [];
        $updateRegion = [];
        foreach ($files as $row => $data) {
            $provinceName = strtoupper(trim($data['province']));
            $cityName = strtoupper(trim($data['city']));
            $subdistrictName = strtoupper(trim($data['subdistrict']));
            $villageName = strtoupper(trim($data['village']));
            $postCode = trim(strval($data['postal_code']));

            $regionalLevel = substr($cityName, 0, 4);
            $cityName = substr($cityName, 5);
            
            $district = [];
            preg_match_all($regExr, $subdistrictName, $districtList);
            if (count($districtList[0]) > 1) $district[] = ['subdistrict'=>$AdministrativeRegionModel->createRegex($subdistrictName, 'i')];
            foreach ($districtList[0] as $key => $value) {
                $value = trim($value);
                if(empty($value) || strlen($value) <= 3) continue;

                $district[] = ['subdistrict'=>$AdministrativeRegionModel->createRegex($value, 'i')];
            }

            $village = [];
            preg_match_all($regExr, $villageName, $villageList);
            if (count($villageList[0]) > 1) $village[] = ['village'=>$AdministrativeRegionModel->createRegex($villageName, 'i')];
            foreach ($villageList[0] as $key => $value) {
                $value = trim($value);
                if(empty($value) || strlen($value) <= 3) continue;

                $village[] = ['village'=>$AdministrativeRegionModel->createRegex($value, 'i')];
            }

            if (empty($district) || empty($village)) {
                $notFound[] = $data;
                continue;
            }

            $filter = [
                'postal_code'=>['$exists'=>false],
                'province'=>$provinceName,
                'city'=>$cityName,
                'regional_level2'=>$regionalLevel,
                '$and'=>[
                    ['$or'=>$district],
                    ['$or'=>$village],
                ]
            ];
            $region = $AdministrativeRegionModel->DBfind($filter);
            if (empty($region['result'])) {
                $notFound[] = $data;
                continue;
            }

            $updateRegion[] = [
                'filter'=>['_id'=>$AdministrativeRegionModel->convertToObjectId($region['result'][0]->_id)],
                'new_value'=>[
                    'postal_code'=>$postCode
                ]
            ];
        }

        !empty($updateData) ?
            $AdministrativeRegionModel->updateBatch($updateRegion) : null;

        if (!empty($notFound)) {
            $data = [
                'region' => $notFound,
                'deleted'=>1
            ];
            $AdministrativeRegionModel->insert($data);
        }
        
        return ['result'=>$notFound];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
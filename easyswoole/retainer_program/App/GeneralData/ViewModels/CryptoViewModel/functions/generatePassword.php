<?php
namespace App\GeneralData\ViewModels\CryptoViewModel;

function generatePassword($arguments, $thisViewModel) {
    $password = $arguments[0];
    $cost = $arguments[1];

    try {

        empty($password) ? 
            $thisViewModel->sendError("Invalid Password", 400): null;

        $algo = PASSWORD_BCRYPT;

        empty($cost) ? $cost = 12: null;

        $option = ['cost'=>$cost];
        $resultPassword = password_hash($password, $algo, $option);
        if (!$resultPassword) {
            $thisViewModel->sendError("Password Denied", 400);
        }
        return $resultPassword;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
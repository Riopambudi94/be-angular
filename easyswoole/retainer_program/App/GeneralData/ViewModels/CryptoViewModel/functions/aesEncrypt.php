<?php
namespace App\GeneralData\ViewModels\CryptoViewModel;

use Services\Aes;

function aesEncrypt($arguments, $thisViewModel) {
    $secret = $arguments[0];
    $hashKey = $arguments[1];
    try {

        if (empty($secret)) {
            $thisViewModel->sendError("secret must not be empty", 400);
        }

        empty($hashKey) ? $hashKey = $thisViewModel->aesKey : null;

        $Aes = new Aes();

        $CURRENT_HASH = explode(":", $hashKey);
       
        $key1   = $CURRENT_HASH[0];
        $key2   = $CURRENT_HASH[1];
    
        $Aes->run($key1, 'CBC', $key2);

        $encryptedPassword = base64_encode($Aes->encrypt($secret));

        return $encryptedPassword;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
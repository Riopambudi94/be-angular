<?php
namespace App\GeneralData\ViewModels\LabelViewModel;

use Services\Barcode;

function generateQrCode($arguments, $thisViewModel) {
    $data = $arguments[0];
    $format = $arguments[1]?:'jpeg';

    try {
        $Barcode = Barcode::getInstance();
        $name = uniqid();
        $fileName = "qrcode-".$name;
        $date = date("Y-m-d");
        $time = date("Y-m-d-H");

        $assetDir = 'assets'.DIRECTORY_SEPARATOR;
        $folder = "evoucher".DIRECTORY_SEPARATOR."qrcode".DIRECTORY_SEPARATOR."{$date}".DIRECTORY_SEPARATOR."{$time}";
        $fileDir = PROJECT_PATH.$assetDir.$folder;
        $filePath = $fileDir.DIRECTORY_SEPARATOR.$fileName.".".$format;

        if(!is_dir($fileDir)){
            mkdir($fileDir, 0777, true);
        }

        $mimeType = '';
        switch ($format) {
            case 'jpeg':
            case 'jpg':
                $image = $Barcode->render_image('qr-h', $data, ['sf'=>5.3]);
                imagejpeg($image, $filePath);
                imagedestroy($image);
                $mimeType = 'image/jpg';
                break;

            case 'png':
                $image = $Barcode->render_image('qr-h', $data, ['sf'=>5.3]);
                imagepng($image, $filePath);
                imagedestroy($image);
                $mimeType = 'image/png';
                break;
                
            case 'gif':
                $image = $Barcode->render_image('qr-h', $data, ['sf'=>5.3]);
                imagegif($image, $filePath);
                imagedestroy($image);
                $mimeType = 'image/gif';
                break;

            case 'svg':
                $image = $Barcode->render_svg('qr-h', $data, ['sf'=>5.3]);
                file_put_contents($filePath, $image);
                $mimeType = 'image/svg+xml';
                break;

            default:
                $thisViewModel->sendError("invalid image format", 400);
                break;
        }

        $result = [
            'file_path'=>$filePath,
            'file_name'=>$fileName,
            'folder'=>$folder,
            'mime_type'=>$mimeType
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
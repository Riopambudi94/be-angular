<?php
namespace App\GeneralData\Models;

use Infrastructure\MongoModel;

class CourierListModel extends MongoModel
{
    public $table;
    public $dbName = 'general_data';
    public $collectionName = "dtm_courier_list";

    public function findByCourierCode($courierCode)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $filter = ['courier_code'=>$courierCode];
            return $this->DBfind($filter);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function getAvailableCourierList () {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try{
            $pipeline = [
                [
                    '$project' => [
                        '_id'=>0,
                        'courier' => '$courier',
                        'courier_code' => '$courier_code',
                        'delivery_service'=>'$delivery_service',
                        'delivery_method'=>'$delivery_method',
                        'default_origin'=>'$default_origin'
                    ]
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function findAll()
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBfind();
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function update($_id, $getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBupdate(['_id'=> new MongoDB\BSON\ObjectId("$_id")], $getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function remove($_id)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBdelete(['_id'=> new MongoDB\BSON\ObjectId("$_id")], $getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function insert($getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }
}

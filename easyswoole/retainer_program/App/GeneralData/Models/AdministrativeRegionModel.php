<?php
namespace App\GeneralData\Models;

use Infrastructure\MongoModel;

class AdministrativeRegionModel extends MongoModel {
    public $table;
    public $dbName = 'general_data';
    public $collectionName = "dtm_administrative_region";

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findRegion($search) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $pipeline = [];

            $match = [];
            if (!empty($search['province'])) {
                $match['province'] = ['$regex'=>$search['province'], '$options'=>'i'];
            }
            if (!empty($search['city'])) {
                $match['city'] = ['$regex'=>$search['city'], '$options'=>'i'];
            }
            if (!empty($search['subdistrict'])) {
                $match['subdistrict'] = ['$regex'=>$search['subdistrict'], '$options'=>'i'];
                /* $match['$or'] = [
                    ['subdistrict'=>['$regex'=>$search['subdistrict'], '$options'=>'i']],
                    ['village'=>['$regex'=>$search['subdistrict'], '$options'=>'i']]
                ]; */
            }
            if (!empty($search['village'])) {
                $match['village'] = ['$regex'=>$search['village'], '$options'=>'i'];
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findRegionByCode($regionCode){
        try{
            if(empty($regionCode)) return false;

            if(!is_array($regionCode)){
                $regionCode = array($regionCode);
            }
            $filter = [
                '$or'=>[
                    ['village_code'=>['$in'=>$regionCode]],
                    ['subdistrict_code'=>['$in'=>$regionCode]],
                    ['city_code'=>['$in'=>$regionCode]],
                    ['province_code'=>['$in'=>$regionCode]],
                ]
            ];

            $result = $this->DBfind($filter);
            return $result;
        }
        catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }

            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function groupByProvince($province = '') {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $pipeline = [];
            if (!empty($province)) {
                $pipeline[] = [
                    '$match'=>[
                        'province'=>['$regex'=>$province, '$options'=>'i']
                    ]
                ];
            }

            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$unwind'=>'$province'
                    ],
                    [
                        '$group'=>[
                            '_id'=>'$province',
                            'province'=>['$first'=>'$province'],
                            'province_code'=>['$first'=>'$province_code']
                        ]
                    ],
                    [
                        '$project'=>[
                            '_id'=>0
                        ]
                    ]
                ]
            );

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function groupByCity($provinceCode = '', $city = '') {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $pipeline = [];
            $match = [];
            if (!empty($provinceCode)) {
                $match['province_code'] = $provinceCode;
            }
            if (!empty($city)) {
                $match['city'] = ['$regex'=>$city, '$options'=>'i'];
            }

            !empty($match) ? $pipeline[] = ['$match'=>$match] : null;
            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$unwind'=>'$city'
                    ],
                    [
                        '$group'=>[
                            '_id'=>[
                                'city'=>'$city',
                                'regional_level'=>'$regional_level2'
                            ],
                            /* 'city'=>[
                                '$first'=>[
                                    '$concat'=>['$regional_level2', ' ', '$city']
                                ]
                            ], */
                            'city'=>['$first'=>'$city'],
                            'city_code'=>['$first'=>'$city_code'],
                            'regional_level'=>['$first'=>'$regional_level2']
                        ]
                    ],
                    [
                        '$project'=>[
                            '_id'=>0
                        ]
                    ]
                ]
            );

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function groupBySubdistrict($cityCode = '', $subdistrict = '') {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $pipeline = [];
            $match = [];
            if (!empty($cityCode)) {
                $match['city_code'] = $cityCode;
            }
            if (!empty($subdistrict)) {
                $match['subdistrict'] = ['$regex'=>$subdistrict, '$options'=>'i'];
            }

            !empty($match) ? $pipeline[] = ['$match'=>$match] : null;
            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$unwind'=>'$subdistrict'
                    ],
                    [
                        '$group'=>[
                            '_id'=>'$subdistrict',
                            'subdistrict'=>['$first'=>'$subdistrict'],
                            'subdistrict_code'=>['$first'=>'$subdistrict_code']
                        ]
                    ],
                    [
                        '$project'=>[
                            '_id'=>0
                        ]
                    ]
                ]
            );

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function groupByVillage($subdistrictCode = '', $village = '') {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $pipeline = [];
            $match = [];
            if (!empty($subdistrictCode)) {
                $match['subdistrict_code'] = $subdistrictCode;
            }
            if (!empty($village)) {
                $match['village'] = ['$regex'=>$village, '$options'=>'i'];
            }

            !empty($match) ? $pipeline[] = ['$match'=>$match] : null;
            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$unwind'=>'$village'
                    ],
                    [
                        '$group'=>[
                            '_id'=>'$village',
                            'village'=>['$first'=>'$village'],
                            'village_code'=>['$first'=>'$village_code']
                        ]
                    ],
                    [
                        '$project'=>[
                            '_id'=>0
                        ]
                    ]
                ]
            );

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
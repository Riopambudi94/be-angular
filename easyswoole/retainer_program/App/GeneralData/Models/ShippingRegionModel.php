<?php
namespace App\GeneralData\Models;

use Infrastructure\MongoModel;

class ShippingRegionModel extends MongoModel {
    public $table;
    public $dbName = 'general_data';
    public $collectionName = "dtm_shipping_region";

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByRegionCode($param) {
        try{

            (!is_array($param['region_code'])) ? 
                $param['region_code'] = array($param['region_code']) : null;

            $filter = [
                'courier_code'=>$param['courier_code'],
                'usage'=>$param['usage'],
                'regional_code'=>['$in'=>$param['region_code']]
            ];
            $result = $this->DBfind($filter);

            return $result;
        }
        catch(\Exception $e){
            return $this->sendError( $e->getMessage(), 500);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }

            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
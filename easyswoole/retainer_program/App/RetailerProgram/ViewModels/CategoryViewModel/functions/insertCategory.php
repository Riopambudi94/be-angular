<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function insertCategory($arguments, $thisViewModel)
{
    $auth = $arguments[0];
    $bodyData = $arguments[1];
    $allowedField = array("name","type","code","key");
    $allowedType = array('product','voucher','e-voucher');
    
    try {
        $setData = [];
        foreach($allowedField as $key => $value){
            if(!empty($bodyData[$value])) {
                $setData[$value] = $bodyData[$value];
            }else {
                $thisViewModel->sendError("{$value} field is empty", 404);
            }
        }

        $setData['type'] = strtolower($setData['type']); //change type value to lowercase;
        
        if(!in_array($setData['type'], $allowedType)) $thisViewModel->sendError('value of type field not allowed', 404); //validated value of type field;
        
        $setData['key'] = strtolower(str_replace(' ', '-', $setData['name'])); // change space to dash for key field and to lowercase;

        $CategoryModel = new CategoryModel();

        $CategoryModel->setCurrentUser($auth['user_id']);

        $find = $CategoryModel->findByCategoryCodeOrName([$setData['name'],$setData['key'], $setData['code']], ['type' => $setData['type']]);

        if(!empty($find['result'])){
            foreach($find['result'] as $key => $category){
                if($category->name == $setData['name'] || $category->code == $setData['code'] || $category->key == $setData['key']){
                    $thisViewModel->sendError('Category already exists', 404);
                }
            }
        }
            $result = $CategoryModel->insert($setData);
            
            return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

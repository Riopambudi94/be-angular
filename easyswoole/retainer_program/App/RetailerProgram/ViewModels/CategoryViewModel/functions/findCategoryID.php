<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function findCategoryID($arguments, $thisViewModel) {
    $id = $arguments[0];
    try {

        $CategoryModel = new CategoryModel();

        $result = $CategoryModel->findByID($id);
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
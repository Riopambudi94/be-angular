<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function updateCategory($arguments, $thisViewModel)
{
    $auth = $arguments[0];
    $keyOrCode = $arguments[1];
    $bodyData = $arguments[2];
    $allowedField = array("type","name","code","key");
    $allowedType = array('product','voucher','e-voucher');

    try {

        if(empty($bodyData)) $thisViewModel->sendError('No record updated', 404);


            $setData = [];
            foreach($allowedField as $key => $value){
                if(!empty($bodyData[$value])){
                    $setData[$value] = $bodyData[$value];
                }else {
                    $thisViewModel->sendError("{$value} field is empty", 404);
                }
            }

            $setData['type'] = strtolower($setData['type']); //change type value to lowercase;
        
            if(!in_array($setData['type'], $allowedType)) $thisViewModel->sendError('value of type field not allowed', 404); //validated value of type field;
            
            $setData['key'] = strtolower(str_replace(' ', '-', $setData['name'])); // change space to dash for key field and to lowercase;

            $CategoryModel = new CategoryModel();
            
            $CategoryModel->setCurrentUser($auth['user_id']);

            $find = $CategoryModel->findByCategoryCodeOrName([$setData['name'],$setData['key']], ['type' => $setData['type']]);

            if(!empty($find['result'])){
               $thisViewModel->sendError('Category already exists', 404);
            }

            $findCat = $CategoryModel->findByCategoryCodeOrName($keyOrCode);

            if(empty($findCat['result'])) $thisViewModel->sendError("Category {$keyOrCode} does not exists", 404);

            $ids = [];

            if(count($findCat['result']) > 1){
                foreach ($findCat['result'] as $key => $category) {
                    if($category->type == $setData['type']) $ids[] = $CategoryModel->convertToObjectId($category->_id);
                }
            } else {
                foreach ($findCat['result'] as $key => $category) {
                    $ids[] = $CategoryModel->convertToObjectId($category->_id);
                }
            }

            unset($setData['code']);
            $result = $CategoryModel->update(['_id'=>['$in'=>$ids]], $setData);
            return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

<?php

namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;
use App\RetailerProgram\Models\MemberModel;

function createNewShoppingCart($arguments, $thisViewModel) {

    $userId = $arguments[0];

    try{

        $MemberModel = new MemberModel();

        $member = $MemberModel->find(['_id' => $userId]);

        if(empty($member['result'])) $thisViewModel->sendError("No User Data Found", 400);

        $ShoppingcartModel = new ShoppingcartModel();
        $ShoppingcartModel->setCurrentUser($userId);

        return $ShoppingcartModel->createNewShoppingCartByUserID($userId);

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
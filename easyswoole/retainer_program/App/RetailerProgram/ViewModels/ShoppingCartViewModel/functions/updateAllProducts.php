<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function updateAllProducts($arguments, $thisViewModel)
{
    $auth = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $ShoppingcartModel = new ShoppingcartModel();
        $ShoppingcartModel->setCurrentUser($auth['user_id']);
        
        $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        if (empty($activeSC['result'])) {
            $createNewSC = $ShoppingcartModel->createNewShoppingCartByUserID($auth['subject_id']);
            $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        }
        $activeSC = $thisViewModel->objectToArray($activeSC['result'][0]);
        
        $validateProduct = $thisViewModel->validateProducts($bodyData['products']);

        $bodyData = array_merge($bodyData, $validateProduct);
        $updateSC = $ShoppingcartModel->update($activeSC['_id'], $bodyData);

        $result = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

<?php
namespace App\RetailerProgram\ViewModels;

class ViewModelMaster {

    public function __call($funcName, $arguments)
    {
        $currentClass = get_called_class();

        // Note: value of $name is case sensitive.
        if($this->loadFunction($funcName)){
            $function = "$currentClass\\$funcName";
            
            eval("\$function = \$function(\$arguments, \$this);");
            return $function;
        }
    }

    private function loadFunction($functionName){
        $namespace = explode("\\", get_called_class());

        $currentDir     = PROJECT_PATH;
        $vmDir          = $namespace[0].DIRECTORY_SEPARATOR.$namespace[1].DIRECTORY_SEPARATOR.$namespace[2].DIRECTORY_SEPARATOR.$namespace[3];
        $functionDir    = 'functions';
        $functionFile   = $currentDir.$vmDir.DIRECTORY_SEPARATOR.$functionDir.DIRECTORY_SEPARATOR.$functionName.'.php';

        if(!file_exists($functionFile)) {
            throw new \Exception("error ".$functionName." doesn't exists", 1);
        }
        @include_once($functionFile);

        return true;
    }

    function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }
            
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(array($this,__FUNCTION__), $d);
        } else {
            // Return array
            return $d;
        }
    }

    function sendError($errorMessage, $errorCode, \Throwable $e = null){
        // $this->error_code = $e->getMessage();
        // $this->error = $e->getCode();
        
        if ($e && get_class($e) == "Error") {
            throw new \Error($errorMessage, $errorCode, $e);
        } else {
            throw new \Exception($errorMessage, $errorCode, $e);
        }
    }
}
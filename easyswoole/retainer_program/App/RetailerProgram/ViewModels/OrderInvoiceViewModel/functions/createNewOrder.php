<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\ViewModels\MemberViewModel;
use App\RetailerProgram\ViewModels\ShoppingCartViewModel;

function createNewOrder($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $MemberViewModel = new MemberViewModel();
        $address = $MemberViewModel->updateMemberAddress($bodyData['address_detail']);

        $ShoppingCartViewModel = new ShoppingCartViewModel();
        $activeSC = $ShoppingCartViewModel->findActiveCart($auth['subject_id'], true)['result'];

        $updateSC = [
            'address_detail'=>$bodyData['address_detail'],
            'products'=>$activeSC['products']
        ];
        /**
         * validate products
         */
        $lastSCData = $ShoppingCartViewModel->updateAllProducts($auth, $updateSC);
        $lastSCData = $thisViewModel->objectToArray($lastSCData['result'][0]);
        if($lastSCData['product_os']>0 || $lastSCData['product_is']>0)
            return $result = ['result'=>$lastSCData];

        $orderInvoice = $thisViewModel->createOrderInvoice($auth, $lastSCData);
        
        $updateSC = $ShoppingCartViewModel->processOrderFromShoppingcart($activeSC['_id'], $orderInvoice);
        $createNewCart = $ShoppingCartViewModel->createNewShoppingCart($auth['subject_id']);

        $activeSC = $ShoppingCartViewModel->findActiveCart($auth['subject_id'], true);
        return $activeSC;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
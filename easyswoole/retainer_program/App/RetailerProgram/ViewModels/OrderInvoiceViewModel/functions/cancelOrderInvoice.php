<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;
use App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;
use App\RetailerProgram\ViewModels\ProductViewModel;

function cancelOrderInvoice($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);

        $PointsTransactionViewModel = new PointsTransactionViewModel();
        $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();
        $ProductViewModel = new ProductViewModel();

        $invoice = $OrderInvoiceModel->findByOrderId($bodyData['order_id'], ['status'=>['PENDING','PROCESSED']]);
        empty($invoice['result']) ?
            $thisViewModel->sendError("order data not found", 404) :
            $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

        $userId = $OrderInvoiceModel->convertToObjectId($auth['user_id']);
        $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));

        $DeliveryTrackingViewModel->cancelDeliveryOrder($auth, $bodyData['order_id']);

        $productsRefund = [];
        foreach ($invoice['products'] as $product) {
            $productsRefund[] = $product['sku_code'];
        }

        $PointsTransactionViewModel->refundRedeemedPoints($auth, $invoice, $productsRefund);

        $ProductViewModel->stockReturnOnOrderInvoice($auth, $invoice);
        
        $invoiceData = [
            'status'=>'CANCEL',
            'cancel_date'=>$currDate,
            'cancel_at'=>getClientIpAddr(),
            'cancel_by'=>$userId
        ];
        $result = $OrderInvoiceModel->updateByID($invoice['_id'], $invoiceData);
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findAllReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {
        $OrderInvoiceModel = new OrderInvoiceModel();

        // utk urutan output
        if (!isset($requested_columns)) {
            $requested_columns = "_id,order_id,status,products,address_detail,shipping_info,delivery_detail,return_detail,redelivery_detail,order_expire_date,request_date,approve_date,cancel_date,username,member_detail,sum_total,total_quantity,last_shipping_info,additional_info,invoice_no,po_no";
        }
        $requested_columns = explode(",", $requested_columns);

        $OrderInvoiceModel->requestColumns .= ',username,member_detail,last_shipping_info';
        $OrderInvoiceModel->requestMapping = array_merge(
            $OrderInvoiceModel->requestMapping,
            [
                '_id'=>0,
                'username'=>'$username',
                'member_detail'=>'$member_detail',
                'last_shipping_info'=>'$last_shipping_info'
            ]
        );

        if ($download) {
            $result_db = $OrderInvoiceModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, 'ORDER_INVOICE_REPORT');
            }
            
        } else {
            $result = $OrderInvoiceModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
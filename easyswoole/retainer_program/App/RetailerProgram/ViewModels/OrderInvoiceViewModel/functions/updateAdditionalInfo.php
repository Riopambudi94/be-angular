<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\GeneralData\ViewModels\MediaViewModel;
use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\Models\MemberModel;

function updateAdditionalInfo($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderId = $arguments[1];
    $bodyData = $arguments[2];
    try {

        $option = $bodyData['option']?:'edit';

        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);

        $invoice = $OrderInvoiceModel->findByOrderId($orderId);
        empty($invoice['result']) ?
            $thisViewModel->sendError("Order Data Not Found", 404):
            $invoice = $thisViewModel->objectToArray($invoice['result'][0]);
            
        $invoice['additional_info'] = (!empty($invoice['additional_info'])) ? $invoice['additional_info'] : [];

        $currDate = date('Y-m-d H:i:s');
        $userId = $auth['user_id'];
        $additionalInfo = [];

        $MediaViewModel = new MediaViewModel();

        if (!empty($bodyData['bast'])) {

            if (strpos($bodyData['bast']['type'], 'image') !== false) {
                $image = $MediaViewModel->uploadImage($auth, $bodyData['bast'])['result'];
                $url = $image['base_url'].$image['pic_big_path'];
            } else {
                $file = $MediaViewModel->uploadDocument($auth, $bodyData['bast']);
                $url = $file['result'];
            }
            $additionalInfo['bast'] = [
                'pic_file_name'=>$url,
                'created_date'=>$currDate,
                'created_by'=>$userId
            ];
        }
        if (!empty($bodyData['surat_kuasa'])) {

            if (strpos($bodyData['surat_kuasa']['type'], 'image') !== false) {
                $image = $MediaViewModel->uploadImage($auth, $bodyData['surat_kuasa'])['result'];
                $url = $image['base_url'].$image['pic_big_path'];
            } else {
                $file = $MediaViewModel->uploadDocument($auth, $bodyData['surat_kuasa']);
                $url = $file['result'];
            }
            $additionalInfo['surat_kuasa'] = [
                'pic_file_name'=>$url,
                'created_date'=>$currDate,
                'created_by'=>$userId
            ];
        }
        if (!empty($bodyData['others'])) {

            if (strpos($bodyData['others']['type'], 'image') !== false) {
                $image = $MediaViewModel->uploadImage($auth, $bodyData['others'])['result'];
                $url = $image['base_url'].$image['pic_big_path'];
            } else {
                $file = $MediaViewModel->uploadDocument($auth, $bodyData['others']);
                $url = $file['result'];
            }
            $additionalInfo['others'] = [
                'id'=>$OrderInvoiceModel->convertToObjectId(null, true)->__toString(),
                'pic_file_name'=>$url,
                'created_date'=>$currDate,
                'created_by'=>$userId
            ];
        }
        if (!empty($bodyData['receipt'])) {

            if (strpos($bodyData['receipt']['type'], 'image') !== false) {
                $image = $MediaViewModel->uploadImage($auth, $bodyData['receipt'])['result'];
                $url = $image['base_url'].$image['pic_big_path'];
            } else {
                $file = $MediaViewModel->uploadDocument($auth, $bodyData['receipt']);
                $url = $file['result'];
            }
            $additionalInfo['receipt'] = [
                'receipt_id'=>$OrderInvoiceModel->convertToObjectId(null, true)->__toString(),
                'pic_file_name'=>$url,
                'created_date'=>$currDate,
                'created_by'=>$userId
            ];
        }

        if (!empty($additionalInfo) || ($option == 'delete' && ($bodyData['id'] || $bodyData['receipt_id']))) {
            $additionalData = $additionalInfo;

            $MemberModel = new MemberModel();
            $MemberModel->setCurrentUser($auth['user_id']);
            
            $memberData = $MemberModel->findByUsernameOrId($invoice['user_id']);
            $memberData = $thisViewModel->objectToArray($memberData['result'][0]);

            if (!empty($memberData['additional_info'])) {
                if (!empty($additionalInfo['bast'])) {

                    if (empty($memberData['additional_info']['bast']))
                        $memberData['additional_info']['bast'] = [];
                    else {
                        foreach ($memberData['additional_info']['bast'] as $key => $value) {
                            if($value['order_id'] == $invoice['order_id']) {
                                unset($memberData['additional_info']['bast'][$key]);
                                break;
                            }
                        }
                    }
                    $additionalInfo['bast']['order_id'] = $invoice['order_id'];
                    $memberData['additional_info']['bast'][] = $additionalInfo['bast'];
                }

                if (!empty($additionalInfo['surat_kuasa'])) {
                    if (empty($memberData['additional_info']['surat_kuasa']))
                        $memberData['additional_info']['surat_kuasa'] = [];
                    else {
                        foreach ($memberData['additional_info']['surat_kuasa'] as $key => $value) {
                            if($value['order_id'] == $invoice['order_id']) {
                                unset($memberData['additional_info']['surat_kuasa'][$key]);
                                break;
                            }
                        }
                    }
                    $additionalInfo['surat_kuasa']['order_id'] = $invoice['order_id'];
                    $memberData['additional_info']['surat_kuasa'][] = $additionalInfo['surat_kuasa'];    
                }

            } else {
                $memberData['additional_info'] = [];

                if (!empty($additionalInfo['bast'])) {
                    if (empty($memberData['additional_info']['bast'])) 
                        $memberData['additional_info']['bast'] = [];
                    $additionalInfo['bast']['order_id'] = $invoice['order_id'];
                    $memberData['additional_info']['bast'][] = $additionalInfo['bast'];
                }

                if (!empty($additionalInfo['surat_kuasa'])) {
                    if (!empty($memberData['additional_info']['surat_kuasa']))
                        $memberData['additional_info']['surat_kuasa'] = [];
                    $additionalInfo['surat_kuasa']['order_id'] = $invoice['order_id'];
                    $memberData['additional_info']['surat_kuasa'][] = $additionalInfo['surat_kuasa'];
                }

            }
            
            if (!empty($invoice['additional_info']['others'])) {

                foreach ($invoice['additional_info']['others'] as $key => $value) {
                    if ($value['id'] == $bodyData['id']) {
                        if ($option == 'delete') {
                            unset($invoice['additional_info']['others'][$key]);        
                        } elseif (isset($additionalData['others']) && $option == 'edit') {
                            $invoice['additional_info']['others'][$key] = $additionalData['others'];
                        }
                        unset($additionalData['others']);
                        break;
                    }
                }

                $invoice['additional_info']['others'] = array_values($invoice['additional_info']['others']);
            } elseif ($option == 'edit') {
                $invoice['additional_info']['others'] = [];
            }

            if (isset($additionalData['others']) && $option == 'edit') {
                $invoice['additional_info']['others'][] = $additionalData['others'];
                unset($additionalData['others']);
            }

            if (!empty($invoice['additional_info']['receipt'])) {

                foreach ($invoice['additional_info']['receipt'] as $key => $value) {
                    if ($value['receipt_id'] == $bodyData['receipt_id']) {
                        if ($option == 'delete') {
                            unset($invoice['additional_info']['receipt'][$key]);        
                        } elseif (isset($additionalData['receipt']) && $option == 'edit') {
                            $invoice['additional_info']['receipt'][$key] = $additionalData['receipt'];
                        }
                        unset($additionalData['receipt']);
                        break;
                    }
                }

                $invoice['additional_info']['receipt'] = array_values($invoice['additional_info']['receipt']);
            } elseif ($option == 'edit') {
                $invoice['additional_info']['receipt'] = [];
            }

            if (isset($additionalData['receipt']) && $option == 'edit') {
                $invoice['additional_info']['receipt'][] = $additionalData['receipt'];
                unset($additionalData['receipt']);
            }

            $invoice['additional_info'] = array_merge($invoice['additional_info'], $additionalData);
            
            if (!empty($memberData['additional_info']['others'])) {
                foreach ($memberData['additional_info']['others'] as $key => $value) {
                    if ($value['id'] == $bodyData['id']) {
                        if ($option == 'delete') {
                            unset($memberData['additional_info']['others'][$key]);        
                        } elseif (isset($additionalInfo['others']) && $option == 'edit') {
                            $memberData['additional_info']['others'][$key] = $additionalInfo['others'];
                        }
                        unset($additionalInfo['others']);
                        break;
                    }
                }

                $memberData['additional_info']['others'] = array_values($memberData['additional_info']['others']);
            } elseif ($option == 'edit') {
                $memberData['additional_info']['others'] = [];
            }

            if (isset($additionalInfo['others']) && $option == 'edit') {
                $memberData['additional_info']['others'][] = $additionalInfo['others'];
            }

            $setData = ['additional_info'=>$memberData['additional_info']];
            $updateMember = $MemberModel->updateByID($memberData['_id'], $setData);

            $setData = ['additional_info'=>$invoice['additional_info']];
            $result = $OrderInvoiceModel->updateByID($invoice['_id'], $setData);

        }
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
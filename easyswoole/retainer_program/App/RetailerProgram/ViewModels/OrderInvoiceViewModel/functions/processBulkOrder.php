<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\Models\MemberModel;
use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;
use App\RetailerProgram\Models\ShoppingcartModel;
use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;
use Services\SpreadsheetService;

function processBulkOrder($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {
        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "ORDER_BULK");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'auth'=>$auth,
            'files'=>$files
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            $result = processXlsx($data, $thisViewModel);
        } elseif (in_array($document['ext'], ['csv'])) {
            
        }

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processXlsx($data, &$thisViewModel) {
    try {
        $userID = $data['auth']['user_id'];

        $MemberModel = new MemberModel();
        $ProductModel = new ProductModel();
        $ProductSkuModel = new ProductSkuModel();

        $ShoppingcartModel = new ShoppingcartModel();
        $ShoppingcartModel->setCurrentUser($userID);
        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($userID);
        $EvoucherModel = new EvoucherModel();
        $EvoucherModel->setCurrentUser($userID);

        $PointsTransactionViewModel = new PointsTransactionViewModel();

        $productList = [];
        $orderList = [];
        $evoucherList = [];
        foreach ($data['files'] as $key => $value) {
            $rowNum = $key;

            $memberId = trim($value['member_id']);
            $productCode = trim($value['product_code']);
            $skuCode = trim($value['sku_code']);
            $itemQty = intval(trim($value['item_qty']));

            empty($memberId) ?
                $thisViewModel->sendError("member id value on row {$rowNum} is required", 400): null;

            if (!isset($orderList[$memberId])) {
                $member = $MemberModel->findByUsernameOrId($memberId);
                if (empty($member['result'])) {
                    $thisViewModel->sendError("member id {$value['member_id']} on row {$rowNum} does not exists", 404);
                }
                $member = $thisViewModel->objectToArray($member['result'][0]);

                $newActiveSC = $ShoppingcartModel->createNewCart($member['_id']);
                $member['member_address'] = array_reverse($member['member_address']);
                foreach ($member['member_address'] as $key => $value) {
                    if ($value['use_as'] == 'main_address') {
                        $newActiveSC['address_detail'] = $value;        
                        /* if (empty($value['name']) || empty($value['cell_phone']) || empty($value['address']) || empty($value['postal_code'])) {
                            $thisViewModel->sendError("member {$value['member_id']} address detail is not complete", 400);
                        } */
                    }
                }

                if (empty($newActiveSC['address_detail'])) {
                    $thisViewModel->sendError("member {$value['member_id']} address detail is required", 400);
                }

                $orderList[$memberId] = $newActiveSC;
            }

            if (!isset($productList[$productCode.$skuCode])) {
                $product = $ProductModel->findByProductCodeOrId($productCode,'ACTIVE');
                if (empty($product['result'])) {
                    $thisViewModel->sendError("product code {$productCode} is not available", 404);
                }
                $product = $thisViewModel->objectToArray($product['result'][0]);

                $match = [
                    'product_code'=>$productCode,
                    'sku_code'=>$skuCode,
                    'status'=>'ACTIVE'
                ];
                $sku = $ProductSkuModel->findProductSku($match);
                if (empty($sku['result'])) {
                    $thisViewModel->sendError("product sku {$skuCode} is not available", 404);
                }
                $sku = $thisViewModel->objectToArray($sku['result'][0]);

                $productList[$productCode.$skuCode] = array_merge($product, $sku);
            }

            $productPrice = $itemQty * $productList[$productCode.$skuCode]['sku_value'];
            if (empty($orderList[$memberId]['products'][$productCode.$skuCode])) {
                $orderList[$memberId]['products'][$productCode.$skuCode] = array_merge(
                    $productList[$productCode.$skuCode],
                    [
                        'quantity'=>$itemQty,
                        'total_product_price' => $productPrice
                    ]
                );
            } else {
                $orderList[$memberId]['products'][$productCode.$skuCode]['quantity'] += $itemQty;
                $orderList[$memberId]['products'][$productCode.$skuCode]['total_product_price'] += $productPrice;
            }
            $productList[$productCode.$skuCode]['qty'] -= $itemQty;
            if ($productList[$productCode.$skuCode]['qty'] < 0) {
                $thisViewModel->sendError("product sku {$skuCode} stock quantity is insufficient", 400);
            }

            if ($productList[$productCode.$skuCode]['type'] == 'e-voucher') {
                !isset($evoucherList[$memberId])? $evoucherList[$memberId] = [] : null;

                $getEvoucher = $EvoucherModel->getAvailableStock($productCode, $skuCode, $itemQty);

                foreach ($getEvoucher['result'] as $sk => $sval) {
                    $evoucherList[$memberId][] = $EvoucherModel->convertToObjectId($sval->_id);
                }
            }

            $orderList[$memberId]['total_price'] += $productPrice;
            $orderList[$memberId]['total_quantity'] += $itemQty;
            $orderList[$memberId]['billings']['total_price'] += $productPrice;
            $orderList[$memberId]['billings']['sum_total'] += $productPrice;
        }

        $expTime = time() + (3600 * 3);
        $expDate = date("Y-m-d H:i:s", $expTime);
        $clientIpAddr = getClientIpAddr();

        $orderData = [];
        $pointsData = [];
        $updateBatch = [];
        $updateEvoucher = [];
        $updateProduct = [];
        foreach ($orderList as $key => $value) {
            $updateBatch[] = [
                'filter'=>[
                    'user_id'=>$value['user_id'],
                    'status'=>'ACTIVE'
                ],
                'new_value'=>['status'=>'INACTIVE']
            ];

            $orderList[$key]['products'] = $value['products'] = array_values($value['products']);

            $orderId = $thisViewModel->createOrderId($data['auth']['app_prefix']);
            $orderData[] = [
                'user_id' => $value['user_id'],
                'products' => $value['products'],
                'address_detail'=>$value['address_detail'],
                'order_id' => $orderId,
                'status'=>'PENDING',
                'sum_total'=>$value['total_price'],
                'total_quantity'=>$value['total_quantity'],
                'shipping_info'=>[],
                'delivery_detail'=>[],
                'shoppingcart_id'=>$OrderInvoiceModel->convertToObjectId($value['_id']),
                'order_expire_date'=>[
                    'in_seconds'=>$expTime,
                    'in_string'=>$expDate
                ],
                'request_date'=>$OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                'request_at'=>$clientIpAddr,
                'request_by'=>$value['user_id']
            ];

            foreach ($value['products'] as $product){
                $pointsData[] = array(
                    'member_id'=>$key,
                    'user_id'=>$value['user_id'],
                    'points'=>$product['total_product_price'],
                    'reference_no'=>$orderId.'-'.$product['sku_code']
                );
            }

            if (isset($evoucherList[$key])) {
                $updateEvoucher[] = [
                    'filter'=>['_id'=>['$in'=>$evoucherList[$key]]],
                    'new_value'=>[
                        'qty_out'=>1,
                        'status'=>'PENDING',
                        'owner_id'=>$value['user_id'],
                        'reference_no'=>$orderId,
                    ]
                ];
            }
        }

        foreach ($productList as $key => $value) {
            $updateProduct[] = [
                'filter'=>['_id'=>$ProductSkuModel->convertToObjectId($value['_id'])],
                'new_value'=>['qty'=>$value['qty']]
            ];
        }

        $result = $ShoppingcartModel->updateBatch($updateBatch);
        $insertSC = $ShoppingcartModel->insertBatch($orderList);

        $PointsTransactionViewModel->pointsRedeemption($data['auth'], $pointsData);
        $ProductSkuModel->updateBatch($updateProduct);
        !empty($updateEvoucher) ? $EvoucherModel->updateBatch($updateEvoucher) : null;
        $createOrder = $OrderInvoiceModel->insertBatch($orderData);

        $scID = [];
        foreach ($insertSC['result']['_id'] as $key => $value) {
            $scID[] = $ShoppingcartModel->convertToObjectId($value);
        }
        $filter = ['_id'=>['$in'=>$scID]];
        $updateSC = $ShoppingcartModel->DBupdate($filter,['status'=>'INACTIVE']);

        return ['result'=>'success'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
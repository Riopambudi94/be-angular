<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;
use App\RetailerProgram\ViewModels\ProductViewModel;
function createOrderInvoice($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderData = $arguments[1];

    try {

        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);

        $expTime = time() + (3600 * 3);
        $expDate = date("Y-m-d H:i:s", $expTime);
        $userId = $OrderInvoiceModel->convertToObjectId($orderData['user_id']);

        $orderId = $thisViewModel->createOrderId($auth['app_prefix']);
        $orderInvoice = [
            'user_id' => $userId,
            'products' => $orderData['products'],
            'address_detail'=>$orderData['address_detail'],
            'order_id' => $orderId,
            'status'=>'PENDING',
            'sum_total'=>$orderData['total_price'],
            'total_quantity'=>$orderData['total_quantity'],
            'shoppingcart_id'=>$OrderInvoiceModel->convertToObjectId($orderData['_id']),
            'order_expire_date'=>[
                'in_seconds'=>$expTime,
                'in_string'=>$expDate
            ],
            'request_date'=>$OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
            'request_at'=>getClientIpAddr(),
            'request_by'=>$userId,
            'points_by' => 'PRODUCT'
        ];

        $ProductViewModel = new ProductViewModel();
        $ProductViewModel->stockUpdateOnOrderInvoice($auth, $orderData, $orderId);

        $PointsTransactionViewModel = new PointsTransactionViewModel();
        
        $pointsData = [];
        foreach ($orderInvoice['products'] as $product){
            $pointsData[] = array(
                'user_id'=>$userId,
                'points'=>$product['total_product_price'],
                'reference_no'=>$orderId.'-'.$product['sku_code']
            );
        }

        $PointsTransactionViewModel->pointsRedeemption($auth, $pointsData);

        $result = $OrderInvoiceModel->insert($orderInvoice);

        return $orderInvoice;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
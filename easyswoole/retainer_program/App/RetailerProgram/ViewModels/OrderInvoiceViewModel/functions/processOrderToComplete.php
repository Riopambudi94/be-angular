<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\Models\DeliveryTrackingModel;

function processOrderToComplete($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);
        $DeliveryTrackingModel = new DeliveryTrackingModel();

        $filter = [
            'order_id'=>['$in'=>$bodyData['order_id']],
            'status'=>'PROCESSED'
        ];
        $invoice = $OrderInvoiceModel->DBfind($filter);
        empty($invoice['result']) ?
            $thisViewModel->sendError("order data not found", 404) :
            $invoice = $thisViewModel->objectToArray($invoice['result']);
            
        $updateData = [];
        foreach ($invoice as $key => $value) {
            $delivery = $DeliveryTrackingModel->DBfind(['order_id'=>$value['order_id']]);
            if (!empty($delivery['result'])) {
                $delivery = $thisViewModel->objectToArray($delivery['result']);
                foreach ($delivery as $sk => $sval) {
                    if (!in_array($sval['status'],['DELIVERED','REORDER'])) 
                        $thisViewModel->sendError("Package with reference no {$sval['reference_no']} is not delivered", 400);
                }
            }

            foreach($value['products'] as $k => $val) {
                if ($val['type'] == 'top-up' && !empty($val['cancel_detail']) && empty($value['additional_info']['receipt'])) {
                    $thisViewModel->sendError("transaction receipt is required", 400);
                }
            }

            $updateData[] = [
                'filter'=>['_id'=>$OrderInvoiceModel->convertToObjectId($value['_id'])],
                'new_value'=>['status'=>'COMPLETED']
            ];
        }

        if (!empty($updateData))
            $result = $OrderInvoiceModel->updateBatch($updateData);
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
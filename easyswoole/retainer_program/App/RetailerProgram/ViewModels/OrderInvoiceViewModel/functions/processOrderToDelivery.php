<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\GeneralData\ViewModels\CourierViewModel;
use Services\SpreadsheetService;

function processOrderToDelivery($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {
        if ($method == 'single') {
            $orderId = $bodyData['order_id'];

            $OrderInvoiceModel = new OrderInvoiceModel();
            $OrderInvoiceModel->setCurrentUser($auth['user_id']);

            $CourierViewModel = new CourierViewModel();

            $invoice = $OrderInvoiceModel->findByOrderId($orderId, ['status'=>'PROCESSED']);
            empty($invoice['result']) ?
                $thisViewModel->sendError("order data not found", 404) :
                $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

            $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));

            $lastShippingInfo = end($invoice['shipping_info']);

            if ($lastShippingInfo['label'] == 'on_processing') {

                $courier = [];
                $supported = 0;
                if (!empty($invoice['available_courier']) && is_array($invoice['available_courier'])) {
                    foreach ($invoice['available_courier'] as $key => $value) {
                        if ($bodyData['courier'] == $value['courier_code']) {
                            $courier = $CourierViewModel->getCourier($bodyData['courier']);
                            !empty($courier) ?
                                $supported = 1 : null;
                            break;
                        }
                    }
                }

                $invoice['delivery_detail'] = array_merge(
                    ($invoice['delivery_detail']?:[]),
                    [
                        'courier'=>$courier['courier_code']?:$bodyData['courier'],
                        'courier_name'=>$courier['courier']?:$bodyData['courier_name'],
                        'delivery_method'=>$bodyData['delivery_method'],
                        'delivery_service'=>$bodyData['delivery_service'],
                        'supported'=>$supported,
                        // 'awb_number'=>$bodyData['awb_number']
                    ]
                );

                if ($supported) {

                    $requestDelivery = $CourierViewModel->requestPickup($invoice, $courier);

                    $invoice['delivery_detail']['track_history'] = $requestDelivery['tracking_history'];
                    $invoice['delivery_detail']['last_status'] = $requestDelivery['last_status'];
                    $invoice['delivery_detail']['awb_number'] = $requestDelivery['awb_number'];

                    if ($courier['courier_code'] == 'REALS-SAP') {
                        $invoice['delivery_detail']['origin_branch'] = $requestDelivery['origin_branch'];
                        $invoice['delivery_detail']['dest_branch'] = $requestDelivery['dest_branch'];
                    }
                } else {
                    ($bodyData['courier'] != 'others') ?
                        $thisViewModel->sendError("courier {$bodyData['courier']} is not supported", 400):
                        $invoice['delivery_detail']['awb_number'] = $bodyData['awb_number'];
                        $invoice['delivery_detail']['courier_name'] = $bodyData['courier_name'];
                }

                $invoice['shipping_info'][] = [
                    'label'=>'on_delivery',
                    'title'=>'On Delivery Process',
                    'remarks'=>$bodyData['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];
                
                $setData = [
                    'shipping_info' => $invoice['shipping_info'],
                    'delivery_detail' => $invoice['delivery_detail']
                ];

            } elseif ($lastShippingInfo['label'] == 'on_delivery') {
                if ($invoice['delivery_detail']['supported'] == 0 && $bodyData['courier'] == 'others') {
                    unset($invoice['shipping_info'][sizeof($invoice['shipping_info'])-1]);
                    $invoice['shipping_info'][] = [
                        'label'=>'on_delivery',
                        'title'=>'On Delivery Process',
                        'remarks'=>$bodyData['remarks'],
                        'created_date'=>$OrderInvoiceModel->convertToMongoDateTime($lastShippingInfo['created_date']),
                        'updated_date'=>$currDate
                    ];
                    $invoice['shipping_info'] = array_values($invoice['shipping_info']);

                    $setData = [
                        'shipping_info' => $invoice['shipping_info'],
                        'delivery_detail' => [
                            'courier'=>$bodyData['courier'],
                            'courier_name'=>$bodyData['courier_name'],
                            'delivery_method'=>$bodyData['delivery_method'],
                            'delivery_service'=>$bodyData['delivery_service'],
                            'supported'=>0,
                            'awb_number'=>$bodyData['awb_number']
                        ]
                    ];
                }
            } else {
                $thisViewModel->sendError("invalid shipping status", 400);
            }

            $result = $OrderInvoiceModel->updateByID($invoice['_id'], $setData);
        } elseif ($method == 'bulk') {
            $MediaViewModel = new MediaViewModel();
            $document = $MediaViewModel->uploadDocument($auth, $bodyData, "ORDER_DELIVERY");

            $SpreadsheetService = new SpreadsheetService();
            $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

            $data = [
                'user_id' => $auth['user_id'],
                'files'=>array_values($files)
            ];
            if (in_array($document['ext'], ['xls','xlsx'])) {
                $result = processOrderDeliveryXLS($data, $thisViewModel);
            } elseif (in_array($document['ext'], ['csv'])) {
                // process csv here
            }
        }
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processOrderDeliveryXLS($data, &$thisViewModel) {

    try {

        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($data['user_id']);

        $CourierViewModel = new CourierViewModel();

        $requiredField = explode(",", "order_id,courier,delivery_service,delivery_method");
        $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                empty($value[$val])? $thisViewModel->sendError("field {$val} on row {$row} is required", 400): null;   
            }

            $invoice = $OrderInvoiceModel->findByOrderId($value['order_id'], ['status'=>'PROCESSED']);
            empty($invoice['result']) ?
                $thisViewModel->sendError("order data for order_id {$value['order_id']} not found", 404) :
                $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

            $lastShippingInfo = end($invoice['shipping_info']);
            if ($lastShippingInfo['label'] == 'on_processing') {

                $courier = [];
                $supported = 0;
                if (!empty($invoice['available_courier']) && is_array($invoice['available_courier'])) {
                    foreach ($invoice['available_courier'] as $sk => $sval) {
                        if ($value['courier'] == $sval['courier_code']) {
                            $courier = $CourierViewModel->getCourier($value['courier']);
                            !empty($courier) ?
                                $supported = 1 : null;
                            break;
                        }
                    }
                }

                $invoice['delivery_detail'] = array_merge(
                    ($invoice['delivery_detail']?:[]),
                    [
                        'courier'=>$courier['courier_code']?:$value['courier'],
                        'courier_name'=>$courier['courier']?:$value['courier_name'],
                        'delivery_method'=>$value['delivery_method'],
                        'delivery_service'=>$value['delivery_service'],
                        'supported'=>$supported,
                    ]
                );

                if ($supported) {
                    $requestDelivery = $CourierViewModel->requestPickup($invoice, $courier);

                    $invoice['delivery_detail']['track_history'] = $requestDelivery['tracking_history'];
                    $invoice['delivery_detail']['last_status'] = $requestDelivery['last_status'];
                    $invoice['delivery_detail']['awb_number'] = $requestDelivery['awb_number'];

                    if ($courier['courier_code'] == 'REALS-SAP') {
                        $invoice['delivery_detail']['origin_branch'] = $requestDelivery['origin_branch'];
                        $invoice['delivery_detail']['dest_branch'] = $requestDelivery['dest_branch'];
                    }
                } else {
                    ($value['courier'] != 'others') ?
                        $thisViewModel->sendError("courier {$value['courier']} is not supported", 400):
                        $invoice['delivery_detail']['awb_number'] = $value['awb_number'];
                        $invoice['delivery_detail']['courier_name'] = $value['courier_name'];
                }

                $invoice['shipping_info'][] = [
                    'label'=>'on_delivery',
                    'title'=>'On Delivery Process',
                    'remarks'=>$value['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];
                
                $updateData[] = [
                    'filter'=>['_id'=>$OrderInvoiceModel->convertToObjectId($invoice['_id'])],
                    'new_value'=>[
                        'shipping_info' => $invoice['shipping_info'],
                        'delivery_detail' => $invoice['delivery_detail']
                    ]
                ];
            } elseif ($lastShippingInfo['label'] == 'on_delivery') {

                if ($invoice['delivery_detail']['supported'] == 0 && $value['courier'] == 'others') {
                    unset($invoice['shipping_info'][sizeof($invoice['shipping_info'])-1]);
                    $invoice['shipping_info'][] = [
                        'label'=>'on_delivery',
                        'title'=>'On Delivery Process',
                        'remarks'=>$value['remarks'],
                        'created_date'=>$OrderInvoiceModel->convertToMongoDateTime($lastShippingInfo['created_date']),
                        'updated_date'=>$currDate
                    ];
                    $invoice['shipping_info'] = array_values($invoice['shipping_info']);

                    $updateData[] = [
                        'filter'=>['_id'=>$OrderInvoiceModel->convertToObjectId($invoice['_id'])],
                        'new_value'=>[
                            'shipping_info' => $invoice['shipping_info'],
                            'delivery_detail' => [
                                'courier'=>$value['courier'],
                                'courier_name'=>$value['courier_name'],
                                'delivery_method'=>$value['delivery_method'],
                                'delivery_service'=>$value['delivery_service'],
                                'supported'=>0,
                                'awb_number'=>$value['awb_number']
                            ]
                        ]
                    ];
                }
            } else {
                $thisViewModel->sendError("invalid shipping status", 400);
            }
        }

        if (!empty($updateData)) {
            $result = $OrderInvoiceModel->updateBatch($updateData);
        } else {
            $result = ['result'=>['status'=>'no data updated']];
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
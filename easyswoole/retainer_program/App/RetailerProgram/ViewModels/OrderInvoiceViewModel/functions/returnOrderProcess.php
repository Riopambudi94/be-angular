<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;
use App\RetailerProgram\ViewModels\ProductViewModel;

function returnOrderProcess ($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $OrderInvoiceModel = new OrderInvoiceModel();
                $OrderInvoiceModel->setCurrentUser($auth['user_id']);
                $orderInvoice = $OrderInvoiceModel->findByOrderId($bodyData['order_id'], ['status'=>'COMPLETED']);

                empty($orderInvoice['result']) ?
                    $thisViewModel->sendError("order data for order_id {$bodyData['order_id']} not found", 404) :
                    $orderInvoice = $thisViewModel->objectToArray($orderInvoice['result'][0]);

                if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/i", $bodyData['return_date'])) {
                    $thisViewModel->sendError("Invalid date format {$bodyData['return_date']}, only \"YYYY-MM-DD\" format allowed", 400);
                }
                $date = createDateTime($bodyData['return_date'], 'format', "Y-m-d H:i:s");
                $returnDate = $OrderInvoiceModel->convertToMongoDateTime($date);
                $currDate = $OrderInvoiceModel->convertToMongoDateTime($date);

                $returnDetail = [
                    'courier'=>$bodyData['courier'],
                    'awb_number'=>$bodyData['awb_number'],
                    'return_date'=>$returnDate,
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];

                $orderInvoice['shipping_info'][] = [
                    'label'=>'return',
                    'title'=>'Return Order',
                    'remarks'=>$bodyData['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];

                if ($bodyData['return_type'] == 'REORDER') {
                    $PointsTransactionViewModel = new PointsTransactionViewModel();
                    $ProductViewModel = new ProductViewModel();

                    $PointsTransactionViewModel->refundRedeemedPoints($auth, $orderInvoice);

                    $ProductViewModel->stockReturnOnOrderInvoice($auth, $orderInvoice);  

                    $setData = [
                        'status'=>'CANCEL',
                        'cancel_date'=>$currDate,
                        'cancel_at'=>getClientIpAddr(),
                        'cancel_by'=>$OrderInvoiceModel->convertToObjectId($auth['user_id']),
                        'shipping_info' => $orderInvoice['shipping_info'],
                        'return_detail' => $returnDetail,
                    ];
                } elseif ($bodyData['return_type'] == 'REDELIVER') {
                    $setData = [
                        'status'=>'RETURN',
                        'shipping_info' => $orderInvoice['shipping_info'],
                        'return_detail' => $returnDetail,
                    ];
                }
                $result = $OrderInvoiceModel->updateByID($orderInvoice['_id'], $setData);
                
                break;
            
            case 'bulk':
                # code...
                break;
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
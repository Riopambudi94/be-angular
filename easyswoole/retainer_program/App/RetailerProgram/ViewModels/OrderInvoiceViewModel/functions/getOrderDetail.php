<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
function getOrderDetail($arguments, $thisViewModel) {
    $orderId = $arguments[0];
    try {


        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->requestColumns = "products,order_id,status,sum_total,total_quantity,shipping_info,address_detail,delivery_detail,return_detail,redelivery_detail,order_expire_date,additional_info,invoice_no,po_no,request_date,approve_date,cancel_date,member_detail,last_shipping_info,username,available_courier";

        $OrderInvoiceModel->requestMapping['member_detail'] = '$member_dertail';
        $OrderInvoiceModel->requestMapping['last_shipping_info'] = '$last_shipping_info';
        $OrderInvoiceModel->requestMapping['username'] = '$username';

        $orderDetail = $OrderInvoiceModel->findByOrderId($orderId, [], true);

        $result = $thisViewModel->objectToArray($orderDetail['result'][0]);

        foreach ($result['products'] as $key => $value) {
            unset($result['products'][$key]['_id']);
            unset($result['products'][$key]['product_id']);
        }
        $result = ['result'=>$result];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
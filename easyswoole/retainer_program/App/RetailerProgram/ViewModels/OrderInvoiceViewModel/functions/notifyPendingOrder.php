<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\GeneralData\ViewModels\EmailViewModel;
use App\UserManagement\ViewModels\AppListViewModel;
use Services\Twig;

function notifyPendingOrder($arguments, $thisViewModel) {
    global $selectedDB;
    try {
        $AppListViewModel = new AppListViewModel();
        $appList = $AppListViewModel->findApp(['status'=>'ACTIVE']);

        if (!empty($appList['result'])) {
            $url = (ENVIRONTMENT == 'DEVELOPMENT') ? "http://dev.migration.admin.form-sig.id" : "https://portal.reals.cls-indo.co.id";

            $EmailViewModel = new EmailViewModel();
            $Twig = new Twig(PROJECT_PATH.'assets/views/email_template/');

            $emailTemplate = $EmailViewModel->getEmailTemplate(['email_type'=>'notify_order_pending']);
            foreach ($appList['result'] as $key => $value) {
                $selectedDB = $value->app_name;

                $OrderInvoiceModel = new OrderInvoiceModel();
                $pendingOrder = $OrderInvoiceModel->DBfind(['status'=>'PENDING']);

                if (!empty($pendingOrder['result'])) {
                    $pendingOrder = $thisViewModel->objectToArray($pendingOrder['result']);

                    $orderData = [];
                    foreach ($pendingOrder as $row => $data) {
                        $orderData[] = [
                            'order_id'=>$data['order_id'],
                            'full_name'=>$data['address_detail']['name'],
                            'sum_total'=>$data['sum_total']
                        ];
                    }

                    $dataView = [
                        'body'=>$emailTemplate['body'],
                        'order_data' => $orderData,
                        'app_label'=>$value->app_label,
                        'portal_url'=>$url
                    ];
                    $template = $Twig->render('list_order_pending.html', $dataView);


                    $bodyMail = [
                        'sender_email' => $emailTemplate['sender_email'],
                        'recipient_email' => $emailTemplate['recipient_email'],
                        'cc' => $emailTemplate['cc']?:[],
                        'subject' =>  $emailTemplate['subject'],
                        'email_type' => 'notify_order_pending',
                        'app_label'=>$value->app_label,
                        'body' => $template,
                    ];
                    $sendEmail = $EmailViewModel->sendEmail($bodyMail);

                }
                
            }
        }

        $result = ['result'=>'success'];
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
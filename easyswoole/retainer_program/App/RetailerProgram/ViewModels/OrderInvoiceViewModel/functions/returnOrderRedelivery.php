<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;

function returnOrderRedelivery ($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $OrderInvoiceModel = new OrderInvoiceModel();
                $OrderInvoiceModel->setCurrentUser($auth['user_id']);
                $orderInvoice = $OrderInvoiceModel->findByOrderId($bodyData['order_id'], ['status'=>'RETURN']);

                empty($orderInvoice['result']) ?
                    $thisViewModel->sendError("order data for order_id {$bodyData['order_id']} not found", 404) :
                    $orderInvoice = $thisViewModel->objectToArray($orderInvoice['result'][0]);

                if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/i", $bodyData['delivery_date'])) {
                    $thisViewModel->sendError("Invalid date format {$bodyData['delivery_date']}, only \"YYYY-MM-DD\" format allowed", 400);
                }

                if (empty($bodyData['awb_number'])) {
                    $thisViewModel->sendError("awb number is required", 400);
                }

                $date = createDateTime($bodyData['return_date'], 'format', "Y-m-d H:i:s");
                $deliveryDate = $OrderInvoiceModel->convertToMongoDateTime($date);
                $currDate = $OrderInvoiceModel->convertToMongoDateTime($date);

                $redeliveryDetail = [
                    'courier'=>$bodyData['courier'],
                    'delivery_method'=>$bodyData['delivery_method'],
                    'delivery_service'=>$bodyData['delivery_service'],
                    'awb_number'=>$bodyData['awb_number'],
                    'delivery_date'=>$deliveryDate,
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];

                $orderInvoice['shipping_info'][] = [
                    'label'=>'redelivery',
                    'title'=>'On Redelivery Process',
                    'remarks'=>$bodyData['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];

                $setData = [
                    'status'=>'PROCESSED',
                    'shipping_info' => $orderInvoice['shipping_info'],
                    'redelivery_detail' => $redeliveryDetail,
                ];
                $result = $OrderInvoiceModel->updateByID($orderInvoice['_id'], $setData);
                break;
            
            case 'bulk':
                # code...
                break;
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
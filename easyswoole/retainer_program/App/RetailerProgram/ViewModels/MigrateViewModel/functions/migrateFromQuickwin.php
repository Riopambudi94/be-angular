<?php
namespace App\RetailerProgram\ViewModels\MigrateViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use App\GeneralData\ViewModels\ReportViewModel;
use Services\Curl;

function migrateFromQuickwin($arguments, $thisViewModel) {
    $auth = $arguments[0];
    try {

        $Curl = new Curl();

        $param = [
            'method' => 'GET',
            'url'=>'https://api2.locard.co.id/f3/v1/members/report',
            'headers'=>[
                'Authorization: t0lCSTHTBFFQkiNHFDgrd3cJ4s3VfZyZTSJwRFcbqvmMjfkb0aA1I1sRfZJE3MKD.izcNMYaF1duup3VQh4hC4XU/IObvda4DwefgZ57xAEIVlecUqrJzs2yI2zS1zQfhSHGtVmFbLBupq2fnYnnZLDyYqGyKyYKLHT3PgPTj64sA5YtkDRLe6+/tE0IlBQu0F8zEK2Gjj0+6Amv+gfJRwWGFShxSbLh1FogcOBXxhRpjdKhd2htfRKtQerZiP7Q91AMNVzO3JHGNzIK/RzsIiOt/SkaKuEy88PymxRSkxfsHpLfSQVzSuQbpeo0xMegksWkXf4Zf3NLuWbi6r2awBWbkxyPG0Y0UN+jG1LAcMeQfREDel34D1jhOiVVYaC3W.ALcus3fv5/ufoauWs57Bvj1ZUD44et6/k3Y2WM/rvnA='
            ]
        ];

        $curlResult = $Curl->sendRequest($param);

        $memberData = $curlResult['result']['result']['values'];

        $CryptoViewModel = new CryptoViewModel();
        $ReportViewModel = new ReportViewModel();
        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($auth['user_id']);
        
        $newMember = [];

        $returnResult = [];
        foreach ($memberData as $key => $value) {

            /* $member = $MemberModel->findByUsernameOrId($value['username']);
            if (!empty($member['result'])) {
                $thisViewModel->sendError("member id {$value['username']} already exists", 400);
            } */

            $newMember[] = [
                'full_name' => $value['full_name'],
                'username' => $value['username'],
                'cell_phone' => $value['cell_phone'],
                'password' => $CryptoViewModel->generatePassword($value['password']),
                'dob' => $MemberModel->convertToMongoDateTime($value['dob']),
                'gender' => $value['gender'],
                'member_address' => $value['buyer_detail'],
                'status' => 'ACTIVE',
                'input_form_data' => $value['autoform'],
                'additional_info' => [],
            ];

            $returnResult[] = [
                'customer_id' => $value['username'],
                'full_name' => $value['full_name'],
                'cell_phone' => $value['cell_phone'],
                'password' => "'".$value['password']
            ];
        }

        $insertMember = $MemberModel->insertBatch($newMember);

        $result = $ReportViewModel->generateReport($returnResult, 'MEMBER_MIGRATION_SG', 'xlsx');

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
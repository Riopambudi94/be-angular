<?php
namespace App\RetailerProgram\ViewModels\PointsTransactionViewModel;

use App\RetailerProgram\Models\PointsTransactionModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findAllReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {
        $PointsTransactionModel = new PointsTransactionModel();

        // utk urutan output
        if (!isset($requested_columns)) {
            $requested_columns = "_id,user_id,points,item_code,reference_no,description,remarks,status,transaction_month,item_code,process_type,process_number,process_id,created_date,updated_date,deleted_date,total_transaction,point_add,point_subtract,point_adj_in,point_adj_out,id_toko,nama_toko,nama_pemilik,no_wa_pemilik,alamat_toko,total_point,group,full_name,username";
        }
        $requested_columns = explode(",", $requested_columns);

        $PointsTransactionModel->requestColumns .= ',total_transaction,point_add,point_subtract,point_adj_in,point_adj_out,id_toko,nama_toko,nama_pemilik,no_wa_pemilik,alamat_toko,total_point,group,full_name,username';
        $PointsTransactionModel->requestMapping = array_merge(
            $PointsTransactionModel->requestMapping,
            [
                '_id'=>0,
                'total_transaction'=>'$total_transaction',
                'point_add'=>'$point_add',
                'point_subtract'=>'$point_subtract',
                'point_adj_in'=>'$point_adj_in',
                'point_adj_out'=>'$point_adj_out',
                'full_name'=>'$full_name',
                'username'=>'$username',
                'id_toko'=>'$id_toko',
                'nama_toko'=>'$nama_toko',
                'nama_pemilik'=>'$nama_pemilik',
                'no_wa_pemilik'=>'$no_wa_pemilik',
                'alamat_toko'=>'$alamat_toko',
                'total_point'=>'$total_point',
                'group'=>'$group'
            ]
        );
        
        // $search['based_on'] = $search['based_on']?:'process_number';

        if ($download) {
            $result_db = $PointsTransactionModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, 'PTS_TRANSACTION_REPORT');
            }
            
        } else {
            $result = $PointsTransactionModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
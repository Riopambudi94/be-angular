<?php
namespace App\RetailerProgram\ViewModels\PointsTransactionViewModel;

use App\RetailerProgram\Models\PointsTransactionModel;
use App\RetailerProgram\Models\PointsInventoryModel;

function pointsRedeemption($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $pointsData = $arguments[1];

    try {

        $PointsInventoryModel = new PointsInventoryModel();
        $PointsInventoryModel->setCurrentUser($auth['user_id']);
        
        $PointsTransactionModel = new PointsTransactionModel();
        $PointsTransactionModel->setCurrentUser($auth['user_id']);

        $ptsTxData = [];
        $tempPtsInv = [];
        $processNumber = $auth['app_prefix']."-".hexdec(uniqid().rand(0,99).chr(rand(65,70)));
        $i = 1;
        foreach ($pointsData as $key => $value) {
            $points = $value['points'];

            ($points <= 0) ?
                $thisViewModel->sendError("invalid points value {$points}", 400) : null;

            $search = [
                'status'=>'ACTIVE',
                'expiry_date'=>date('Y-m'),
                'points_exists'=>true
            ];
            $ptsItem = $PointsInventoryModel->getPointByUserId($value['user_id'], $search);
            empty($ptsItem['result'])?
                $thisViewModel->sendError("user {$value['member_id']} points balance is empty", 400) : 
                $ptsItem = $thisViewModel->objectToArray($ptsItem['result']);

            $userId = $PointsTransactionModel->convertToObjectId($value['user_id']);

            foreach ($ptsItem as $skey => $svalue) {
                if ($svalue['points'] > $points) {
                    $pts = $points;
                } else {
                    $pts = $svalue['points'];
                }
                $points -= $pts;

                $ptsTxData[] = [
                    'points'=>$pts,
                    'user_id'=>$userId,
                    'reference_no'=>$value['reference_no'],
                    'description'=>'points redeemption',
                    'remarks'=>'redeem points',
                    'status'=>'PROCESSED',
                    'transaction_month'=>$svalue['transaction_month'],
                    'item_code'=>$svalue['item_code'],
                    'process_type'=>'SUB',
                    'process_number'=>$processNumber,
                    'process_id'=>$processNumber.'-'.sprintf("%03d", $i++),
                ];

                isset($tempPtsInv[$svalue['_id']]) ? 
                $tempPtsInv[$svalue['_id']] -= $pts : $tempPtsInv[$svalue['_id']] = $svalue['points'] - $pts;

                if ($points <= 0) break;
            }

            ($points > 0) ?
                $thisViewModel->sendError("user {$value['member_id']} points balance is insufficient", 400): null;
            
        }

        $updatePtsInv = [];
        foreach ($tempPtsInv as $ptsID => $ptsValue){
            $updatePtsInv[] = [
                'filter' => ['_id'=>$PointsTransactionModel->convertToObjectId($ptsID)],
                'new_value'=>[
                    'points'=>$ptsValue
                ]
            ];
        }

        if(!empty($ptsTxData)) $PointsTransactionModel->insertBatch($ptsTxData);
        if(!empty($updatePtsInv)) $PointsInventoryModel->updateBatch($updatePtsInv);
            
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
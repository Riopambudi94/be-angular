<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;

function find($arguments, $thisViewModel) {
    $search = $arguments[0];

    try {

        $ProductModel = new ProductModel();

        $ProductModel->requestColumns['_id'] = 0;
        
        $result = $ProductModel->find($search);

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductSkuModel;

function getSkuDetail($arguments, $thisViewModel) {
    $productCode = $arguments[0];
    $skuCode = $arguments[1];
    $status = $arguments[2];
    try {

        $ProductSkuModel = new ProductSkuModel();

        $search = [
            'product_code'=>$productCode,
            'sku_code'=>$skuCode,
        ];
        !empty($status)? $search['status'] = $status : null;
        $result = $ProductSkuModel->findProductSku($search);
        if (empty($result['result'])) {
            $thisViewModel->sendError("Product Sku {$skuCode} does not exists", 404);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductSkuModel;
use App\RetailerProgram\Models\EvoucherModel;

function stockReturnOnOrderInvoice($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderData = $arguments[1];

    try {
        $products = $orderData['products'];

        $ProductSkuModel = new ProductSkuModel();
        $ProductSkuModel->setCurrentUser($auth['user_id']);

        $EvoucherModel = new EvoucherModel;
        $EvoucherModel->setCurrentUser($auth['user_id']);

        $memberId = $EvoucherModel->convertToObjectId($orderData['user_id']);
        $updateProduct = [];
        $updateEvoucher = [];
        foreach ($products as $key => $value) {
            switch ($value['type']) {
                case 'product':
                case 'voucher':
                    $updateProduct[] = [
                        'filter'=>[
                            '_id'=>$ProductSkuModel->convertToObjectId($value['_id'])
                        ],
                        'new_value'=>[
                            'qty'=>$value['qty'] + $value['quantity']
                        ]
                    ];
                    break;
                
                case 'e-voucher':
                    $filter = [
                        'product_code'=>$value['product_code'],
                        'sku_code'=>$value['sku_code'],
                        'status'=>['in'=>['PENDING','RESERVED']],
                        'owner_id'=>$memberId,
                        'reference_no'=>$orderData['order_id']
                    ];
                    $getEvoucher = $EvoucherModel->findEvoucher($filter);
                    $getEvoucher = $thisViewModel->objectToArray($getEvoucher['result']);

                    foreach ($getEvoucher as $key => $value) {
                        $updateEvoucher[] = [
                            'filter'=>['_id'=>$EvoucherModel->convertToObjectId($value['_id'])],
                            'new_value'=>[
                                'qty_out'=>0,
                                'status'=>'ACTIVE',
                                'owner_id'=>null,
                                'reference_no'=>null,
                            ]
                        ];   
                    }
                    break;
            }
        }

        if (!empty($updateProduct))
            $ProductSkuModel->updateBatch($updateProduct);
        if (!empty($updateEvoucher))
            $EvoucherModel->updateBatch($updateEvoucher);
       
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;

function getProductDetail($arguments, $thisViewModel) {
    $productCode = $arguments[0];
    $status = $arguments[1];
    try {

        $ProductModel = new ProductModel();

        if (!empty($status))
            $result = $ProductModel->findByProductCodeOrId($productCode, $status);
        else 
            $result = $ProductModel->findByProductCodeOrId($productCode);

        if (empty($result['result'])) {
            $thisViewModel->sendError("Product {$productCode} does not exists", 404);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
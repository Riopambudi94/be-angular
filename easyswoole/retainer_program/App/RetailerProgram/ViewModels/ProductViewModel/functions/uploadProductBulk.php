<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function uploadProductBulk($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {
        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "PRODUCT_UPLOAD");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>$files
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            $result = processXlsx($data, $thisViewModel);
        } elseif (in_array($document['ext'], ['csv'])) {
            
        }

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processXlsx($data, &$thisViewModel) {
    try {

        $ProductModel = new ProductModel();
        $ProductSkuModel = new ProductSkuModel();

        $ProductModel->setCurrentUser($data['user_id']);
        $ProductSkuModel->setCurrentUser($data['user_id']);

        $productType = ["product","voucher","e-voucher","gold","top-up"];
        $varName = "variation_name_";
        $varValue = "variation_";

        $skuList = [];

        $newProduct = [];
        $updateProduct = [];

        $newProductSku = [];
        $updateProductSku = [];

        $variation = [];
        foreach ($data['files'] as $key => $value) {
            $rowNum = $key + 1;

            $prodCode = trim($value['product_code']);
            $prodName = trim($value['product_name']);
            $prodType = trim($value['type']);
            $category = trim($value['category']);
            $skuPrice = trim($value['price']);
            $skuValue = trim($value['value']);
            $skuCode = trim($value['sku_code']);
            $status = trim($value['status']);
            
            $productData = [];
            $productSkuData = [];

            !in_array($status, ['ACTIVE','INACTIVE'])?
                $thisViewModel->sendError("invalid status {$status} on row {$rowNum}", 400) : null;

            !in_array($prodType, $productType) ?
                $thisViewModel->sendError("invalid product type {$prodType} on row {$rowNum}", 400) : null;

            if (empty($skuPrice) || empty($skuValue)) {
                $thisViewModel->sendError("sku price and value at row {$key} is required  on row {$rowNum}", 400);
            }

            if (isset($newProductSku[$prodCode.$skuCode]) || isset($updateProductSku[$prodCode.$skuCode]) || 
            ($prodType != 'product' && (isset($newProduct[$prodCode]) || isset($updateProduct[$prodCode])))) {
                continue;
            }

            if (!isset($newProduct[$prodCode]) && !isset($updateProduct[$prodCode])) {

                $productData = [
                    'product_name'=>$prodName,
                    'product_code'=>$prodCode,
                    'description'=>$value['description'],
                    'type'=>$prodType,
                    'category'=>$category,
                    'status'=>$status,
                    'image_gallery'=>[],
                ];

                if ($prodType == 'e-voucher') {
                    $productData['qty'] = 0;
                    $productData['tnc'] = $value['tnc'];
                } elseif ($prodType == 'product' || $prodType == 'voucher') {
                    $volume = $thisViewModel->calculateVolumetricWeight($value['length'], $value['width'], $value['height']);
                    $productData = array_merge($productData, $volume);
                    $productData['weight'] = floatval($value['weight']);
                    $productData['variation'] = [];
                }

                $findProduct = $ProductModel->find(['product_code'=>$prodCode]);
                if (!empty($findProduct['result'])) {
                    unset($productData['image_gallery']);
                    $productData['_id'] = $ProductSkuModel->convertToObjectId($findProduct['result'][0]->_id);;
                    $updateProduct[$prodCode] = $productData;
                } else {
                    $newProduct[$prodCode] = $productData;
                }
            }

            if (isset($newProduct[$prodCode])) {
                $skuList[$prodCode][] = $skuCode;
            }

            $productSkuData = [
                'sku_code' => $prodCode.'-'.$skuCode,
                'sku_price' => intval($skuPrice),
                'sku_value' => intval($skuValue),
                'product_id'=>$updateProduct[$prodCode]['_id']?:null,
                'product_code'=>$prodCode,
                'variant' => null,
                'qty'=>intval($value['stock_qty']),
                'image_gallery'=>[],
                'status'=>$status
            ];

            if ($prodType == 'product') {
                $productSkuData['variant'] = [];
                $i = 0;
                $variant = [];
                foreach ($value as $sk => $sval) {
                    $sval = trim(strtolower($sval));
                    if (strpos($sk, $varName) !== false && !empty($sval)) {
                        $i = substr($sk, strlen($varName));
                        $fieldName = $varValue.$i;
                        empty($value[$fieldName]) ?
                            $thisViewModel->sendError("variation value for field name {$fieldName} is required", 400) : null;
                        
                        !empty($productSkuData['variant'][$sval]) ?
                            $thisViewModel->sendError("repeated variation name {$value[$fieldName]} on row {$rowNum}", 400): null;

                        $variant[$sval] = $value[$fieldName];

                        if (isset($newProduct[$prodCode]) && !in_array($value[$fieldName], $newProduct[$prodCode]['variation'][$sval])){
                            $newProduct[$prodCode]['variation'][$sval][] = $value[$fieldName];
                        }

                        $productSkuData['variant'][$sval] = $value[$fieldName];
                    }
                }
                if (!empty($variant) && in_array($variant, $variation[$prodCode])) {
                    $thisViewModel->sendError("product variation on row {$rowNum} already exists", 400);
                }
                $variation[$prodCode][] = $variant;
            }

            $productSku = $ProductSkuModel->findProductSku(['product_code'=>$prodCode, 'sku_code'=>$prodCode.'-'.$skuCode]);
            if (!empty($productSku['result'])) {
                unset($productSkuData['image_gallery']);
                $updateProductSku[$prodCode.$skuCode] = [
                    'filter'=>['_id'=>$ProductSkuModel->convertToObjectId($productSku['result'][0]->_id)],
                    'new_value'=>$productSkuData
                ];
            } else {
                $newProductSku[$prodCode.$skuCode] = $productSkuData;
            }
        }

        if (!empty($updateProductSku)) {
            $updateSKU = $ProductSkuModel->updateBatch($updateProductSku);
        }

        if (!empty($updateProduct)) {
            $updateData = [];
            foreach ($updateProduct as $key => $value) {
                if ($value['type'] == 'product') {
                    $skuListFromServer = $ProductSkuModel->findProductSku(['product_code'=>$value['product_code']]);
                    foreach ($skuListFromServer['result'] as $skey => $svalue) {
                        if ($value['status'] == 'INACTIVE' && $svalue->status == 'ACTIVE'){
                            $value['status'] = 'ACTIVE';
                        }
                        
                        foreach ($svalue->variant as $sk => $sval) {
                            empty($value['variation'][$sk]) ? $value['variation'][$sk] = [] : null;

                            if (!in_array($sval, $value['variation'][$sk])) {
                                $value['variation'][$sk][] = $sval;
                            }
                        }
                    }
                }
                $productId = $value['_id'];
                unset($value['_id']);
                $updateData[] = [
                    'filter'=>['_id'=>$productId],
                    'new_value'=>$value
                ];
            }

            $updateProduct = $ProductModel->updateBatch($updateData);
        }

        if (!empty($newProduct)) {
            $insertProduct = $ProductModel->insertBatch($newProduct);

            foreach ($insertProduct['result']['_id'] as $key => $value) {
                $productId = $ProductSkuModel->convertToObjectId($value);
                foreach ($skuList[$key] as $skey => $sval) {
                    $newProductSku[$key.$sval]['product_id'] = $productId;
                }
            }
        }

        if (!empty($newProductSku)) {
            $insertSKU = $ProductSkuModel->insertBatch($newProductSku);
        }

        $result = ['result'=>'success'];
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
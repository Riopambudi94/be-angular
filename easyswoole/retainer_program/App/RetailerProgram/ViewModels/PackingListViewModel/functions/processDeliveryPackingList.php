<?php
namespace App\RetailerProgram\ViewModels\PackingListViewModel;

use App\RetailerProgram\Models\PackingListModel;

function processDeliveryPackingList($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $packingNo = $arguments[1];

    try{

        $PackingListModel = new PackingListModel();
        $PackingListModel->setCurrentUser($auth['user_id']);

        $filter = [
            'packing_no'=>$packingNo,
            'status'=>'ACTIVE'
        ];
        $getPackingList = $PackingListModel->getDetail($filter);

        empty($getPackingList['result']) ?
            $thisViewModel->sendResult("packing no {$packingNo} is not available", 404) :
            $getPackingList = $thisViewModel->objectToArray($getPackingList['result'][0]);

        $getPackingList['status'] = 'PROCESSED';
        $getPackingList['delivery_date'] = date('Y-m-d H:i:s');

        $update = $PackingListModel->updateByID($getPackingList['_id'], [
            'status'=>$getPackingList['status'],
            'delivery_date'=>$PackingListModel->convertToMongoDateTime($getPackingList['delivery_date'])
        ]);

        unset($getPackingList['_id']);
        $result = ['result' => $getPackingList];

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
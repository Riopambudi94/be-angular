<?php
namespace App\RetailerProgram\ViewModels\PackingListViewModel;

use App\RetailerProgram\Models\PackingListModel;
use App\RetailerProgram\Models\DeliveryTrackingModel;

function createPackingList($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $referenceNo = $arguments[1];
    
    try{

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
        $PackingListModel = new PackingListModel();
        $PackingListModel->setCurrentUser($auth['user_id']);

        $updateData = [];
        $newPackingList = [];
        $updatePackingList = [];
        $packingList = [];
        foreach ($referenceNo as $key => $value) {

            isset($updateData[$value])?
                $thisViewModel->sendError("multiple reference no {$value}", 400) : null;

            $filter = [
                'reference_no'=>$value,
                'shipping_info'=>['$exists'=>true],
                'shipping_info.label'=>'on_delivery',
                'status'=>'ACTIVE',
                '$or'=>[
                    ['in_packing_list'=>false],
                    ['in_packing_list'=>['$exists'=>false]]
                ]
            ];
            $delivery = $DeliveryTrackingModel->DBfind($filter);

            empty($delivery['result']) ?
                $thisViewModel->sendError("cannot process order {$value} to packing list", 400) : 
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $k = $delivery['courier'].$delivery['courier_name'].$delivery['delivery_method'].$delivery['delivery_service'];
            $setData = [];
            if (empty($updatePackingList[$k]) && empty($newPackingList[$k])) {
                $filter = [
                    'courier_code' => $delivery['courier'],
                    "courier_name" => $delivery['courier_name'],
                    'delivery_method' => $delivery['delivery_method'],
                    'delivery_service' => $delivery['delivery_service'],
                    'status'=>'ACTIVE'
                ];

                $getPackingList = $PackingListModel->DBfind($filter);
    
                if (!empty($getPackingList['result'])) {
                    $packingList[$k] = $thisViewModel->objectToArray($getPackingList['result'][0]);

                    $updatePackingList[$k] = [
                        'filter' => ['_id'=>$PackingListModel->convertToObjectId($getPackingList['result'][0]->_id)],
                        'new_value'=>[
                            'reference_no'=>$getPackingList['result'][0]->reference_no
                        ]
                    ];
                    // $updatePackingList[$k]['packing_no'] = $getPackingList['result'][0]->packing_no;
                    $updatePackingList[$k]['new_value']['reference_no'][] = $delivery['reference_no'];

                    $setData['packing_no'] = $packingList[$k]['packing_no'];
                } else {
                    $newPackingList[$k]['packing_no'] = strval(hexdec(time().rand(0,99)));
                    $newPackingList[$k]['reference_no'] = [$delivery['reference_no']];
                    $newPackingList[$k]['courier_code'] = $delivery['courier'];
                    $newPackingList[$k]['courier_name'] = $delivery['courier_name'];
                    $newPackingList[$k]['delivery_method'] = $delivery['delivery_method'];
                    $newPackingList[$k]['delivery_service'] = $delivery['delivery_service'];
                    $newPackingList[$k]['status'] = 'ACTIVE';

                    $setData['packing_no'] = $newPackingList[$k]['packing_no'];
                }
            } elseif (isset($newPackingList[$k])) {
                $newPackingList[$k]['reference_no'][] = $delivery['reference_no'];
                $setData['packing_no'] = $newPackingList[$k]['packing_no'];
            } elseif (isset($updatePackingList[$k])) {
                $updatePackingList[$k]['new_value']['reference_no'][] = $delivery['reference_no'];
                $setData['packing_no'] = $packingList[$k]['packing_no'];
            }

            $setData['in_packing_list'] = true;
            $updateData[$value] = [
                'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                'new_value'=>$setData
            ];
        }

        !empty($newPackingList) ?
            $insertBatch = $PackingListModel->insertBatch($newPackingList) : null;

        !empty($updatePackingList) ?
            $updateBatch = $PackingListModel->updateBatch($updatePackingList) : null;

        !empty($updateData) ?
            $updateData = $DeliveryTrackingModel->updateBatch($updateData) : null;

        $result = [
            'result' => ['status'=>'success']
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
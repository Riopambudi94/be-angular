<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;

function redeliveryProcess ($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
                $deliveryTracking = $DeliveryTrackingModel->findByRefno($bodyData['ref_no'], ['status'=>['RETURN', 'ACTIVE']]);

                empty($deliveryTracking['result']) ?
                    $thisViewModel->sendError("Delivery data for reference number {$bodyData['ref_no']} not found", 404) :
                    $deliveryTracking = $thisViewModel->objectToArray($deliveryTracking['result'][0]);

                if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/i", $bodyData['delivery_date'])) {
                    $thisViewModel->sendError("Invalid date format {$bodyData['delivery_date']}, only \"YYYY-MM-DD\" format allowed", 400);
                }

                $shippingInfo = $deliveryTracking['shipping_info'] ? :[];
                $lastShippingInfo = end($shippingInfo);
                $redeliveryDetail = $deliveryTracking['redelivery_detail'] ? : [];

                if(!empty($lastShippingInfo) && !in_array($lastShippingInfo['label'], ['return','redelivery']))  $thisViewModel->sendError("invalid operation", 400);

                $deliveryDate = createDateTime($bodyData['delivery_date'], 'format', "Y-m-d H:i:s");
                $date = date("Y-m-d H:i:s");
                $updateDate = $date;
                $deliveryId = $DeliveryTrackingModel->convertToObjectId(true,true)->__toString();


                if(!empty($lastShippingInfo) && $lastShippingInfo['label'] == 'redelivery') {
                    $deliveryId = $lastShippingInfo['id'];
                    $date = $lastShippingInfo['created_date'];
                    $updatedDate = date("Y-m-d H:i:s");
                    array_pop($shippingInfo);

                    $tempredeliveryDetail = $redeliveryDetail;
                    $redeliveryDetail = array();
                    foreach ($tempredeliveryDetail as $key => $value){
                        if($value['delivery_id'] == $deliveryId){
                            $value['courier'] = $bodyData['courier'];
                            $value['delivery_method'] = $bodyData['delivery_method'];
                            $value['delivery_service'] = $bodyData['delivery_service'];
                            $value['awb_number'] = $bodyData['awb_number'];
                            $value['delivery_date'] = $deliveryDate;
                            $value['created_date'] = $date;
                            $value['updated_date'] = $updatedDate;
                        }
                        $redeliveryDetail[] = $value;
                    }                    
                }

                else {
                    $redeliveryDetail[] = [
                        'delivery_id' => $deliveryId,
                        'courier'=>$bodyData['courier'],
                        'delivery_method'=>$bodyData['delivery_method'],
                        'delivery_service'=>$bodyData['delivery_service'],
                        'awb_number'=>$bodyData['awb_number'],
                        'delivery_date'=>$deliveryDate,
                        'created_date'=>$date,
                        'updated_date'=>$updateDate
                    ];
                }

                $shippingInfo[] = [
                    'id' => $deliveryId,
                    'label'=>'redelivery',
                    'title'=>'On Redelivery Process',
                    'remarks'=>$bodyData['remarks'],
                    'created_date'=>$date,
                    'updated_date'=>$updateDate
                ];

                $setData = [
                    'status'=>'ACTIVE',
                    'shipping_info' => $shippingInfo,
                    'redelivery_detail' => $redeliveryDetail,
                ];

                foreach ($setData['shipping_info'] as $key => $value) {
                    empty($value['id']) ? :$value['id'] = $DeliveryTrackingModel->convertToObjectId($value['id']);
                    $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                    $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
    
                    $setData['shipping_info'][$key] = $value;
                }
                $setData['shipping_info'] = array_values($setData['shipping_info']);

                foreach ($setData['redelivery_detail'] as $key => $value) {
                    $value['delivery_id'] = $DeliveryTrackingModel->convertToObjectId($value['delivery_id']);
                    $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                    $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
                    $value['delivery_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['delivery_date']);
    
                    $setData['redelivery_detail'][$key] = $value;
                }
                $setData['redelivery_detail'] = array_values($setData['redelivery_detail']);

                $result = $DeliveryTrackingModel->updateByID($deliveryTracking['_id'], $setData);
                break;
            
            case 'bulk':
                # code...
                $thisViewModel->sendError("method bulk not available", 404);
                break;

            default :
                $thisViewModel->sendError("methods parameter is required", 404);
                break;
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
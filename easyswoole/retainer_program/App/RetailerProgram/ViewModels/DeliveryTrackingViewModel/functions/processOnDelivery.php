<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\ViewModels\CourierViewModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function processOnDelivery($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {
        if ($method == 'single') {
            $refNo = $bodyData['ref_no'];

            $DeliveryTrackingModel = new DeliveryTrackingModel();
            $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

            $delivery = $DeliveryTrackingModel->findByRefno($refNo, ['status'=>'ACTIVE']);
            empty($delivery['result']) ?
                $thisViewModel->sendError("delivery data not available", 404) :
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $currDate = date('Y-m-d H:i:s');

            $shippingInfo = $delivery['shipping_info']?:[];
            $lastShippingInfo = end($shippingInfo);
            empty($lastShippingInfo['label']) ?
                $thisViewModel->sendError("invalid operation", 400):null;

            if ($lastShippingInfo['label'] == 'on_processing') {

                $CourierViewModel = new CourierViewModel();
                
                $courier = [];
                $supported = 0;
                if (!empty($delivery['available_courier']) && is_array($delivery['available_courier'])) {
                    foreach ($delivery['available_courier'] as $key => $value) {
                        if ($bodyData['courier'] == $value['courier_code']) {
                            $courier = $CourierViewModel->getCourier($bodyData['courier']);
                            !empty($courier) ?
                                $supported = 1 : null;

                            !in_array($bodyData['delivery_method'], $value['delivery_method']) ?
                                $thisViewModel->sendError("delivery method {$bodyData['delivery_method']} is not supported", 400) :
                                null;

                            empty($value['delivery_service'][$bodyData['delivery_service']]) ?
                                $thisViewModel->sendError("delivery service {$bodyData['delivery_service']} is not supported", 400) :
                                null;
                            
                            $courier['available_service'] = $value['delivery_service'];
                            $courier['origin'] = $value['origin'];
                            $courier['destination'] = $value['destination'];
                            break;
                        }
                    }
                }

                $setData = [
                    'courier'=>$courier['courier_code']?:$bodyData['courier'],
                    'courier_name'=>$courier['courier']?:$bodyData['courier_name'],
                    'delivery_method'=>$bodyData['delivery_method'],
                    'delivery_service'=>$bodyData['delivery_service'],
                    'supported'=>$supported
                ];

                $delivery = array_merge($delivery, $setData);

                if ($supported) {
                    $setData['delivery_cost'] = $courier['available_service'][$bodyData['delivery_service']];

                    $requestDelivery = $CourierViewModel->requestPickup($delivery, $courier, $bodyData['delivery_options']);

                    $setData['awb_number'] = $requestDelivery['awb_number'];
                    $setData['track_history'] = $requestDelivery['tracking_history'];

                    $setData['additional_info'] = $bodyData['delivery_options'];
                    $setData['additional_info']['origin'] = $courier['origin'];
                    $setData['additional_info']['destination'] = $courier['destination'];
                    $setData['additional_info']['origin_branch'] = $courier['origin']['origin_branch'];
                    $setData['additional_info']['dest_branch'] = $courier['destination']['dest_branch'];
                } else {
                    ($bodyData['courier'] != 'others') ?
                        $thisViewModel->sendError("courier {$bodyData['courier']} is not supported", 400):
                        $setData['awb_number'] = $bodyData['awb_number'];
                }

                $delivery['shipping_info'][] = [
                    'label'=>'on_delivery',
                    'title'=>'On Delivery Process',
                    'remarks'=>$bodyData['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];
                $setData['shipping_info'] = $delivery['shipping_info'];

            } elseif ($lastShippingInfo['label'] == 'on_delivery') {
                if ($delivery['supported'] == 0 && $bodyData['courier'] == 'others') {
                    array_pop($delivery['shipping_info']);
                    $delivery['shipping_info'][] = [
                        'label'=>'on_delivery',
                        'title'=>'On Delivery Process',
                        'remarks'=>$bodyData['remarks'],
                        'created_date'=>$lastShippingInfo['created_date'],
                        'updated_date'=>$currDate
                    ];

                    $setData = [
                        'courier'=>$bodyData['courier'],
                        'courier_name'=>$bodyData['courier_name'],
                        'delivery_method'=>$bodyData['delivery_method'],
                        'delivery_service'=>$bodyData['delivery_service'],
                        'supported'=>0,
                        'awb_number'=>$bodyData['awb_number'],
                        'shipping_info'=>$delivery['shipping_info']
                    ];
                }
            } else {
                $thisViewModel->sendError("invalid shipping status", 400);
            }

            foreach ($setData['shipping_info'] as $key => $value) {
                $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);

                $setData['shipping_info'][$key] = $value;
            }
            $setData['shipping_info'] = array_values($setData['shipping_info']);

            $result = $DeliveryTrackingModel->updateByID($delivery['_id'], $setData);
        } elseif ($method == 'bulk') {
            $MediaViewModel = new MediaViewModel();
            $document = $MediaViewModel->uploadDocument($auth, $bodyData, "PROCESS_DELIVERY");

            $SpreadsheetService = new SpreadsheetService();
            $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

            $data = [
                'user_id' => $auth['user_id'],
                'files'=>array_values($files)
            ];
            if (!in_array($document['ext'], ['xls','xlsx'])) {
                $thisViewModel->sendError("only .xls and .xlsx file type are allowed", 400);
            }
            $result = processOnDeliveryBulk($data, $thisViewModel);
        }
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processOnDeliveryBulk($data, &$thisViewModel) {

    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($data['user_id']);

        $CourierViewModel = new CourierViewModel();

        $requiredField = explode(",", "ref_no,courier,delivery_method,delivery_service");
        $currDate = date('Y-m-d H:i:s');
        $processPickup = [];
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                empty($value[$val])? $thisViewModel->sendError("field {$val} on row {$row} is required", 400): null;   
            }

            if (!empty($updateData[$value['ref_no']])) {
                $thisViewModel->sendError("multiple reference_no value \"{$value['ref_no']}\"", 400);
            }

            $delivery = $DeliveryTrackingModel->findByRefno($value['ref_no'], ['status'=>'ACTIVE']);
            empty($delivery['result']) ?
                $thisViewModel->sendError("delivery data not available", 404) :
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $lastShippingInfo = end($delivery['shipping_info']);
            if ($lastShippingInfo['label'] == 'on_processing') {

                $courier = [];
                $supported = 0;
                if (!empty($delivery['available_courier']) && is_array($delivery['available_courier'])) {
                    foreach ($delivery['available_courier'] as $sk => $sval) {
                        if ($value['courier'] == $sval['courier_code']) {
                            $courier = $CourierViewModel->getCourier($value['courier']);
                            !empty($courier) ?
                                $supported = 1 : null;

                            !in_array($value['delivery_method'], $sval['delivery_method']) ?
                                $thisViewModel->sendError("delivery method {$value['delivery_method']} is not supported", 400) :
                                null;

                            empty($sval['delivery_service'][$value['delivery_service']]) ?
                                $thisViewModel->sendError("delivery service {$value['delivery_service']} is not supported", 400) :
                                null;
                            
                            $courier['available_service'] = $sval['delivery_service'];
                            break;
                        }
                    }
                }

                $setData = [
                    'courier'=>$courier['courier_code']?:$value['courier'],
                    'courier_name'=>$courier['courier']?:$value['courier_name'],
                    'delivery_method'=>$value['delivery_method'],
                    'delivery_service'=>$value['delivery_service'],
                    'supported'=>$supported,
                ];

                $delivery = array_merge($delivery, $setData);

                if ($supported) {
                    $setData['delivery_cost'] = $courier['available_service'][$value['delivery_service']];

                    $processPickup[$value['ref_no']] = [
                        'delivery' => $delivery,
                        'courier' => $courier,
                        'options' => $value['delivery_options']
                    ];
                } else {
                    ($value['courier'] != 'others') ?
                        $thisViewModel->sendError("courier {$value['courier']} is not supported", 400):
                        $setData['awb_number'] = $value['awb_number'];
                }

                $delivery['shipping_info'][] = [
                    'label'=>'on_delivery',
                    'title'=>'On Delivery Process',
                    'remarks'=>$value['remarks'],
                    'created_date'=>$currDate,
                    'updated_date'=>$currDate
                ];
                $setData['shipping_info'] = $delivery['shipping_info'];

            } elseif ($lastShippingInfo['label'] == 'on_delivery') {

                if ($delivery['supported'] == 0 && $value['courier'] == 'others') {
                    array_pop($delivery['shipping_info']);
                    $delivery['shipping_info'][] = [
                        'label'=>'on_delivery',
                        'title'=>'On Delivery Process',
                        'remarks'=>$value['remarks'],
                        'created_date'=>$lastShippingInfo['created_date'],
                        'updated_date'=>$currDate
                    ];

                    $setData = [
                        'courier'=>$value['courier'],
                        'courier_name'=>$value['courier_name'],
                        'delivery_method'=>$value['delivery_method'],
                        'delivery_service'=>$value['delivery_service'],
                        'supported'=>0,
                        'awb_number'=>$value['awb_number'],
                        'shipping_info'=>$delivery['shipping_info']
                    ];
                }
            } else {
                $thisViewModel->sendError("invalid shipping status", 400);
            }

            foreach ($setData['shipping_info'] as $sk => $sval) {
                $sval['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['created_date']);
                $sval['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['updated_date']);

                $setData['shipping_info'][$sk] = $sval;
            }
            $setData['shipping_info'] = array_values($setData['shipping_info']);

            $updateData[$value['ref_no']] = [
                'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                'new_value'=>$setData
            ];
        }

        if (!empty($processPickup)) {
            foreach ($processPickup as $key => $value) {
    
                $requestDelivery = $CourierViewModel->requestPickup($value['delivery'], $value['courier'], $value['options']);

                $value['options']['origin_branch'] = $requestDelivery['origin_branch'];
                $value['options']['dest_branch'] = $requestDelivery['dest_branch'];

                $updateData[$key]['new_value'] = array_merge(
                    $updateData[$key]['new_value'],
                    [
                        'awb_number'  => $requestDelivery['awb_number'],
                        'track_history' => $requestDelivery['tracking_history'],
                        'additional_info' => $value['options']
                    ]
                );
            }
        }

        if (!empty($updateData)) {
            $result = $DeliveryTrackingModel->updateBatch($updateData);
        } else {
            $result = ['result'=>['status'=>'no data updated']];
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
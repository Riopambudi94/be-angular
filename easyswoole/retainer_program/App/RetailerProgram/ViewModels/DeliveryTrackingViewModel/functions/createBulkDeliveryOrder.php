<?php

namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\GeneralData\Models\AdministrativeRegionModel;
use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\RetailerProgram\Models\OrderInvoiceModel;

function createBulkDeliveryOrder($arguments, $thisViewModel)
{
    $auth = $arguments[0];

    try {
        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $OrderInvoiceModel = new OrderInvoiceModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

        $search_result = $OrderInvoiceModel->DBfind(['status' => ['$in' => ['PROCESSED', 'COMPLETED']]]);
        $orders = is_array($search_result['result']) ? $search_result['result'] : [];

        if (empty($orders)) {
            $thisViewModel->sendError('Data order not found', 400);
        }

        $originAddress = [
            'name' => 'CLS System',
            'cell_phone' => '02122686277',
            'address' => 'Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160',
        ];

        $originCode = [
            '31.73.03.1003',
            '31.73.03',
            '31.73',
            '31',
        ];

        $newData = [];
        foreach ($orders as $key => $value) {
            $order = $thisViewModel->objectToArray($value);
            $delivery = $DeliveryTrackingModel->DBfind(['order_id' => $order['order_id']]);

            if (!empty($delivery['result'])) {
                $thisViewModel->sendError('data order was migrated', 400);
                break;
            }

            $destAddress = $order['address_detail'];

            $deliveryOrder = [
                'origin' => [
                    'address' => $originAddress,
                    'code' => $originCode,
                ],
                'destination' => [
                    'address' => $destAddress,
                ],
                'product_list' => [],
                'order_id' => $order['order_id'],
                'created_date' => $DeliveryTrackingModel->convertToMongoDateTime($order['approve_date']),
            ];

            if (!empty($destAddress['name']) && !empty($destAddress['address']) && !empty($destAddress['cell_phone']) && !empty($destAddress['city_name']) && !empty($destAddress['district_name'])) {
                $AdministrativeRegionModel = new AdministrativeRegionModel();

                $filter = [
                    'province' => $destAddress['province_name'],
                    'city' => $destAddress['city_name'],
                    'subdistrict' => $destAddress['district_name'],
                ];
                $getDest = $AdministrativeRegionModel->findRegion($filter);

                if (!empty($getDest['result'])) {
                    $getDest = $thisViewModel->objectToArray($getDest['result'][0]);
                    $destCode = [
                        $getDest['subdistrict_code'],
                        $getDest['city_code'],
                        $getDest['province_code'],
                    ];

                    $deliveryOrder['destination']['code'] = $destCode;
                }
            }

            if ($order['status'] == 'COMPLETED') {
                $deliveryOrder['status'] = $order['status'];
                $deliveryOrder['dimensions'] = [
                        'length' => 1,
                        'width' => 1,
                        'height' => 1,
                    ];
                $deliveryOrder['weight'] = '1.0';
                $deliveryOrder['volumetric_weight'] = '1.0';
                $deliveryOrder['shipping_info'] = $order['shipping_info'];
                $deliveryOrder['courier'] = $order['delivery_detail']['courier'];
                $deliveryOrder['courier_name'] = $order['delivery_detail']['courier'];
                $deliveryOrder['supported'] = array_key_exists('available_courier', $order) ? 1 : 0;
                $deliveryOrder['available_courier'] = array_key_exists('available_courier', $order) ? $order['available_courier'] : [];
                $deliveryOrder['delivery_method'] = $order['delivery_detail']['delivery_method'];
                $deliveryOrder['delivery_service'] = $order['delivery_detail']['delivery_service'];
                $deliveryOrder['awb_number'] = $order['delivery_detail']['awb_number'];
                $deliveryOrder['delivery_cost'] = null;
                $deliveryOrder['track_history'] = array_key_exists('track_history', $order) ? $order['track_history'] : [];
            } elseif ($order['status'] == 'PROCESSED') {
                $deliveryOrder['status'] = 'PENDING';
            }

            $tempNewData = [];
            $i = 0;
            foreach ($order['products'] as $key => $value) {
                if (in_array($value['type'], ['voucher', 'product', 'gold'])) {
                    $deliveryType = $value['type'];

                    if (empty($tempNewData[$deliveryType])) {
                        $tempNewData[$deliveryType] = $deliveryOrder;
                        $tempNewData[$deliveryType]['reference_no'] = $order['order_id'].'-'.++$i;
                        $tempNewData[$deliveryType]['delivery_type'] = $deliveryType;
                        $tempNewData[$deliveryType]['package_value'] = 0;
                        $tempNewData[$deliveryType]['package_qty'] = 0;
                    }
                    $tempNewData[$deliveryType]['product_list'][] = $value;
                    $tempNewData[$deliveryType]['total_item_value'] += $value['total_product_price'];
                    $tempNewData[$deliveryType]['total_item_qty'] += $value['quantity'];
                }
            }

            $newData = array_merge($newData, array_values($tempNewData));
        }
        if (empty($newData)) {
            $thisViewModel->sendError('all data orders with status COMPLETED already exists in data delivery', 400);
        }

        $result = $DeliveryTrackingModel->insertBatch($newData);

        if ($result['result']['status'] != 'success') {
            $thisViewModel->sendError('failed create data delivery', 400);
        }

        $resultBackup = $OrderInvoiceModel->createBackupCollection();

        if ($resultBackup['error'] == true) {
            $thisViewModel->sendError('failed created data backup', 400);
        }

        $resultRemovedFields = $OrderInvoiceModel->removedFields(['shipping_info', 'delivery_detail']);

        unset($result['result']['_id']);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

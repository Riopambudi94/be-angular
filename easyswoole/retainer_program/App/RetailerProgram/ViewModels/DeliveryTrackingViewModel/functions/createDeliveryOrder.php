<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\Models\AdministrativeRegionModel;

function createDeliveryOrder($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderInvoice = $arguments[1];

    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

        $delivery = $DeliveryTrackingModel->DBfind(['order_id'=>$orderInvoice['order_id']]);
        !empty($delivery['result']) ?
            $thisViewModel->sendError("delivery order for order {$orderInvoice['order_id']} already exists", 400) : null;

        // shipper origin address from CLS System
        // Shipper origin might be adjusted for each Courier Supported By System
        $originAddress = [
            'name'=>'CLS System',
            'cell_phone'=>'02122686277',
            'address'=>'Jl. Kebon Jeruk VII No.2E, RT.4/RW.5, Maphar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11160'
        ];
        $originCode = [
            '31.73.03.1003',
            '31.73.03',
            '31.73',
            '31'
        ];

        $destAddress = $orderInvoice['address_detail'];

        $deliveryOrder = [
            'origin'=>[
                'address'=>$originAddress,
                'code'=>$originCode
            ],
            'destination'=>[
                'address'=>$destAddress
            ],
            'product_list'=>[],
            'order_id'=>$orderInvoice['order_id'],
            'status'=>'PENDING'
        ];

        if (!empty($destAddress['name']) && !empty($destAddress['address']) && !empty($destAddress['cell_phone']) && !empty($destAddress['city_name']) && !empty($destAddress['district_name'])) {

            $AdministrativeRegionModel = new AdministrativeRegionModel();

            $filter = [
                'province'=>$destAddress['province_name'],
                'city'=>$destAddress['city_name'],
                'subdistrict'=>$destAddress['district_name']
            ];
            $getDest = $AdministrativeRegionModel->findRegion($filter);

            if (!empty($getDest['result'])) {
                $getDest = $thisViewModel->objectToArray($getDest['result'][0]);
                $destCode = [
                    $getDest['subdistrict_code'],
                    $getDest['city_code'],
                    $getDest['province_code']
                ];

                $deliveryOrder['destination']['code'] = $destCode;
            }
        }

        $newData = [];
        $i = 0;
        foreach ($orderInvoice['products'] as $key => $value) {
            if (in_array($value['type'], ['voucher', 'product', 'gold'])) {
                $deliveryType = $value['type'];

                if (empty($newData[$deliveryType])) {
                    $newData[$deliveryType] = $deliveryOrder;
                    $newData[$deliveryType]['reference_no'] = $orderInvoice['order_id'].'-'.++$i;
                    $newData[$deliveryType]['delivery_type'] = $deliveryType;
                    $newData[$deliveryType]['package_value'] = 0;
                    $newData[$deliveryType]['package_qty'] = 0;
                }
                $newData[$deliveryType]['product_list'][] = $value;
                $newData[$deliveryType]['total_item_value'] += $value['total_product_price'];
                $newData[$deliveryType]['total_item_qty'] += $value['quantity'];
            }
        }

        !empty($newData) ?
            $result = $DeliveryTrackingModel->insertBatch($newData): null;

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
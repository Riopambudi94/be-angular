<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;

function getAvailableStock($arguments, $thisViewModel) {
    $productCode = $arguments[0];
    $skuCode = $arguments[1];
    $limit = $arguments[3];

    try {

        $EvoucherModel = new EvoucherModel();

        $result = $EvoucherModel->getAvailableStock($productCode, $skuCode, $limit);
        empty($result['result']) ?
            $thisViewModel->sendError("no evoucher stock available", 400):
            $result = $thisViewModel->objectToArray($result['result']);

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findReportSummary($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {

        $EvoucherModel = new EvoucherModel();

        if (empty($requested_columns)) {
            $requested_columns = '_id,product_id,product_code,product_name,expiry_date,qty_available,qty_sold,qty_used,qty_inactive,qty_expired,redeem_type,product';
        }
        $requested_columns = explode(",", $requested_columns);

        $EvoucherModel->requestColumns .= ',product_name,qty_available,qty_sold,qty_used,qty_inactive,qty_expired,redeem_type,product';
        $additionalMapping = [
            '_id'=>0,
            'product_name'=>'$product_name',
            'qty_available'=>'$qty_available',
            'qty_sold'=>'$qty_sold',
            'qty_used'=>'$qty_used',
            'qty_inactive'=>'$qty_inactive',
            'qty_expired'=>'$qty_expired',
            'redeem_type'=>'$redeem_type',
            'product'=>'$product'
        ];
        $EvoucherModel->requestMapping = array_merge(
            $EvoucherModel->requestMapping,
            $additionalMapping
        );

        if ($download) {
            $result_db = $EvoucherModel->findReportSummary($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, "EVOUCHER_SUMMARY");
            }
            
        } else {
            $result = $EvoucherModel->findReportSummary($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
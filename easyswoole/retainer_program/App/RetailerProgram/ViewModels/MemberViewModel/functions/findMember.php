<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function findMember($arguments, $thisViewModel) {
    $search = $arguments[0];
    try {

        $MemberModel = new MemberModel();

        $result_DB = $MemberModel->find($search);
        return $result_DB;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\Models\ConfigurationModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function updateByInputFormBulk($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "MEMBER_UPDATE");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>$files
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            $result = formUpdateXlsx($data, $thisViewModel);
        } elseif (in_array($document['ext'], ['csv'])) {
            
        }
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function formUpdateXlsx($data, &$thisViewModel) {
    try {
        
        $MemberModel = new MemberModel();
        
        $bulkUpdate = [];
        foreach ($data['files'] as $key => $value) {
            $result = [];

            $username = trim(strval($value['member_id']));
            $getMember = $MemberModel->findByUsernameOrId($username, "ACTIVE");
            if (empty($getMember['result'])) {
                $thisViewModel->sendError("Member with username {$username} does not exists", 404);
            }

            $getMember = $thisViewModel->objectToArray($getMember['result'][0]);
            $memberId = $getMember['_id'];
            
            $ktpPemilik = trim(strval($value['url_ktp_pemilik']));
            $ktpPenerima = trim(strval($value['url_ktp_penerima']));
            $npwpPemilik = trim(strval($value['url_npwp_pemilik']));
            $npwpPenerima = trim(strval($value['url_npwp_penerima']));

            !empty($ktpPemilik) ?
                $getMember['input_form_data']['foto_ktp_pemilik'] = $ktpPemilik : null;

            !empty($ktpPenerima) ?
                $getMember['input_form_data']['foto_ktp_penerima'] = $ktpPenerima : null;

            !empty($npwpPemilik) ?
                $getMember['input_form_data']['foto_npwp_pemilik'] = $npwpPemilik : null;

            !empty($npwpPenerima) ?
                $getMember['input_form_data']['foto_npwp_penerima'] = $npwpPenerima : null;

            $bulkUpdate[] = [
                'filter'=>['_id'=>$MemberModel->convertToObjectId($memberId)],
                'new_value'=>[
                    'input_form_data'=>$getMember['input_form_data']
                ]
            ];
        }

        if (!empty($bulkUpdate))
            $result = $MemberModel->updateBatch($bulkUpdate);
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function formUpdateXlsx2($data, &$thisViewModel) {
    try {
        $ConfigurationModel = new ConfigurationModel();
        $getConfig = $ConfigurationModel->findByKeyName('member-input-form')['result'][0];
        $formFields = $thisViewModel->objectToArray($getConfig->values->input_form);
        $fieldName = $thisViewModel->objectToArray($getConfig->values->field_name);

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($data['user_id']);
        
        $bulkUpdate = [];
        foreach ($data['files'] as $key => $value) {
            $result = [];

            foreach ($formFields as $k => $val) {
                $fieldValue = trim($value[$k]);
                if (empty($fieldValue) && !empty($val['alias'])) {
                    foreach ($val['alias'] as $sk => $sval) {
                        if (!empty($value[$sval])) {
                            $fieldValue = trim($value[$sval]);
                            break;
                        }
                    }
                }
                if ($val['required'] && (empty($fieldValue) || $fieldValue == "-")) {
                    $thisViewModel->sendError("field {$k} is required", 400);
                }
                $result[$k] = $fieldValue?:'-';
            }

            $formData = [];
            foreach ($fieldName as $sk => $sval) {
                foreach ($sval as $k => $val) {
                    $data = trim($result[$val]," -\t\n\r\0\x0B");
                    if (!empty($data)) {
                        $formData[$sk] = $data;
                        break;
                    }   
                }
            }
            $formData['form_input'] = $result;

            $getMember = $MemberModel->findByUsernameOrId($formData['username'], "ACTIVE");
            if (empty($getMember['result'])) {
                $thisViewModel->sendError("Member with username {$formData['username']} does not exists", 404);
            }

            $getMember = $thisViewModel->objectToArray($getMember['result'][0]);
            $memberId = $getMember['_id'];

            $memberAddress = [
                '_id'=>$MemberModel->convertToObjectId(null, true)->__toString(),
                'name'=>$formData['receiver_name'],
                'cell_phone'=>$formData['receiver_cell_phone'],
                'address'=>$formData['address'],
                'province_name'=>$formData['province'],
                'city_name'=>$formData['city'],
                'district_name'=>$formData['district'],
                'village_name'=>$formData['village'],
                'postal_code'=>$formData['postal_code'],
                'use_as'=>'main_address'
            ];

            $addressList = [];
            if (!empty($getMember['member_address'])) {
                foreach ($getMember['member_address'] as $sk => $sval) {
                    if ((!empty($memberAddress['_id'])) && $sval['id'] == $memberAddress['_id']) {
                        continue;
                    }
                    $sval['use_as'] = 'secondary_address';
                    $addressList[] = $sval;
                }
            }
            $addressList[] = $memberAddress;

            $updateData = [
                // 'username'=>$formData['id_pel'],
                'full_name'=>$formData['full_name'],
                'cell_phone'=>$formData['cell_phone'],
                'member_address'=>$addressList,
                'input_form_data'=>$formData['form_input']
            ];
            if (!empty($value['status']) && in_array($value['status'], ['ACTIVE', 'INACTIVE'])) {
                $updateData['status'] = $value['status'];
            }

            $bulkUpdate[] = [
                'filter'=>['_id'=>$MemberModel->convertToObjectId($memberId)],
                'new_value'=>$updateData
            ];
        }

        if (!empty($bulkUpdate))
            $result = $MemberModel->updateBatch($bulkUpdate);
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
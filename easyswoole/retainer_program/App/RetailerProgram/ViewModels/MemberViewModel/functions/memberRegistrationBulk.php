<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\Models\ConfigurationModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use App\GeneralData\ViewModels\ReportViewModel;
use Services\SpreadsheetService;

function memberRegistrationbulk($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {
        
        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "MEMBER_REGISTRATION");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>$files
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            $result = processXlsx($data, $thisViewModel);
        } elseif (in_array($document['ext'], ['csv'])) {
            
        }

        $ReportViewModel = new ReportViewModel();
        $result = $ReportViewModel->generateReport($result, "MEMBER_REGISTRATION");

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processXlsx($data, &$thisViewModel) {

    try {
        $ConfigurationModel = new ConfigurationModel();
        $getConfig = $ConfigurationModel->findByKeyName('member-input-form')['result'][0];
        $formFields = $thisViewModel->objectToArray($getConfig->values->input_form);
        $fieldName = $thisViewModel->objectToArray($getConfig->values->field_name);

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($data['user_id']);

        $CryptoViewModel = new CryptoViewModel();

        $returnResult = [];
        $insertData = [];
        foreach ($data['files'] as $key => $value) {
            $result = [];

            isset($insertData[trim($value['username'])]) ?
                $thisViewModel->sendError("multiple member data {$value['username']}", 400) : null;

            foreach ($formFields as $k => $val) {

                // if (!isset($value[$k])) continue;

                $fieldValue = trim($value[$k]?:'');
                if (empty($fieldValue) && !empty($val['alias'])) {
                    foreach ($val['alias'] as $sk => $sval) {
                        if (!empty($value[$sval])) {
                            $fieldValue = trim($value[$sval]);
                            break;
                        }
                    }
                }

                if ($val['required'] && (empty($fieldValue) || $fieldValue == "-")) {
                    $thisViewModel->sendError("field {$k} is required", 400);
                }
                
                $result[$k] = $fieldValue?:'-';
            }

            

            $formData = [];
            foreach ($fieldName as $sk => $sval) {
                foreach ($sval as $k => $val) {
                    $data = trim($result[$val]," -\t\n\r\0\x0B");
                    if (!empty($data)) {
                        $formData[$sk] = $data;
                        break;
                    }   
                }
            }
            $formData['form_input'] = $result;
            
            $validBody = $thisViewModel->validateBodyRegister($formData);

            $findMember = $MemberModel->findByUsernameOrId($validBody['username']);
            if (!empty($findMember['result'])) {
                $thisViewModel->sendError("Member with username {$validBody['username']} already exists", 409);
            }

            /* $validBody['points']    = [
                'point_balance'=> 0 ,
                'expire_date'  => date('Y-m-d H:i:s'),
                'exp_date'     => time()
            ]; */
    
            $memberAddress = [
                '_id'=>$MemberModel->convertToObjectId(null, true)->__toString(),
                'name'=>$formData['receiver_name'],
                'cell_phone'=>$formData['receiver_cell_phone'],
                'address'=>$formData['address'],
                'province_name'=>$formData['province'],
                'city_name'=>$formData['city'],
                'district_name'=>$formData['district'],
                'village_name'=>$formData['village'],
                'postal_code'=>$formData['postal_code'],
                'use_as'=>'main_address'
            ];
    
            $validBody['member_address'][] = $memberAddress;
            $validBody['status'] = 'ACTIVE';
            $validBody['input_form_data'] = $formData['form_input'];

            $returnResult[] = [
                'username'=>$validBody['username'],
                'password'=>"'".$validBody['password']
            ];
            $validBody['password'] = $CryptoViewModel->generatePassword($validBody['password']);

            $insertData[$formData['username']] = $validBody;
        }

        if (!empty($insertData))
            $result = $MemberModel->insertBatch($insertData);
        
        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }

}
<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function updateMemberAddress($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($auth['user_id']);

        $member = $MemberModel->findByUsernameOrId($auth['subject_id']);
        $member = $thisViewModel->objectToArray($member['result'][0]);

        $addressDetail = [
            '_id'=>$bodyData['_id']?:$MemberModel->convertToObjectId(null, true)->__toString(),
            'name'=>$bodyData['name'],
            'cell_phone'=>$bodyData['dell_phone'],
            'address'=>$bodyData['address'],
            'province_name'=>$bodyData['province_name'],
            'city_name'=>$bodyData['city_name'],
            'district_name'=>$bodyData['district_name'],
            'village_name'=>$bodyData['village_name'],
            'postal_code'=>$bodyData['postal_code'],
            'use_as'=>'main_address'
        ];

        $memberAddress = [];
        if (!empty($member['member_address'])) {
            foreach ($member['member_address'] as $key => $value) {
                if ((!empty($addressDetail['_id'])) && $value['id'] == $addressDetail['_id']) {
                    continue;
                }
                $value['use_as'] = 'secondary_address';
                $memberAddress[] = $value;
            }
        }
        $memberAddress[] = $addressDetail;

        $updateAddress = $MemberModel->updateByID($auth['subject_id'], ['member_address'=>$memberAddress]);

        return $addressDetail;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class PointsInventoryModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dtm_points_inventory";

    public $requestColumns = "_id,item_code,user_id,points,expiry_date,transaction_month,status,created_date,updated_date,deleted_date,count";
    public $requestMapping = [
        '_id' => '$_id',
        'item_code'=>'$item_code',
        'user_id'=>'$user_id',
        'points'=>'$points',
        'expiry_date'=>'$expiry_date',
        'transaction_month'=>'$transaction_month',
        'status'=>'$status',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            $pipeline[] = ['$project' => $columnMapping];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            unset($pipeline[2]);
            unset($pipeline[3]);
            unset($pipeline[4]);

            $countPipeline = $pipeline;
            $countPipeline[2] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($countPipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function getPointByUserId($userId, $search = []) {
        try {

            $userId = $this->convertToObjectId($userId);

            $match = ['user_id'=>$userId];
            if (!empty($search['status']))
                $match['status'] = $search['status'];
            if (!empty($search['expiry_date']))
                $match['expiry_date'] = ['$gte'=>$this->convertToMongoDateTime($search['expiry_date'])];
            if (!empty($search['transaction_month']))
                $match['transaction_month'] = $search['transaction_month'];
            if (!empty($search['item_code']))
                $match['item_code'] = $search['item_code'];
            if ($search['points_exists'])
                $match['points'] = ['$gt'=>0];

            $pipeline = [
                [
                    '$match'=>$match
                ]
            ];

            if ($search['get_total_points']) {
                $pipeline[] = [
                    '$group'=>[
                        '_id'=>null,
                        'points'=>['$sum'=>'$points'],
                    ]
                ];
            }

            return $this->DBaggregate($pipeline);
            
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class PackingListModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dth_packing_list";

    public $requestColumns = "_id,packing_no,reference_no,courier_code,courier_name,delivery_method,delivery_service,status,created_date,updated_date,deleted_date,delivery_date,count";
    public $requestMapping = [
        '_id' => '$_id',
        'packing_no'=>'$packing_no',
        'reference_no'=>'$reference_no',
        'courier_code'=>'$courier_code',
        'courier_name'=>'$courier_name',
        'delivery_method'=>'$delivery_method',
        'delivery_service'=>'$delivery_service',
        'delivery_date'=>'$delivery_date',
        'status'=>'$status',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $sliceLen = 3;

            $pipeline = [];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$lookup'=>[
                            'from'=>'dth_delivery_tracking',
                            'let'=>['ref_no'=>'$reference_no'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$in'=>['$reference_no','$$ref_no']
                                        ]
                                    ]
                                ],
                                [
                                    '$sort'=>['_id'=>-1]
                                ],
                                [
                                    '$unset'=>['_id']
                                ],
                                [
                                    '$addFields'=>[
                                        'origin'=>'$origin.address',
                                        'destination'=>'$destination.address',
                                    ]
                                ],
                                [
                                    '$unwind'=>'$product_list'
                                ],
                                [
                                    '$group'=>[
                                        '_id'=>null,
                                        'total_price'=>['$sum'=>'$products.total_product_price'],
                                        'total_quantity'=>['$sum'=>'$products.quantity'],
                                        'delivery_list'=>[
                                            '$push'=>'$$ROOT'
                                        ]
                                    ]
                                ]
                            ],
                            'as'=>'delivery_list'
                        ]
                    ],
                    [
                        '$replaceRoot'=>[
                            'newRoot'=>[
                                '$mergeObjects'=>[
                                    '$$ROOT',
                                    ['$arrayElemAt'=>['$delivery_list', 0]]
                                ]
                            ]
                        ]
                    ]
                ]
            );
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function getDetail($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }

            $pipeline = [];

            !empty($filter) ? $pipeline[] = ['$match'=>$filter] : null;

            $pipeline[] = [
                '$lookup'=>[
                    'from'=>'dth_order_invoice',
                    'let'=>['ref_no'=>'$reference_no'],
                    'pipeline'=>[
                        [
                            '$match'=>[
                                '$expr'=>[
                                    '$in'=>['$order_id','$$ref_no']
                                ]
                            ]
                        ],
                        [
                            '$project'=>[
                                '_id'=>0,
                                'products'=>'$products',
                                'address_detail'=>'$address_detail',
                                'order_id'=>'$order_id',
                                'status'=>'$status',
                                'po_no'=>'$po_no'
                            ]
                        ],
                        [
                            '$unwind'=>'$products'
                        ]
                    ],
                    'as'=>'order_list'
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
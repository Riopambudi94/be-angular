<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class EvoucherStockModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dth_evoucher_stock";

    public $requestColumns = "_id,process_number,process_type,status,total_sku,total_qty,total_value,total_price,request_date,request_at,request_by,approve_date,approve_at,approve_by,cancel_date,cancel_at,cancel_by,created_date,updated_date,deleted_date,count";
    public $requestMapping = [
        '_id' => '$_id',
        'process_number'=>'$process_number', 
        'process_type'=>'$process_type',
        'status'=>'$status',
        'total_sku'=>'$total_sku',
        'total_qty'=>'$total_qty',
        'total_value'=>'$total_value',
        'total_price'=>'$total_price',
        'request_date'=>'$request_date',
        'request_at'=>'$request_at',
        'request_by'=>'$request_by',
        'approve_date'=>'$approve_date',
        'approve_at'=>'$approve_at',
        'approve_by'=>'$approve_by',
        'cancel_date'=>'$cancel_date',
        'cancel_at'=>'$cancel_at',
        'cancel_by'=>'$cancel_by',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            $pipeline[] = ['$project' => $columnMapping];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            unset($pipeline[2]);
            unset($pipeline[3]);
            unset($pipeline[4]);

            $countPipeline = $pipeline;
            $countPipeline[2] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($countPipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findStockDetail($filter = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $this->requestColumns .= ",sku_list";
            $this->requestMapping['sku_list'] = '$sku_list';

            if (!empty($filter['process_number'])) {
                is_string($filter['process_number']) ? $filter['process_number'] = array($filter['process_number']) : null;
                $filter['process_number'] = ['$in'=>$filter['process_number']];
            }

            $pipeline = [];
            if (!empty($filter)) {
                $pipeline[] = [
                    '$match'=>$filter
                ];
            }

            $pipeline = array_merge($pipeline,
                [
                    [
                        '$lookup'=>[
                            'from'=>'dtm_evoucher',
                            'let'=>['process_number'=>'$process_number'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$process_number','$$process_number']]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    '$group'=>[
                                        '_id'=>['process_number'=>'$process_number', 'sku_code'=>'$sku_code'],
                                        'sku_code'=>['$first'=>'$sku_code'],
                                        'qty'=>['$sum'=>1]
                                    ]
                                ],
                                ['$project'=>['_id'=>0]]
                            ],
                            'as'=>'evoucher'
                        ]
                    ],
                    [
                        '$addFields'=>[
                            'sku_list'=>'$evoucher'
                        ]
                    ]
                ]
            );
            
            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
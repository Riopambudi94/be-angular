<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class DeliveryTrackingModel extends MongoModel
{
    public $table;
    public $collectionName = "dth_delivery_tracking";
    public $requestColumns = "_id,origin,destination,product_list,total_item_value,total_item_qty,order_id,reference_no,dimensions,weight,volumetric_weight,shipping_info,status,courier,courier_name,supported,available_courier,delivery_type,delivery_method,delivery_service,awb_number,delivery_cost,track_history,packing_no,in_packing_list,additional_info,created_date,updated_date,count,return_detail,redelivery_detail";

    public $requestMapping = [
        '_id' => '$_id',
        'origin'=>'$origin',
        'destination'=>'$destination',
        'product_list' => '$product_list',
        'total_item_value' => '$total_item_value',
        'total_item_qty' => '$total_item_qty',
        'order_id' => '$order_id',
        'reference_no' => '$reference_no',
        'dimensions'=>'$dimensions',
        'weight'=>'$weight',
        'volumetric_weight'=>'$volumetric_weight',
        'shipping_info'=>'$shipping_info',
        'status'=>'$status',
        'courier'=>'$courier',
        'courier_name'=>'$courier_name',
        'supported'=>'$supported',
        'available_courier'=>'$available_courier',
        'delivery_type'=>'$delivery_type',
        'delivery_method'=>'$delivery_method',
        'delivery_service'=>'$delivery_service',
        'awb_number'=>'$awb_number',
        'delivery_cost'=>'$delivery_cost',
        'track_history'=>'$track_history',
        'packing_no'=>'$packing_no',
        'in_packing_list'=>'$in_packing_list',
        'additional_info'=>'$additional_info',
        'return_detail'=>'$return_detail',
        'redelivery_detail'=>'$redelivery_detail',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date'
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } 
                elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
                elseif (strpos($key, '.') !== false) {
                    $keyName = explode('.', $key);
                    if (!empty($this->requestMapping[$keyName[0]])) {
                        $requestMatch[$key] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            $pipeline = [
                [
                    '$addFields'=>[
                        'last_shipping_info'=>['$arrayElemAt'=>['$shipping_info.label', -1]]
                    ]
                ]
            ];
            $sliceLen = 3;

            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function findByRefno($refNo, $match = [])
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $filter = ['reference_no'=>$refNo];
            if (!empty($match['status'])) {
                !is_array($match['status']) ? 
                    $match['status'] = [$match['status']]:
                    null;

                $filter['status'] = ['$in'=>$match['status']];
            }

            return $this->DBfind($filter);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function getAvailableCourierList () {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try{
            $pipeline = [
                [
                    '$project' => [
                        '_id'=>0,
                        'courier' => '$courier',
                        'courier_code' => '$courier_code',
                        'delivery_service'=>'$delivery_service',
                        'delivery_method'=>'$delivery_method'
                    ]
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function findAll()
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBfind();
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function update($_id, $getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBupdate(['_id'=> new MongoDB\BSON\ObjectId("$_id")], $getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function remove($_id)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBdelete(['_id'=> new MongoDB\BSON\ObjectId("$_id")], $getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function insert($getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($getBody);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    function findOnDelivery() {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {

            $pipeline = [
                [
                    '$addFields'=>[
                        'last_shipping_info'=>[
                            '$arrayElemAt'=>['$shipping_info.label', -1]
                        ]
                    ]
                ],
                [
                    '$match'=>[
                        'last_shipping_info'=>'on_delivery',
                        'status'=>'ACTIVE',
                        'supported'=>1
                    ]
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}

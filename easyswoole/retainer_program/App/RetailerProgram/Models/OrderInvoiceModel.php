<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class OrderInvoiceModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dth_order_invoice";

    public $requestColumns = "_id,user_id,products,order_id,status,sum_total,total_quantity,shipping_info,address_detail,delivery_detail,return_detail,available_courier,redelivery_detail,shoppingcart_id,order_expire_date,additional_info,invoice_no,po_no,request_date,approve_date,cancel_date,count,created_date,points_by";
    public $requestMapping = [
        '_id' => '$_id',
        'user_id'=>'$user_id',
        'products'=>'$products',
        'order_id'=>'$order_id',
        'status'=>'$status',
        'sum_total'=>'$sum_total',
        'total_quantity'=>'$total_quantity',
        'shipping_info'=>'$shipping_info',
        'address_detail'=>'$address_detail',
        'delivery_detail'=>'$delivery_detail',
        'available_courier'=>'$available_courier',
        'return_detail'=>'$return_detail',
        'redelivery_detail'=>'$redelivery_detail',
        'shoppingcart_id'=>'$shoppingcart_id',
        'order_expire_date'=>'$order_expire_date',
        'additional_info'=>'$additional_info',
        'invoice_no'=>'$invoice_no',
        'po_no'=>'$po_no',
        'request_date'=>'$request_date',
        'approve_date'=>'$approve_date',
        'cancel_date'=>'$cancel_date',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
        'points_by'=>'$points_by'
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } 
                elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
                else if (strpos($key, '.') !== false) {
                    $keyName = explode('.', $key);
                    if (!empty($this->requestMapping[$keyName[0]])) {
                        $requestMatch[$key] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            $pipeline = [
                [
                    '$lookup'=>[
                        'from'=>'dtm_member',
                        'let'=>['user_id'=>'$user_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$eq'=>['$_id','$$user_id']
                                    ]
                                ]
                            ]
                        ],
                        'as'=>'member_detail'
                    ]
                ],
                [
                    '$addFields'=>[
                        'username'=>['$arrayElemAt'=>['$member_detail.username',0]],
                        'member_detail'=>['$arrayElemAt'=>['$member_detail.input_form_data',0]],
                    ]
                ],
            ];
            $sliceLen = 3;

            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findReportSalesOrder($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } 
                else if ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
                else if (strpos($key, '.') !== false) {
                    $keyName = explode('.', $key);
                    if (!empty($this->requestMapping[$keyName[0]])) {
                        $requestMatch[$key] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            $match = [
                'status'=>['$in'=>['PROCESSED','COMPLETED']],
            ];
            if ($request['based_on'] == 'bast'){
                $match['additional_info.bast'] = ['$exists'=>true];
            } elseif ($request['based_on'] == 'invoice_no') {
                $match['invoice_no'] = ['$exists'=>true];
            } elseif ($request['based_on'] == 'po_no') {
                $match['po_no'] = ['$exists'=>true];
            }

            $pipeline = [
                [
                    '$match'=>$match
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_member',
                        'let'=>['user_id'=>'$user_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$eq'=>['$_id','$$user_id']
                                    ]
                                ]
                            ]
                        ],
                        'as'=>'member_detail'
                    ]
                ],
                [
                    '$addFields'=>[
                        'id_toko'=>['$arrayElemAt'=>['$member_detail.input_form_data.id_pel',0]],
                        'nama_toko'=>['$arrayElemAt'=>['$member_detail.input_form_data.nama_toko',0]],
                        'nama_pemilik'=>['$arrayElemAt'=>['$member_detail.input_form_data.nama_pemilik',0]],
                        'no_wa_pemilik'=>['$arrayElemAt'=>['$member_detail.input_form_data.no_wa_pemilik',0]],
                        'alamat_toko'=>['$arrayElemAt'=>['$member_detail.input_form_data.alamat_toko',0]],
                        'group'=>['$arrayElemAt'=>['$member_detail.input_form_data.description',0]],
                        'member_detail'=>[
                            '$arrayElemAt'=>['$member_detail.input_form_data',0]
                        ],
                    ]
                ],
                [
                    '$unwind'=>'$products'
                ],
                [
                    '$match'=>[
                        'products.cancel_detail' =>[
                            '$exists' => false
                            ]
                        ]
                ]
            ];
            $sliceLen = 7;
            
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByOrderId($orderId, $search = [], $getDetails = false) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = ['order_id'=>$orderId];
            if (!empty($search['status'])) {
                !is_array($search['status']) ? $search['status'] = array($search['status']): null;
                $match['status'] = ['$in'=>$search['status']];
            }
            if (!empty($search['user_id']))
                $match['user_id'] = $this->convertToObjectId($search['user_id']);
            if (!empty($search['shoppingcart_id']))
                $match['shoppingcart_id'] = $this->convertToObjectId($search['shoppingcart_id']);
            if (!empty($search['order_expire_date']))
                $match['order_expire_date'] = ['$gte'=>$this->convertToObjectId($search['order_expire_date'])];

            $pipeline = [
                [
                    '$match'=>$match
                ],
            ];

            if ($getDetails) {
                $pipeline = array_merge(
                    $pipeline,
                    [
                        [
                            '$lookup'=>[
                                'from'=>'dtm_member',
                                'let'=>['user_id'=>'$user_id'],
                                'pipeline'=>[
                                    [
                                        '$match'=>[
                                            '$expr'=>[
                                                '$eq'=>['$_id','$$user_id']
                                            ]
                                        ]
                                    ]
                                ],
                                'as'=>'member_detail'
                            ]
                        ],
                        [
                            '$addFields'=>[
                                'username'=>['$arrayElemAt'=>['$member_detail.username',0]],
                                'member_detail'=>['$arrayElemAt'=>['$member_detail.input_form_data',0]],
                            ]
                        ],
                    ]
                );
            }

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function removedFields($fields = []){
        try{
            $bulk   = new \MongoDB\Driver\BulkWrite(['upserted' => false]);
            $removedFields = [];
            foreach($fields as $key => $value){
                $removedFields[$value] = '';
            }
            $bulk->update([],['$unset' => $removedFields], ['multi' => true]);
            $result = $this->MongoClient->executeBulkWrite($this->table, $bulk);
                $dbRes  = [
                            'status'=>'success',
                        ];
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
        return $this->sendResult($dbRes);
    }

    function createBackupCollection($replace = false){
        if(!$replace){
            try{
                $query = new \MongoDB\Driver\Query([],[]);
                
                $rows  = $this->MongoClient->executeQuery($this->dbName.".".$this->collectionName.'_backup',$query);
    
                $res = $this->sendResult($this->clear_result_format($rows));
    
                $backupData = is_array($res['result']) ? $res['result'] : [];
    
                if(count($backupData) > 0) return array('result' => [], 'error' => true);
            }
            catch (\Exception $e) {
                $this->sendError($e->getMessage(), 500);
            }
        }
           
        $pipeline = [
            [
                '$out'=> [
                    'db' => $this->dbName,
                    'coll' => $this->collectionName.'_backup'
                ]
            ]
        ];

        try {
            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class ShoppingcartModel extends MongoModel
{

    public $table;
    public $collectionName = "dtm_shoppingcart";
    public $requestColumns = '_id,user_id,status,total_price,total_quantity,products,address_detail,order_id,billings,shipping,product_os,product_is';

    public function findAllComplete($request = array(), $orderBy = array(), $pageNo = 1, $limit=50)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            foreach ($request as $key=>$value) {
                if (trim($value)=='' || empty($value)) {
                    unset($request[$key]);
                } else {
                    $search_query   = $this->createRegex($value, 'i');
                    $request [$key] = ['$regex' => $search_query];
                }
            }
           
            $filter = [];

            if (count($request)!=0) {
                $filter = $request;
            }

            $sort   = ['_id'=>-1];
            $skip   = $this->parsePageToSkip($pageNo, $limit);
            $opt    = $skip;

            if (isset($orderBy) && is_array($orderBy) && !empty($orderBy)) {
                foreach ($orderBy as $key=>$ord) {
                    $sort[$key] = $ord['asc']?1:-1;
                }
            }
            $opt['sort'] = $sort;
            return $this->DBFind($filter, $opt);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function findActiveCartByUserID($userId)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $userId = $this->convertToObjectId($userId);
            $filter = [
                'user_id' => $userId,
                'status' => 'ACTIVE'
            ];
            $options = ['limit' => 1];  
            return $this->DBfind($filter, $options);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function update($_id, $getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $result = $this->DBupdate(['_id'=> $this->convertToObjectId($_id)], $getBody);
            return $this->sendResult($result);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function updateStatusToPending($_id)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {

            $result = $this->DBupdate(['_id'=> $this->convertToObjectId($_id)], ['status'=>'PENDING']);
            return $this->sendResult($result);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function createNewShoppingCartByUserID($userId)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($this->createNewCart($userId));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function createNewCart($userId) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return [
                'user_id'=>$this->convertToObjectId($userId),
                'products'=>[],
                'address_detail'=>[],
                'billings'=>[
                    'total_price'=>0,
                    'sum_total'=>0
                ],
                'total_price'=>0,
                'total_quantity'=>0,
                'status'=>'ACTIVE'
            ];
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }
}

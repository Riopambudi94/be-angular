<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/8/15
 * Time: 上午10:39
 */

namespace App\HttpController;

use EasySwoole\Http\AbstractInterface\AbstractRouter;
use EasySwoole\Http\Bridge;
use FastRoute\RouteCollector;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\Http\Message\Status;


class Router extends AbstractRouter
{
    
    function initialize(RouteCollector $routeCollector)
    {

        @spl_autoload_register([$this, 'autoLoad']);
        
        $routeCollector->options("{route:.+}", function(Request $request, Response $response){
            $response->withHeader("Access-Control-Allow-Origin", "*");
            $response->withHeader("Access-Control-Allow-Headers", "token, Content-Type, Authorization, authorization, api_key, enctype, app_label, app-label");
            $response->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        });

        $this->SwooleJSONLoader($routeCollector);
        
        /** Routing Examples
         * $routeCollector->get('/user', '/index.html');
         * $routeCollector->get('/rpc', '/Rpc/index');
         * $routeCollector->get('/', function (Request $request, Response $response) {
         *      $Bridge = new Bridge($request, $response);
         *      $bannerController = new \BannerController();
         *      $Bridge->Response->writeJson(200, ['hello' => "yes"]);
         * });
         * 
         * $routeCollector->get('/test', function (Request $request, Response $response) {
         *     $response->write('this router test');
         *     return '/a';//Relocate to the /a method
         * });
         * 
         * $routeCollector->get('/userTest/{id:\d+}', function (Request $request, Response $response) {
         *    $response->write("this is router user ,your id is {$request->getQueryParam('id')}");//Get the id of the route match
         *     return false;//No longer request, end this response
         * });
         */

    }

    function autoLoad($className, $checkOnly = false){
        if(strpos($className, "App\\") === false){
            if(strpos($className,'Controller') !== false){
                $stringFileLocation = (PROJECT_PATH."Controllers/".$className.".php");
            } elseif(strpos($className,'ViewModelMaster') !== false){
                // $stringFileLocation = (PROJECT_PATH."viewmodels/".$className.".php");
            } elseif(strpos($className,'ViewModel') !== false){
                // $stringFileLocation = (PROJECT_PATH."viewmodels/{$className}/".$className.".php");
            } elseif(strpos($className,'Model') !== false){
                // $stringFileLocation = (PROJECT_PATH."models/".$className.".php");
            } else{
                // $stringFileLocation = (PROJECT_PATH."lib/".strtolower($className).".php");
            }
        }

        if($checkOnly == true){
            return is_file($stringFileLocation);
        }
        else{
            if(!class_exists($className) && isset($stringFileLocation)){
                @include_once($stringFileLocation);
            }
        }

    }
    
    function SwooleJSONLoader(RouteCollector $routeCollector){
        // return false;
        global $jsonRouterObjects;
        $currentDir     = PROJECT_PATH;
        // opening a directory and reading its contents 
        $dir        = $currentDir."Routes/JSONs/";

        if(!isset($jsonRouterObjects)){
             $jsonRouterObjects = [];
    
             $routerList = scandir($dir); 
    
             foreach($routerList as $routerJson){
                if($routerJson == '.' || $routerJson == ".."){
                    continue;
                }
        
                $jsonString     = @file_get_contents($dir.$routerJson);
                
                if($jsonString == false){
                    continue;
                }
    
                $jsonDecode    = json_decode($jsonString,1);
                if(is_null($jsonDecode)){
                    continue;
                }
    
                foreach($jsonDecode as $router){
                    $jsonRouterObjects[$router['method'].$router['version_path'].$router['route_url']] = [$router['method'], $router['version_path'].$router['route_url'], $router['class_method']];
                    
                 }
            }
        }
        
        
        //Setup Routing
        SETUP_ROUTING: {
            
            $r = array();
            foreach($jsonRouterObjects as $rt){
                list($urlMethod, $url, $classMethod) = $rt;
                
                $classMethod = explode("->", $classMethod);
                $urlMethod   = strtolower($urlMethod);

                $class  = isset($classMethod[0])?$classMethod[0]:null;
                $method = isset($classMethod[1])?$classMethod[1]:null;
                
                if(strpos($url, "@")){
                    preg_match_all("/\@([a-zA-Z\_]+)/i", $url, $match);

                    if(count($match[0]) > 0 && count($match[1]) > 0){
                        foreach($match[1] as $key=>$m){
                            // $match[1][$key] = "{".$m.":\d+}";
                            $match[1][$key] = "{".$m."}";
                        }
                        $url = str_replace($match[0], $match[1], $url);

                    }
                }
                $routeCollector->{$urlMethod}($url, function(Request $request, Response $response) use ($method, $class, $urlMethod, $url){
                    $this->load($request, $response, $method, $class, $urlMethod, $url);
                });
                

            }
        }
        return true;
    }

    function load(Request $request, Response $response, $method, $className, $urlMethod, $url){
        global $calledWriteLog;

        $Bridge = new Bridge($request, $response);

        if($this->autoLoad($className, true)){
            $class  = new $className($Bridge->Srv);

            $return = $class->{$method}($Bridge->Srv, $request->getQueryParams());

            if(isset($return['result'])){
                $return = $return['result'];
            }
//print_r($return);
            /**
             * Put Log into files 
             **/
            /* PUT_LOG_INTO_FILES:{
                foreach($calledWriteLog as $key=>$logs){
                    writeBulkToFile($logs, $logs[0][1]);
                    unset($calledWriteLog[$key]);
                }
            } */

            $Bridge->Response->withHeader("Access-Control-Allow-Origin", "*");
            $Bridge->Response->withHeader("Access-Control-Allow-Headers", "token, Content-Type, Authorization, authorization, api_key, enctype, app_label, app-label");
            $Bridge->Response->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            
            if(isset($return['error_code'])){
                $Bridge->Response->writeJson($return['error_code'], $return , $return['error']);
                return ;
            } else{
                if (!empty($return['output_dir']) && file_exists($return['output_dir']) ){

                    $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
                    $fileMimeType = finfo_file($fileinfo, $return['output_dir']);

                    if ($fileMimeType == "text/plain") {
                        $pos = strrpos($return['output_dir'], ".csv");
                        if ($pos !== false && $pos == strlen($return['output_dir']) - 4) {
                            $fileMimeType = "text/csv";
                        }
                    }

                    $Bridge->Response->withHeader("Content-Description", "File Transfer");
                    $Bridge->Response->withHeader("Content-Type", $fileMimeType);
                    // $Bridge->Response->withHeader("Content-Type", "application/octet-stream");
                    $Bridge->Response->withHeader("Content-Disposition", "attachment; filename=".basename($return['output_dir']));
                    // $Bridge->Response->withHeader("Content-Disposition", 'attachment; filename="'.basename($return['output_dir']).'"');
                    $Bridge->Response->withHeader("Content-Transfer-Encoding", "Binary");
                    $Bridge->Response->withHeader("Expires", "0");
                    $Bridge->Response->withHeader("Cache-Control", "must-revalidate");
                    $Bridge->Response->withHeader("Pragma", "public");
                    $Bridge->Response->withHeader("Content-Length", filesize($return['output_dir']));
                    $Bridge->Response->withStatus(200);
                    $Bridge->Response->sendFile($return['output_dir']);
                } else {
                    $Bridge->Response->writeJson(200, $return );
                }
                return ;
            }
        }
        else{
            $Bridge->Response->writeJson(404, ['error' =>true]);
            return ;
        }
    }
}
